/*
 * pcbqueue.h
 *
 *  Created on: Sep 20, 2014
 *      Author: rdu
 */

#ifndef PCBQUEUE_H_
#define PCBQUEUE_H_

#include "myos.h"

#define	QSORT_PRIORITY		0
#define QSORT_SLEEP_TIME	1

// Add a element to the tail of a queue
void PushToPCBQueueTail(OS_PCB_QUEUE *queue, OS_PCB *pcb);

// Insert a new element to a list, caller of this function needs to
//	specify the list to be operated by passing the head pointer, the
//	element to be inserted and the sort_type (sort all elements according
//	to priority or sleep time).
void InsertToPCBQueue(OS_PCB_QUEUE *queue, OS_PCB *element, INT32 sort_type);

// Remove a existing element from the head of a queue and return this
//	element as the result of this function call
OS_PCB* PopFromPCBQueueHead(OS_PCB_QUEUE *queue);

// Remove a existing element from a list, caller of this function needs to
//	specify the list to be operated by passing the head pointer and the
//	element to be removed. If the the operation is successful, "TRUE" will
//	be returned. Otherwise, "FALSE" will be returned.
BOOL RemoveFromPCBQueue(OS_PCB_QUEUE *queue, OS_PCB *element);

// Search a process using process name. Pass in the name of a process, find
//	the PCB of this process from a queue. If the process is found, return
//	the pointer to this pcb, otherwise return a NULL pointer.
OS_PCB*	SearchNameFromQueue(OS_PCB_QUEUE *queue, char *process_name);

// Search a process using process id. Pass in the process id, find the PCB
//	of this process from a queue. If the process is found, return the
//	pointer to this pcb, otherwise return a NULL pointer.
OS_PCB* SearchPIDFromQueue(OS_PCB_QUEUE *queue, long process_id);

#endif /* PCBQUEUE_H_ */
