/*
 * mytest.c
 *
 *  Created on: Sep 23, 2014
 *      Author: rdu
 */

#define          USER
#include         "global.h"
#include         "protos.h"
#include         "syscalls.h"
#include		 "hardware_driver.h"

#include         "stdio.h"
#include         "string.h"
#include         "stdlib.h"
#include         "math.h"
#include		 <assert.h>

#include		 "myos.h"

INT16 Z502_PROGRAM_COUNTER;

extern long Z502_REG1;
extern long Z502_REG2;
extern long Z502_REG3;
extern long Z502_REG4;
extern long Z502_REG5;
extern long Z502_REG6;
extern long Z502_REG7;
extern long Z502_REG8;
extern long Z502_REG9;
extern INT16 Z502_MODE;

extern OS_PCB	*OSCurrentRunningPCB;

// Private function prototypes, these functions are renamed version
//	of the ones from test.c
void mytest1x(void);
void mytest1x_nosleep(void);
void mytest1x_onlysleep(void);
void mytest1j_echo(void);
void MyErrorExpected(INT32, char[]);
void MySuccessExpected(INT32, char[]);
void MyTest2f_Statistics(int Pid);

#define         ILLEGAL_PRIORITY                -3
#define         LEGAL_PRIORITY                  10

// This test is designed to test CREATE_PROCESS() and GET_PROCESS_ID()
//	system calls
void test_process(void) {
    // Try to create a process with an illegal priority.
    printf("This is Release %s:  test_process\n", CURRENT_REL);
    CREATE_PROCESS("test1a", mytest1x, -1, &Z502_REG1,
                &Z502_REG9);
    CREATE_PROCESS("test1b", mytest1x, 2, &Z502_REG1,
            &Z502_REG9);
    CREATE_PROCESS("test1c", mytest1x, 5, &Z502_REG1,
    		&Z502_REG9);
    CREATE_PROCESS("test1d", mytest1x, 3, &Z502_REG1,
    		&Z502_REG9);
    CREATE_PROCESS("test1e", mytest1x, -5, &Z502_REG1,
    		&Z502_REG9);
    CREATE_PROCESS("test1f", mytest1x, 4, &Z502_REG1,
    		&Z502_REG9);
    CREATE_PROCESS("test1g", mytest1x, 3, &Z502_REG1,
    		&Z502_REG9);

    OSShowAllProcessInfo();

    //      Now test the call GET_PROCESS_ID for ourselves
    GET_PROCESS_ID("", &Z502_REG2, &Z502_REG9);     // Legal
    printf("The PID of root process is %ld\n", Z502_REG2);
    GET_PROCESS_ID("test1a", &Z502_REG2, &Z502_REG9);     // Legal
    printf("The PID of test1a process is %ld\n", Z502_REG2);
    GET_PROCESS_ID("test1b", &Z502_REG2, &Z502_REG9);     // Legal
    printf("The PID of test1b process is %ld\n", Z502_REG2);
    GET_PROCESS_ID("test1c", &Z502_REG2, &Z502_REG9);     // Legal
    printf("The PID of test1c process is %ld\n", Z502_REG2);

    TERMINATE_PROCESS(-2, &Z502_REG9);
}                                                  // End of test1b


#define         ILLEGAL_PRIORITY                -3
#define         LEGAL_PRIORITY                  10

// This test is designed to test if TerminateProcess() can work properly
void test_terminate(void) {
    static char process_name[16];

    // Try to create a process with an illegal priority.
    printf("This is Release %s:  test_terminate\n", CURRENT_REL);
    CREATE_PROCESS("test1b_a", mytest1x, ILLEGAL_PRIORITY, &Z502_REG1,
            &Z502_REG9);

    // Create two processes with same name - 1st succeeds, 2nd fails
    // Then terminate the process that has been created
    CREATE_PROCESS("two_the_same", mytest1x, LEGAL_PRIORITY, &Z502_REG2,
            &Z502_REG9);
    CREATE_PROCESS("two_the_same", mytest1x, LEGAL_PRIORITY, &Z502_REG1,
            &Z502_REG9);

    TERMINATE_PROCESS(Z502_REG2, &Z502_REG9);

    // Loop until an error is found on the create_process.
    // Since the call itself is legal, we must get an error
    // because we exceed some limit.
    Z502_REG9 = ERR_SUCCESS;
    while (Z502_REG9 == ERR_SUCCESS) {
        Z502_REG3++; /* Generate next unique program name*/
        sprintf(process_name, "Test1b_%ld", Z502_REG3);
        printf("Creating process \"%s\"\n", process_name);
        CREATE_PROCESS(process_name, mytest1x, LEGAL_PRIORITY, &Z502_REG1,
                &Z502_REG9);
    }

    OSShowAllProcessInfo();
    TERMINATE_PROCESS(1, &Z502_REG9);
    OSShowAllProcessInfo();
    TERMINATE_PROCESS(5, &Z502_REG9);
    OSShowAllProcessInfo();
    TERMINATE_PROCESS(19, &Z502_REG9);
    OSShowAllProcessInfo();

    //  When we get here, we've created all the processes we can.
    //  So the OS should have given us an error
    printf("%ld processes were created in all.\n", Z502_REG3);

    //      Now test the call GET_PROCESS_ID for ourselves
    GET_PROCESS_ID("", &Z502_REG2, &Z502_REG9);     // Legal
    printf("The PID of this process is %ld\n", Z502_REG2);

    // Try GET_PROCESS_ID on another existing process
    strcpy(process_name, "Test1b_1");
    GET_PROCESS_ID(process_name, &Z502_REG1, &Z502_REG9); /* Legal */
    printf("The PID of target process is %ld\n", Z502_REG1);

    GET_TIME_OF_DAY(&Z502_REG4);
    printf("Test1b, PID %ld, Ends at Time %ld\n", Z502_REG2, Z502_REG4);
    TERMINATE_PROCESS(-2, &Z502_REG9)

}                                                  // End of test1b


// This test is designed to test if timer queue works properly, it's a
//	simplified version of test1c without recursively sleep in test1x
#define         PRIORITY1C              10

void test_timerqueue(void) {
	static long   sleep_time = 1000;

	printf("This is Release %s:  test_timerqueue\n", CURRENT_REL);
	CREATE_PROCESS("test1c_a", mytest1x_nosleep, PRIORITY1C, &Z502_REG1, &Z502_REG9);
	//	SuccessExpected(Z502_REG9, "CREATE_PROCESS");

	CREATE_PROCESS("test1c_b", mytest1x_nosleep, PRIORITY1C, &Z502_REG2, &Z502_REG9);

	CREATE_PROCESS("test1c_c", mytest1x_nosleep, PRIORITY1C, &Z502_REG3, &Z502_REG9);

	CREATE_PROCESS("test1c_d", mytest1x_nosleep, PRIORITY1C, &Z502_REG4, &Z502_REG9);

	CREATE_PROCESS("test1c_e", mytest1x_nosleep, PRIORITY1C, &Z502_REG5, &Z502_REG9);

	OSShowAllProcessInfo();
	//	OSShowEmptyListInfo();

	// Now we sleep, see if one of the five processes has terminated, and
	// continue the cycle until one of them is gone.  This allows the test1x
	// processes to exhibit scheduling.
	// We know that the process terminated when we do a GET_PROCESS_ID and
	// receive an error on the system call.

	Z502_REG9 = ERR_SUCCESS;
	while (Z502_REG9 == ERR_SUCCESS) {
		SLEEP(sleep_time);
		GET_PROCESS_ID("test1c_e", &Z502_REG6, &Z502_REG9);

		OSShowAllProcessInfo();
		printf("process_id: %ld\n",Z502_REG6);
	}

	TERMINATE_PROCESS(-2, &Z502_REG9); /* Terminate all */

}


// This test is designed for testing the dispatcher
void test_dispatcher(void) {
	static long   sleep_time = 1000;

	printf("This is Release %s:  test_dispatcher\n", CURRENT_REL);
	CREATE_PROCESS("test1c_a", mytest1x, PRIORITY1C, &Z502_REG1, &Z502_REG9);
//	SuccessExpected(Z502_REG9, "CREATE_PROCESS");

	CREATE_PROCESS("test1c_b", mytest1x, PRIORITY1C, &Z502_REG2, &Z502_REG9);

	CREATE_PROCESS("test1c_c", mytest1x, PRIORITY1C, &Z502_REG3, &Z502_REG9);

	CREATE_PROCESS("test1c_d", mytest1x, PRIORITY1C, &Z502_REG4, &Z502_REG9);

	CREATE_PROCESS("test1c_e", mytest1x, PRIORITY1C, &Z502_REG5, &Z502_REG9);

	OSShowAllProcessInfo();
//	OSShowEmptyListInfo();

	// Now we sleep, see if one of the five processes has terminated, and
	// continue the cycle until one of them is gone.  This allows the test1x
	// processes to exhibit scheduling.
	// We know that the process terminated when we do a GET_PROCESS_ID and
	// receive an error on the system call.

	Z502_REG9 = ERR_SUCCESS;
	while (Z502_REG9 == ERR_SUCCESS) {
		SLEEP(sleep_time);
		GET_PROCESS_ID("test1c_e", &Z502_REG6, &Z502_REG9);

		OSShowAllProcessInfo();
		printf("process_id: %ld\n",Z502_REG6);
	}

	TERMINATE_PROCESS(-2, &Z502_REG9); /* Terminate all */
}


#define         PRIORITY_1F1             5
#define         PRIORITY_1F2            10
#define         PRIORITY_1F3            15
#define         PRIORITY_1F4            20
#define         PRIORITY_1F5            25

// This test is designed for testing if suspend/resume works
void test_suspend(void) {

	static long   sleep_time = 300;
	    int           iterations;

	    // Get OUR PID
	    Z502_REG1 = 0; // Initialize
	    GET_PROCESS_ID("", &Z502_REG2, &Z502_REG9);

	// Make legal targets
	    printf("Release %s:Test 1f: Pid %ld\n", CURRENT_REL, Z502_REG2);
	    CREATE_PROCESS("test1f_a", mytest1x, PRIORITY_1F1, &Z502_REG3, &Z502_REG9);

	    CREATE_PROCESS("test1f_b", mytest1x, PRIORITY_1F2, &Z502_REG4, &Z502_REG9);

	    CREATE_PROCESS("test1f_c", mytest1x, PRIORITY_1F3, &Z502_REG5, &Z502_REG9);

	    CREATE_PROCESS("test1f_d", mytest1x, PRIORITY_1F4, &Z502_REG6, &Z502_REG9);

	    CREATE_PROCESS("test1f_e", mytest1x, PRIORITY_1F5, &Z502_REG7, &Z502_REG9);

	    // Let the 5 processes go for a while
	    SLEEP(sleep_time);

	    // Do a set of suspends/resumes four times
	    for (iterations = 0; iterations < 4; iterations++) {
	        // Suspend 3 of the pids and see what happens - we should see
	        // scheduling behavior where the processes are yanked out of the
	        // ready and the waiting states, and placed into the suspended state.

	        SUSPEND_PROCESS(Z502_REG3, &Z502_REG9);
	        SUSPEND_PROCESS(Z502_REG5, &Z502_REG9);
	        SUSPEND_PROCESS(Z502_REG7, &Z502_REG9);

	        printf("check point: will call SLEEP()\n");

	        // Sleep so we can watch the scheduling action
	        SLEEP(sleep_time);

	        RESUME_PROCESS(Z502_REG3, &Z502_REG9);
	        RESUME_PROCESS(Z502_REG5, &Z502_REG9);
	        RESUME_PROCESS(Z502_REG7, &Z502_REG9);
	    }

	    //   Wait for children to finish, then quit
	    SLEEP((INT32 )10000);

	    printf("All processes will be terminated from root process!\n");
	    TERMINATE_PROCESS(-2, &Z502_REG9);

}                        // End of test1f


#define         MOST_FAVORABLE_PRIORITY         1
#define         FAVORABLE_PRIORITY             10
#define         NORMAL_PRIORITY                20
#define         LEAST_FAVORABLE_PRIORITY       30

// This test is designed for testing if priority can be changed properly
void test_changepriority(void) {
//    long     ourself;
//
//    GET_PROCESS_ID("", &Z502_REG2, &Z502_REG9);
//
//    // Make our priority high
//    printf("Release %s:Test 1h: Pid %ld\n", CURRENT_REL, Z502_REG2);
//    ourself = -1;
//    CHANGE_PRIORITY(ourself, 1, &Z502_REG9);
//
//    // Make legal targets
//    CREATE_PROCESS("test1h_a", mytest1x, NORMAL_PRIORITY, &Z502_REG3,
//            &Z502_REG9);
//    CREATE_PROCESS("test1h_b", mytest1x, NORMAL_PRIORITY, &Z502_REG4,
//            &Z502_REG9);
//    CREATE_PROCESS("test1h_c", mytest1x, NORMAL_PRIORITY, &Z502_REG5,
//            &Z502_REG9);
//    CREATE_PROCESS("test1h_d", mytest1x, NORMAL_PRIORITY, &Z502_REG6,
//                &Z502_REG9);
//    CREATE_PROCESS("test1h_e", mytest1x, NORMAL_PRIORITY, &Z502_REG7,
//                &Z502_REG9);
//
//
//    CHANGE_PRIORITY(1, 20, &Z502_REG9);
//    CHANGE_PRIORITY(2, 11, &Z502_REG9);
//    CHANGE_PRIORITY(3, 15, &Z502_REG9);
//    CHANGE_PRIORITY(4, 5, &Z502_REG9);
//    CHANGE_PRIORITY(5, 3, &Z502_REG9);
//
//    // Terminate everyone
//    TERMINATE_PROCESS(-2, &Z502_REG9);

    long     ourself;

    GET_PROCESS_ID("", &Z502_REG2, &Z502_REG9);

    // Make our priority high
    printf("Release %s:test change priority: Pid %ld\n", CURRENT_REL, Z502_REG2);
    ourself = -1;
    CHANGE_PRIORITY(ourself, MOST_FAVORABLE_PRIORITY, &Z502_REG9);

    // Make legal targets
    CREATE_PROCESS("test1h_a", mytest1x, NORMAL_PRIORITY, &Z502_REG3,
            &Z502_REG9);
    CREATE_PROCESS("test1h_b", mytest1x, NORMAL_PRIORITY, &Z502_REG4,
            &Z502_REG9);
    CREATE_PROCESS("test1h_c", mytest1x, NORMAL_PRIORITY, &Z502_REG5,
            &Z502_REG9);

    //      Sleep awhile to watch the scheduling
    SLEEP(200);

    //  Now change the priority - it should be possible to see
    //  that the priorities have been changed for processes that
    //  are ready and for processes that are sleeping.

    CHANGE_PRIORITY(Z502_REG3, FAVORABLE_PRIORITY, &Z502_REG9);

    CHANGE_PRIORITY(Z502_REG5, LEAST_FAVORABLE_PRIORITY, &Z502_REG9);

    //      Sleep awhile to watch the scheduling
    SLEEP(200);

    //  Now change the priority - it should be possible to see
    //  that the priorities have been changed for processes that
    //  are ready and for processes that are sleeping.

    CHANGE_PRIORITY(Z502_REG3, LEAST_FAVORABLE_PRIORITY, &Z502_REG9);

    CHANGE_PRIORITY(Z502_REG4, FAVORABLE_PRIORITY, &Z502_REG9);

    //     Sleep awhile to watch the scheduling
    SLEEP(10000);

    // Terminate everyone
    TERMINATE_PROCESS(-2, &Z502_REG9);

}                                               // End of test1h


// This test is designed to test if the OS can behave properly when it tries
//	to suspend/terminate a process in timer queue
void test_timerqueuespecial(void) {
	GET_PROCESS_ID("", &Z502_REG2, &Z502_REG9);

	// Make our priority high
	printf("Release %s:test change priority: Pid %ld\n", CURRENT_REL, Z502_REG2);
	CHANGE_PRIORITY(-1, 0, &Z502_REG9);

	// Make legal targets
	CREATE_PROCESS("test1h_a", mytest1x_onlysleep, NORMAL_PRIORITY, &Z502_REG3,
			&Z502_REG9);
	CREATE_PROCESS("test1h_b", mytest1x_onlysleep, NORMAL_PRIORITY, &Z502_REG4,
			&Z502_REG9);
	CREATE_PROCESS("test1h_c", mytest1x_onlysleep, NORMAL_PRIORITY, &Z502_REG5,
			&Z502_REG9);
	CREATE_PROCESS("test1h_d", mytest1x_onlysleep, NORMAL_PRIORITY, &Z502_REG6,
				&Z502_REG9);

	//      Sleep awhile to watch the scheduling
	SLEEP(100);

	OSShowAllProcessInfo();
	TERMINATE_PROCESS(Z502_REG3, &Z502_REG9);
	OSShowAllProcessInfo();
	TERMINATE_PROCESS(Z502_REG4, &Z502_REG9);
	OSShowAllProcessInfo();

	SUSPEND_PROCESS(Z502_REG5, &Z502_REG9);
	SUSPEND_PROCESS(Z502_REG6, &Z502_REG9);

	SLEEP(5000);

	// Terminate everyone
	TERMINATE_PROCESS(-2, &Z502_REG9);
}


#define         LEGAL_MESSAGE_LENGTH           (INT16)64
#define         ILLEGAL_MESSAGE_LENGTH         (INT16)1000

#define         MOST_FAVORABLE_PRIORITY         1
#define         NORMAL_PRIORITY                20

typedef struct {
    long    target_pid;
    long    source_pid;
    long    actual_source_pid;
    long    send_length;
    long    receive_length;
    long    actual_send_length;
    long    loop_count;
    char    msg_buffer[LEGAL_MESSAGE_LENGTH ];
} TEST1I_DATA;

void test_mytest1i(void) {
    TEST1I_DATA *td; // Use as ptr to data */

    // Here we maintain the data to be used by this process when running
    // on this routine.  This code should be re-entrant.

    td = (TEST1I_DATA *) calloc(1, sizeof(TEST1I_DATA));
    if (td == 0) {
        printf("Something screwed up allocating space in test1i\n");
    }

    td->loop_count = 0;

    // Get OUR PID
    GET_PROCESS_ID("", &Z502_REG2, &Z502_REG9);
    printf("Release %s:test message: Pid %ld\n", CURRENT_REL, Z502_REG2);

    // Make our priority high
    CHANGE_PRIORITY(-1, MOST_FAVORABLE_PRIORITY, &Z502_REG9);

    // Make a legal target
    CREATE_PROCESS("test1i_a", mytest1x, NORMAL_PRIORITY, &Z502_REG3,
            &Z502_REG9);

    //		Send a legal message
    td->target_pid = 0;
    td->send_length = 8;
    SEND_MESSAGE(td->target_pid, "message", td->send_length, &Z502_REG9);

    OSShowMsgQueueInfo();

    //      Receive with illegal buffer size
    td->source_pid = 0;
    td->receive_length = ILLEGAL_MESSAGE_LENGTH;
    RECEIVE_MESSAGE(td->source_pid, td->msg_buffer, td->receive_length,
            &(td->actual_send_length), &(td->actual_source_pid), &Z502_REG9);
//    ErrorExpected(Z502_REG9, "RECEIVE_MESSAGE");

    OSShowMsgQueueInfo();

    //      Send a legal ( but long ) message to self
    td->target_pid = Z502_REG2;
    td->send_length = LEGAL_MESSAGE_LENGTH;
    SEND_MESSAGE(td->target_pid, "a long but legal message", td->send_length,
            &Z502_REG9);
//    SuccessExpected(Z502_REG9, "SEND_MESSAGE");
    td->loop_count++;      // Count the number of legal messages sent

    //   Receive this long message, which should error because the receive buffer is too small
    td->source_pid = Z502_REG2;
    td->receive_length = 10;
    RECEIVE_MESSAGE(td->source_pid, td->msg_buffer, td->receive_length,
            &(td->actual_send_length), &(td->actual_source_pid), &Z502_REG9);
//    ErrorExpected(Z502_REG9, "RECEIVE_MESSAGE");

    printf("A total of %ld messages were enqueued.\n", td->loop_count - 1);
    TERMINATE_PROCESS(-2, &Z502_REG9);
}                                              // End of test1i


#define         LEGAL_MESSAGE_LENGTH            (INT16)64
#define         ILLEGAL_MESSAGE_LENGTH          (INT16)1000
#define         MOST_FAVORABLE_PRIORITY         1
#define         NORMAL_PRIORITY                20

typedef struct {
    long    target_pid;
    long    source_pid;
    long    actual_source_pid;
    long    send_length;
    long    receive_length;
    long    actual_send_length;
    long    send_loop_count;
    long    receive_loop_count;
    char    msg_buffer[LEGAL_MESSAGE_LENGTH ];
    char    msg_sent[LEGAL_MESSAGE_LENGTH ];
} TEST1J_DATA;

void test_mytest1j(void) {
    int Iteration;
    TEST1J_DATA *td;           // Use as pointer to data

    // Here we maintain the data to be used by this process when running
    // on this routine.  This code should be re-entrant.                */

    td = (TEST1J_DATA *) calloc(1, sizeof(TEST1J_DATA));
    if (td == 0) {
        printf("Something screwed up allocating space in test1j\n");
    }
    td->send_loop_count = 0;
    td->receive_loop_count = 0;

    // Get OUR PID
    GET_PROCESS_ID("", &Z502_REG2, &Z502_REG9);

    // Make our prior high
    printf("Release %s:Test mytest1j: Pid %ld\n", CURRENT_REL, Z502_REG2);
    CHANGE_PRIORITY(-1, MOST_FAVORABLE_PRIORITY, &Z502_REG9);

    // Make 3 legal targets
    CREATE_PROCESS("test1j_1", mytest1j_echo, NORMAL_PRIORITY, &Z502_REG3,
            &Z502_REG9);
    CREATE_PROCESS("test1j_2", mytest1j_echo, NORMAL_PRIORITY, &Z502_REG4,
            &Z502_REG9);
    CREATE_PROCESS("test1j_3", mytest1j_echo, NORMAL_PRIORITY, &Z502_REG5,
            &Z502_REG9);

    //      Send/receive a legal message to each child
    for (Iteration = 1; Iteration <= 3; Iteration++) {
        if (Iteration == 1) {
            td->target_pid = Z502_REG3;
            strcpy(td->msg_sent, "message to #1");
        }
        if (Iteration == 2) {
            td->target_pid = Z502_REG4;
            strcpy(td->msg_sent, "message to #2");
        }
        if (Iteration == 3) {
            td->target_pid = Z502_REG5;
            strcpy(td->msg_sent, "message to #3");
        }
        td->send_length = 20;
        SEND_MESSAGE(td->target_pid, td->msg_sent, td->send_length, &Z502_REG9);

        td->source_pid = -1;
        td->receive_length = LEGAL_MESSAGE_LENGTH;
        RECEIVE_MESSAGE(td->source_pid, td->msg_buffer, td->receive_length,
                &(td->actual_send_length), &(td->actual_source_pid),
                &Z502_REG9);

        if (strcmp(td->msg_buffer, td->msg_sent) != 0)
            printf("ERROR - msg sent != msg received.\n");

        if (td->actual_source_pid != td->target_pid )
            printf("ERROR - source PID not correct.\n");

        if (td->actual_send_length != td->send_length)
            printf("ERROR - send length not sent correctly.\n");
    }    // End of for loop

    //      Keep sending legal messages until the architectural (OS)
    //      limit for buffer space is exhausted.

    Z502_REG9 = ERR_SUCCESS;
    while (Z502_REG9 == ERR_SUCCESS) {
        td->target_pid = -1;
        sprintf(td->msg_sent, "This is message %ld", td->send_loop_count);
        td->send_length = 20;
        SEND_MESSAGE(td->target_pid, td->msg_sent, td->send_length, &Z502_REG9);

        td->send_loop_count++;
    }
    td->send_loop_count--;
    printf("A total of %ld messages were enqueued.\n", td->send_loop_count);

    //  Now receive back from the other processes the same number of messages we've just sent.
    SLEEP(100);
    while (td->receive_loop_count < td->send_loop_count) {
        td->source_pid = -1;
        td->receive_length = LEGAL_MESSAGE_LENGTH;
        RECEIVE_MESSAGE(td->source_pid, td->msg_buffer, td->receive_length,
                &(td->actual_send_length), &(td->actual_source_pid),
                &Z502_REG9);
        printf("Receive from PID = %ld: length = %ld: msg = %s:\n",
                td->actual_source_pid, td->actual_send_length, td->msg_buffer);
        td->receive_loop_count++;
    }

    printf("A total of %ld messages were received.\n",
            td->receive_loop_count);
    CALL(TERMINATE_PROCESS(-2, &Z502_REG9));

}                                                 // End of test1j


#define         LEGAL_MESSAGE_LENGTH            (INT16)64
#define         MOST_FAVORABLE_PRIORITY         1
#define         NORMAL_PRIORITY                20

typedef struct {
    long    target_pid;
    long    source_pid;
    long    actual_source_pid;
    long    send_length;
    long    receive_length;
    long    actual_send_length;
    long    send_loop_count;
    long    receive_loop_count;
    char    msg_buffer[LEGAL_MESSAGE_LENGTH ];
    char    msg_sent[LEGAL_MESSAGE_LENGTH ];
} TEST1L_DATA;

void test_mytest1l(void) {
    TEST1L_DATA *td; // Use as ptr to data */

    // Here we maintain the data to be used by this process when running
    // on this routine.  This code should be re-entrant.

    td = (TEST1L_DATA *) calloc(1, sizeof(TEST1L_DATA));
    if (td == 0) {
        printf("Something screwed up allocating space in test1j\n");
    }

    // Get OUR PID
    GET_PROCESS_ID("", &Z502_REG2, &Z502_REG9);

    // Make our priority high
    printf("Release %s:Test 1l: Pid %ld\n", CURRENT_REL, Z502_REG2);
    CHANGE_PRIORITY(-1, MOST_FAVORABLE_PRIORITY, &Z502_REG9);
    MySuccessExpected(Z502_REG9, "CHANGE_PRIORITY");

    // Make process to test with
    CREATE_PROCESS("test1l_1", mytest1j_echo, NORMAL_PRIORITY, &Z502_REG3,
            &Z502_REG9);
    MySuccessExpected(Z502_REG9, "CREATE_PROCESS");

    // BEGIN CASE 1 ------------------------------------------
    printf("\n\nBegin Case 1:\n\n");

    //      Sleep so that first process will wake and receive
    SLEEP(200);

    GET_PROCESS_ID("test1l_1", &Z502_REG4, &Z502_REG9);
    MySuccessExpected(Z502_REG9, "Get Receiving Process ID");

    if (Z502_REG3 != Z502_REG4)
        printf("ERROR!  The process ids should match! New process ID is: %ld",
                Z502_REG4);

    //      Suspend the receiving process
    SUSPEND_PROCESS(Z502_REG3, &Z502_REG9);
    MySuccessExpected(Z502_REG9, "SUSPEND");

    // Resume the recieving process
    RESUME_PROCESS(Z502_REG3, &Z502_REG9);
    MySuccessExpected(Z502_REG9, "RESUME");

    // Send it a message
    td->target_pid = Z502_REG3;
    td->send_length = 30;
    strcpy(td->msg_sent, "Resume first echo");
    SEND_MESSAGE(td->target_pid, td->msg_sent, td->send_length, &Z502_REG9);
    MySuccessExpected(Z502_REG9, "SEND_MESSAGE");

    // Receive it's response (process is now back in recieving mode)
    td->source_pid = -1;
    td->receive_length = LEGAL_MESSAGE_LENGTH;
    RECEIVE_MESSAGE(td->source_pid, td->msg_buffer, td->receive_length,
            &(td->actual_send_length), &(td->actual_source_pid), &Z502_REG9);
    MySuccessExpected(Z502_REG9, "RECEIVE_MESSAGE");
    if (strcmp(td->msg_buffer, td->msg_sent) != 0)
        printf("ERROR - msg sent != msg received.\n");
    if (td->actual_source_pid != Z502_REG3)
        printf("ERROR - source PID not correct.\n");
    if (td->actual_send_length != td->send_length)
        printf("ERROR - send length not sent correctly.\n");

    // BEGIN CASE 2 ------------------------------------------
    printf("\n\nBegin Case 2:\n\n");

    // create a competitor to show suspend works with incoming messages
    CREATE_PROCESS("test1l_2", mytest1j_echo, NORMAL_PRIORITY, &Z502_REG5,
            &Z502_REG9);
    MySuccessExpected(Z502_REG9, "CREATE_PROCESS");

    //      Sleep so that the new process will wake and receive
    SLEEP(200);

    GET_PROCESS_ID("test1l_2", &Z502_REG6, &Z502_REG9);
    MySuccessExpected(Z502_REG9, "Get Receiving Process ID");
    if (Z502_REG5 != Z502_REG6)
        printf("ERROR!  The process ids should match! New process ID is: %ld",
                Z502_REG4);

    //      Suspend the first process
    SUSPEND_PROCESS(Z502_REG3, &Z502_REG9);
    MySuccessExpected(Z502_REG9, "SUSPEND");

    // Send anyone a message
    td->target_pid = -1;
    td->send_length = 30;
    strcpy(td->msg_sent, "Going to second process");
    SEND_MESSAGE(td->target_pid, td->msg_sent, td->send_length, &Z502_REG9);
    MySuccessExpected(Z502_REG9, "SEND_MESSAGE");

    // Resume the first process
    RESUME_PROCESS(Z502_REG3, &Z502_REG9);
    MySuccessExpected(Z502_REG9, "RESUME");

    // Receive the second process' response
    td->source_pid = Z502_REG5;
    td->receive_length = LEGAL_MESSAGE_LENGTH;
    RECEIVE_MESSAGE(td->source_pid, td->msg_buffer, td->receive_length,
            &(td->actual_send_length), &(td->actual_source_pid), &Z502_REG9);
    MySuccessExpected(Z502_REG9, "RECEIVE_MESSAGE");
    if (strcmp(td->msg_buffer, td->msg_sent) != 0)
        printf("ERROR - msg sent != msg received.\n");
    if (td->actual_source_pid != Z502_REG5)
        printf("ERROR - source PID not correct.\n");
    if (td->actual_send_length != td->send_length)
        printf("ERROR - send length not sent correctly.\n");

    //
    // BEGIN CASE 3 ------------------------------------------
    printf("\n\nBegin Case 3:\n\n");

    //      Suspend the first process
    SUSPEND_PROCESS(Z502_REG3, &Z502_REG9);
    MySuccessExpected(Z502_REG9, "SUSPEND");

    // Send it, specifically, a message
    td->target_pid = Z502_REG3;
    td->send_length = 30;
    strcpy(td->msg_sent, "Going to suspended");
    SEND_MESSAGE(td->target_pid, td->msg_sent, td->send_length, &Z502_REG9);
    MySuccessExpected(Z502_REG9, "SEND_MESSAGE");

    // Resume the first process
    RESUME_PROCESS(Z502_REG3, &Z502_REG9);
    MySuccessExpected(Z502_REG9, "RESUME");

    // Receive the process' response
    td->source_pid = Z502_REG3;
    td->receive_length = LEGAL_MESSAGE_LENGTH;
    RECEIVE_MESSAGE(td->source_pid, td->msg_buffer, td->receive_length,
            &(td->actual_send_length), &(td->actual_source_pid), &Z502_REG9);
    MySuccessExpected(Z502_REG9, "RECEIVE_MESSAGE");

    if (strcmp(td->msg_buffer, td->msg_sent) != 0)
        printf("ERROR - msg sent != msg received.\n");
    if (td->actual_source_pid != Z502_REG3)
        printf("ERROR - source PID not correct.\n");
    if (td->actual_send_length != td->send_length)
        printf("ERROR - send length not sent correctly.\n");

    TERMINATE_PROCESS(-2, &Z502_REG9);

}                                                 // End of test1l


// Add code for the original test1m in test.c here
void test_mytest1m(void) {

	INT32 *pid,*creation_result;

	pid = (INT32 *)malloc(sizeof(INT32));
	creation_result = (INT32 *)malloc(sizeof(INT32));
	assert(pid != NULL);
	assert(creation_result != NULL);

	system("clear");
	printf("\n");
	printf("***   Welcome to use Z502 Simulator Release %s   ***\n",CURRENT_REL);

	printf("\n");
	printf("This a simple shell for the OS implemented by rdu.\n");
	printf("To get more information, type \"help\" in the following prompt.");
	printf("\n");

	while(1) {
		char cmd[15];

		printf("\n>> ");

		scanf("%s",cmd);

		if(strcmp(cmd, "help") == 0) {

			printf("----------------- 1. Run tests -----------------\n");

			printf("To run each test in the terminal of host OS, please exit this simple "
					"\n   shell first and use the following command:\n");
			printf("\n	 ./myOS test_name \n");
			printf("\nFor example: \n");
			printf("     $ ./myOS test1a \n");
			printf("     $ ./myOS test1c \n");

			printf("\n");
			printf("To run each test in this simple shell, please use the following command:\n");
			printf("\n 	test_name \n");
			printf("\nFor example: \n");
			printf("     >> test0 \n");
			printf("     >> test1a \n");

			printf("\n");
			printf("!!! Note: Currently only test0 can run in this simple shell !!!");
			printf("\n");

			printf("\n---------------- 2. List tests -----------------\n");
			printf("\n");
			printf("To see what tests are available to run, use this command:\n");
			printf("\n 	 lstest \n");
			printf("\n");
			printf("Please note that not all tests can run in this simple shell! If not, please\n");
			printf("exit this simple shell and run tests in the terminal of host OS.\n");

			printf("\n-------------- 3. Clear prompt ---------------\n");
			printf("\n");
			printf(" 	 clear\n");

			printf("\n------------- 4. Quit simple shell -------------\n");
			printf("\n");
			printf(" 	 quit or exit\n");


		}
		else if(strcmp(cmd, "quit") == 0 || strcmp(cmd, "exit") == 0) {
			break;
		}
		else if(strcmp(cmd, "lstest") == 0) {
			printf("The current OS for Z502 simulator can pass the following tests:\n");
			printf("\n");
			printf(" 	 test0\n");
			printf(" 	 test1a\n");
			printf(" 	 test1b\n");
			printf(" 	 test1c\n");
			printf(" 	 test1d\n");
			printf(" 	 test1e\n");
			printf(" 	 test1f\n");
			printf(" 	 test1g\n");
			printf(" 	 test1h\n");
			printf(" 	 test1i\n");
			printf(" 	 test1j\n");
			printf(" 	 test1k\n");

			printf("\nIt's recommended to run tests in the terminal of the host OS.\n");
			printf("\n");
			printf("*******************************************************************\n");
			printf("Only test0 is supported to run in this simple shell for the purpose "
					"\n  of feature demonstration of the simple shell. \n");
			printf("*******************************************************************\n");
		}
		else if(strcmp(cmd, "clear") == 0) {
			system("clear");

			printf("This a simple shell for the OS implemented by rdu.\n");
			printf("To get more information, type \"help\" in the following prompt.");
			printf("\n");
		}
		else if(strcmp(cmd, "test0") == 0) {
			CREATE_PROCESS("test0", test0, NORMAL_PRIORITY, &Z502_REG3,
			            &Z502_REG9);

			printf("\nTest0 has finished successfully!");
		}
		else if(strcmp(cmd, "test1a") == 0){
			printf("Test1a cannot be run this simple shell now.\n");
			printf("Please run test0 to show the feature of running tests in this"
					"\n   simple shell.");
		}
		else {
			printf("Illegal command!\n");
			printf("\n");
			printf("Please use \"help\" command to check available commands!\n");
		}
	}

	TERMINATE_PROCESS(-2, &Z502_REG9);
}

/**************************************************************************
 Test1x

 is used as a target by the process creation programs.
 It has the virtue of causing lots of rescheduling activity in
 a relatively random way.

 Z502_REG1              Loop counter
 Z502_REG2              OUR process ID
 Z502_REG3              Starting time
 Z502_REG4              Ending time
 Z502_REG9              Error returned

 **************************************************************************/

#define         NUMBER_OF_TEST1X_ITERATIONS     10

void mytest1x(void) {
	long   RandomSleep = 17;
	    int    Iterations;

	//    GET_PROCESS_ID("", &Z502_REG2, &Z502_REG9); // original code
	    GET_PROCESS_ID(OSCurrentRunningPCB->OSPCBProcName, &Z502_REG2, &Z502_REG9);
	    printf("Release %s:Test 1x: Pid %ld\n", CURRENT_REL, Z502_REG2);

	    for (Iterations = 0; Iterations < NUMBER_OF_TEST1X_ITERATIONS;
	            Iterations++) {
	        GET_TIME_OF_DAY(&Z502_REG3);
	        RandomSleep = (RandomSleep * Z502_REG3) % 143;
	        SLEEP(RandomSleep);
	        GET_TIME_OF_DAY(&Z502_REG4);
	        printf("Test1X: Pid = %d, Sleep Time = %ld, Latency Time = %d\n",
	                (int) Z502_REG2, RandomSleep, (int) (Z502_REG4 - Z502_REG3));
	    }
	    printf("Test1x, PID %ld, Ends at Time %ld\n", Z502_REG2, Z502_REG4);

	    TERMINATE_PROCESS(-1, &Z502_REG9);
	    printf("ERROR: Test1x should be terminated but isn't.\n");
} /* End of test1x    */

void mytest1x_nosleep(void) {

    GET_PROCESS_ID(OSCurrentRunningPCB->OSPCBProcName, &Z502_REG2, &Z502_REG9);
    printf("Release %s:Test 1x without sleep: Pid %ld\n", CURRENT_REL, Z502_REG2);

    OSShowAllProcessInfo();

    printf("Test1x, PID %ld, Ends at Time %ld\n", Z502_REG2, Z502_REG4);

    TERMINATE_PROCESS(-1, &Z502_REG9);
    printf("ERROR: Test1x should be terminated but isn't.\n");

} /* End of test1x    */

void mytest1x_onlysleep(void) {

    GET_PROCESS_ID(OSCurrentRunningPCB->OSPCBProcName, &Z502_REG2, &Z502_REG9);
    printf("Release %s:Test 1x without sleep: Pid %ld\n", CURRENT_REL, Z502_REG2);

    SLEEP(150);

    printf("Test1x, PID %ld, Ends at Time %ld\n", Z502_REG2, Z502_REG4);

//    while(1);

    TERMINATE_PROCESS(-1, &Z502_REG9);
    printf("ERROR: Test1x should be terminated but isn't.\n");

} /* End of test1x    */


/**************************************************************************

 Test1j_echo

 is used as a target by the message send/receive programs.
 All it does is send back the same message it received to the
 same sender.

 Z502_REG1              Pointer to data private to each process
 running this routine.
 Z502_REG2              OUR process ID
 Z502_REG3              Starting time
 Z502_REG4              Ending time
 Z502_REG9              Error returned
 **************************************************************************/

typedef struct {
    long     target_pid;
    long     source_pid;
    long     actual_source_pid;
    long     send_length;
    long     receive_length;
    long     actual_senders_length;
    char     msg_buffer[LEGAL_MESSAGE_LENGTH ];
    char     msg_sent[LEGAL_MESSAGE_LENGTH ];
} TEST1J_ECHO_DATA;

void mytest1j_echo(void) {
    TEST1J_ECHO_DATA *td;                          // Use as ptr to data

    // Here we maintain the data to be used by this process when running
    // on this routine.  This code should be re-entrant.

    td = (TEST1J_ECHO_DATA *) calloc(1, sizeof(TEST1J_ECHO_DATA));
    if (td == 0) {
        printf("Something screwed up allocating space in test1j_echo\n");
    }

    GET_PROCESS_ID("", &Z502_REG2, &Z502_REG9);
    printf("Release %s:Test 1j_echo: Pid %ld\n", CURRENT_REL, Z502_REG2);

    while (1) {       // Loop forever.
//    	printf("new loop starts...\n");
        td->source_pid = -1;
        td->receive_length = LEGAL_MESSAGE_LENGTH;
        RECEIVE_MESSAGE(td->source_pid, td->msg_buffer, td->receive_length,
                &(td->actual_senders_length), &(td->actual_source_pid),
                &Z502_REG9);

        printf("Receive from PID = %ld: length = %ld: msg = %s:\n",
                td->actual_source_pid, td->actual_senders_length,
                td->msg_buffer);

        td->target_pid = td->actual_source_pid;
        strcpy(td->msg_sent, td->msg_buffer);
        td->send_length = td->actual_senders_length;
        SEND_MESSAGE(td->target_pid, td->msg_sent, td->send_length, &Z502_REG9);
//        printf("a loop ended.\n");
    }     // End of while

}                                               // End of test1j_echo


/**************************************************************************
 ErrorExpected    and    SuccessExpected

 These routines simply handle the display of success/error data.

 **************************************************************************/

void MyErrorExpected(INT32 ErrorCode, char sys_call[]) {
    if (ErrorCode == ERR_SUCCESS) {
        printf("An Error SHOULD have occurred.\n");
        printf("????: Error( %d ) occurred in case %d (%s)\n", ErrorCode,
                Z502_PROGRAM_COUNTER - 2, sys_call);
    } else
        printf("Program correctly returned an error: %d\n", ErrorCode);

}                      // End of ErrorExpected

void MySuccessExpected(INT32 ErrorCode, char sys_call[]) {
    if (ErrorCode != ERR_SUCCESS) {
        printf("An Error should NOT have occurred.\n");
        printf("????: Error( %d ) occurred in case %d (%s)\n", ErrorCode,
                Z502_PROGRAM_COUNTER - 2, sys_call);
    } else
        printf("Program correctly returned success.\n");

}                      // End of SuccessExpected

void test_mytest2a(void) {

    GET_PROCESS_ID("", &Z502_REG4, &Z502_REG9);

    printf("Release %s: test_mytest2a: Pid %ld\n", CURRENT_REL, Z502_REG4);
    Z502_REG3 = 412;
    Z502_REG1 = Z502_REG3 + Z502_REG4;
    printf("Z502_REG4 = %ld \n",Z502_REG4);
    MEM_WRITE(Z502_REG3, &Z502_REG1);

    MEM_READ(Z502_REG3, &Z502_REG2);

    printf("PID= %ld  address= %ld   written= %ld   read= %ld\n", Z502_REG4,
            Z502_REG3, Z502_REG1, Z502_REG2);
    if (Z502_REG2 != Z502_REG1)
        printf("AN ERROR HAS OCCURRED.\n");
    TERMINATE_PROCESS(-1, &Z502_REG9);

}                   // End of test2a


#define         TEST_DATA_SIZE          (INT16)7

void test_mytest2b(void) {
    static INT32 test_data[TEST_DATA_SIZE ] = { 0, 4, PGSIZE - 2, PGSIZE, 3
            * PGSIZE - 2, (VIRTUAL_MEM_PAGES - 1) * PGSIZE, VIRTUAL_MEM_PAGES
            * PGSIZE - 2 };

    GET_PROCESS_ID("", &Z502_REG4, &Z502_REG9);
    printf("\n\nRelease %s:test_mytest2b: Pid %ld\n", CURRENT_REL, Z502_REG4);

    Z502_REG8 = 5 * PGSIZE;
    Z502_REG6 = Z502_REG8 + Z502_REG4 + 7;
    printf("Z502_REG6 = %ld\n", Z502_REG6);
    MEM_WRITE(Z502_REG8, &Z502_REG6);

    MEM_READ(Z502_REG8, &Z502_REG2);
    printf("address= %ld   written= %ld   read= %ld\n", Z502_REG8,
                Z502_REG6, Z502_REG2);

    int i=0;

    // Loop through all the memory addresses defined
    for(i=0 ; i<7; i++) {
        Z502_REG3 = test_data[i];
//        Z502_REG1 = Z502_REG3 + Z502_REG4 + 27;
        Z502_REG1 = i;
        printf("Virtual address: %ld\n", Z502_REG3);
        MEM_WRITE(Z502_REG3, &Z502_REG1);

        MEM_READ(Z502_REG3, &Z502_REG2);

        printf("test number= %d  address= %ld  written= %ld   read= %ld\n", i,
                Z502_REG3, Z502_REG1, Z502_REG2);
        if (Z502_REG2 != Z502_REG1)
            printf("AN ERROR HAS OCCURRED.\n");

        //      Go back and check earlier write
        MEM_READ(Z502_REG8, &Z502_REG7);

//        printf("PID= %d  address= %ld   written= %ld   read= %ld\n",
//                i, Z502_REG8, Z502_REG6, Z502_REG7);
        if (Z502_REG6 != Z502_REG7)
            printf("AN ERROR HAS OCCURRED.\n");
    }

    TERMINATE_PROCESS(-1, &Z502_REG9);
}                            // End of test2b



#define         DISPLAY_GRANULARITY2c           10
#define         TEST2C_LOOPS                    50

typedef union {
    char char_data[PGSIZE ];
    UINT32 int_data[PGSIZE / sizeof(int)];
} DISK_DATA;

void test_diskwr(void) {
	DISK_DATA *data_written;
	DISK_DATA *data_read;

	long       disk_id;
	INT32      sanity = 1234;
	long       sector;
//	int        Iterations;

	data_written = (DISK_DATA *) calloc(1, sizeof(DISK_DATA));
	data_read = (DISK_DATA *) calloc(1, sizeof(DISK_DATA));
	if (data_read == 0)
		printf("Something screwed up allocating space in test2c\n");

	GET_PROCESS_ID("", &Z502_REG4, &Z502_REG9);

	sector = Z502_REG4;
	printf("\n\nRelease %s:test_diskwr: Pid %ld\n", CURRENT_REL, Z502_REG4);

	disk_id = 1;
	sector = 1;
	data_written->int_data[0] = disk_id;
	data_written->int_data[1] = sanity;
	data_written->int_data[2] = sector;
	data_written->int_data[3] = 168;

//	disk_id = (Z502_REG4 / 2) % MAX_NUMBER_OF_DISKS + 1;
//	sector = ( 0 + (sector * 177)) % NUM_LOGICAL_SECTORS;
//	data_written->int_data[0] = disk_id;
//	data_written->int_data[1] = sanity;
//	data_written->int_data[2] = sector;
//	data_written->int_data[3] = (int) Z502_REG4;

	//printf( "Test2c - Pid = %d, Writing from Addr = %X\n", (int)Z502_REG4, (unsigned int )(data_written->char_data));
	DISK_WRITE(disk_id, sector, (char* )(data_written->char_data));

//	SLEEP(100);
	// Now read back the same data.  Note that we assume the
	// disk_id and sector have not been modified by the previous
	// call.
	DISK_READ(disk_id, sector, (char* )(data_read->char_data));

//	SLEEP(100);

	if ((data_read->int_data[0] != data_written->int_data[0])
			|| (data_read->int_data[1] != data_written->int_data[1])
			|| (data_read->int_data[2] != data_written->int_data[2])
			|| (data_read->int_data[3] != data_written->int_data[3])) {
		printf("ERROR in test_diskwr, Pid = %ld. Disk = %ld\n",
				Z502_REG4, disk_id );
		printf("Written:  %d,  %d,  %d,  %d\n",
				data_written->int_data[0], data_written->int_data[1],
				data_written->int_data[2], data_written->int_data[3] );
		printf("Read:     %d,  %d,  %d,  %d\n",
				data_read->int_data[0], data_read->int_data[1],
				data_read->int_data[2], data_read->int_data[3] );
	}
	else {
		printf("Disk is working properly!\n");

		printf("Written:  %d,  %d,  %d,  %d\n",
				data_written->int_data[0], data_written->int_data[1],
				data_written->int_data[2], data_written->int_data[3] );
		printf("Read:     %d,  %d,  %d,  %d\n",
				data_read->int_data[0], data_read->int_data[1],
				data_read->int_data[2], data_read->int_data[3] );
	}

	TERMINATE_PROCESS(-1, &Z502_REG9);
}

void test_mytest2c(void) {
	int		   failure_count=0;

    DISK_DATA *data_written;
    DISK_DATA *data_read;
    long       disk_id;
    INT32      sanity = 1234;
    long       sector;
    int        Iterations;

    data_written = (DISK_DATA *) calloc(1, sizeof(DISK_DATA));
    data_read = (DISK_DATA *) calloc(1, sizeof(DISK_DATA));
    if (data_read == 0)
        printf("Something screwed up allocating space in test2c\n");

    GET_PROCESS_ID("", &Z502_REG4, &Z502_REG9);

    sector = Z502_REG4;
    printf("\n\nRelease %s:test_mytest2c: Pid %ld\n", CURRENT_REL, Z502_REG4);

    for (Iterations = 0; Iterations < TEST2C_LOOPS; Iterations++) {
        // Pick some location on the disk to write to
    	disk_id = (Z502_REG4 / 2) % MAX_NUMBER_OF_DISKS + 1;
    	//sector = ( Iterations + (sector * 177)) % NUM_LOGICAL_SECTORS;
    	sector = Z502_REG4 + (Iterations * 17) % NUM_LOGICAL_SECTORS;  // Bugfix 4.11
        data_written->int_data[0] = disk_id;
        data_written->int_data[1] = sanity;
        data_written->int_data[2] = sector;
        data_written->int_data[3] = (int) Z502_REG4;
        //printf( "Test2c - Pid = %d, Writing from Addr = %X\n", (int)Z502_REG4, (unsigned int )(data_written->char_data));
        if(Iterations == 0)
        	printf("first disk operation\n");
//    	   printf("write: disk id: %ld, sector: %ld \n",disk_id,sector);
        DISK_WRITE(disk_id, sector, (char* )(data_written->char_data));

        // Now read back the same data.  Note that we assume the
        // disk_id and sector have not been modified by the previous
        // call.
//        if(Iterations == 0)
//        	printf("write: disk id: %ld, sector: %ld \n",disk_id,sector);
        DISK_READ(disk_id, sector, (char* )(data_read->char_data));

        if ((data_read->int_data[0] != data_written->int_data[0])
                || (data_read->int_data[1] != data_written->int_data[1])
                || (data_read->int_data[2] != data_written->int_data[2])
                || (data_read->int_data[3] != data_written->int_data[3])) {
            printf("ERROR in Test 2c, Pid = %ld. Disk = %ld\n",
                    Z502_REG4, disk_id );
            printf("Written:  %d,  %d,  %d,  %d\n",
                data_written->int_data[0], data_written->int_data[1],
                data_written->int_data[2], data_written->int_data[3] );
            printf("Read:     %d,  %d,  %d,  %d\n",
                data_read->int_data[0], data_read->int_data[1],
                data_read->int_data[2], data_read->int_data[3] );

            failure_count++;
        }
//        else if (( Iterations % DISPLAY_GRANULARITY2c ) == 0) {
//            printf("TEST2C: PID = %ld: SUCCESS READING  disk_id =%ld, sector = %ld\n",
//                    Z502_REG4, disk_id, sector);
//        }
    }   // End of for loop

    printf("\nTest part 1 result: %d failures out of %d tests with PID %ld\n\n",failure_count,TEST2C_LOOPS, Z502_REG4);
    failure_count = 0;

    // Now read back the data we've previously written and read
    printf("TEST2C: Reading back data: PID %ld.\n", Z502_REG4);
    sector = Z502_REG4;

    for (Iterations = 0; Iterations < TEST2C_LOOPS; Iterations++) {
    	disk_id = (Z502_REG4 / 2) % MAX_NUMBER_OF_DISKS + 1;
    	//sector = ( Iterations + (sector * 177)) % NUM_LOGICAL_SECTORS;
    	sector = Z502_REG4 + (Iterations * 17) % NUM_LOGICAL_SECTORS;  // Bugfix 4.11
        data_written->int_data[0] = disk_id;
        data_written->int_data[1] = sanity;
        data_written->int_data[2] = sector;
        data_written->int_data[3] = Z502_REG4;

        DISK_READ(disk_id, sector, (char* )(data_read->char_data));

        if ((data_read->int_data[0] != data_written->int_data[0])
                || (data_read->int_data[1] != data_written->int_data[1])
                || (data_read->int_data[2] != data_written->int_data[2])
                || (data_read->int_data[3] != data_written->int_data[3])) {
            printf("AN ERROR HAS OCCURRED in Test 2c, Pid = %ld.\n", Z502_REG4);
            printf("Written:  %d,  %d,  %d,  %d\n",
                data_written->int_data[0], data_written->int_data[1],
                data_written->int_data[2], data_written->int_data[3] );
            printf("Read:     %d,  %d,  %d,  %d\n",
                data_read->int_data[0], data_read->int_data[1],
                data_read->int_data[2], data_read->int_data[3] );

            failure_count++;
        }
//        else if ( ( Iterations % DISPLAY_GRANULARITY2c ) == 0) {
//            printf("TEST2C: PID = %ld: SUCCESS READING  disk_id =%ld, sector = %ld\n",
//                    Z502_REG4, disk_id, sector);
//        }

    }   // End of for loop

    printf("\nTest part 2 result: %d failures out of %d tests with PID %ld\n\n",failure_count,TEST2C_LOOPS, Z502_REG4);

    GET_TIME_OF_DAY(&Z502_REG8);
    printf("test_mytest2c:    PID %ld, Ends at Time %ld\n", Z502_REG4, Z502_REG8);
    TERMINATE_PROCESS(-1, &Z502_REG9);

}                                       // End of test2c

#define           MOST_FAVORABLE_PRIORITY                       1

void test_mytest2d(void) {
    static INT32 trash;
    static long    sleep_time = 10000;

    GET_PROCESS_ID("", &Z502_REG4, &Z502_REG5);
    printf("\n\nRelease %s: test_mytest2d: Pid %ld\n", CURRENT_REL, Z502_REG4);
    CHANGE_PRIORITY(-1, MOST_FAVORABLE_PRIORITY, &Z502_REG9);

    CREATE_PROCESS("first", test_mytest2c, 5, &trash, &Z502_REG5);
    CREATE_PROCESS("second", test_mytest2c, 5, &trash, &Z502_REG5);
    CREATE_PROCESS("third", test_mytest2c, 7, &trash, &Z502_REG5);
    CREATE_PROCESS("fourth", test_mytest2c, 7, &trash, &Z502_REG5);
    CREATE_PROCESS("fifth", test_mytest2c, 10, &trash, &Z502_REG5);

//    SLEEP(100000);
    SLEEP(50000);

    // In these next cases, we will loop until EACH of the child processes
    // has terminated.  We know it terminated because for a while we get
    // success on the call GET_PROCESS_ID, and then we get failure when the
    // process no longer exists.
    // We do this for each process so we can get decent statistics on completion

    Z502_REG9 = ERR_SUCCESS;      // Modified 07/2014 Rev 4.10
    while (Z502_REG9 == ERR_SUCCESS) {
        SLEEP(sleep_time);
        GET_PROCESS_ID("first", &Z502_REG6, &Z502_REG9);
    }
    Z502_REG9 = ERR_SUCCESS;
    while (Z502_REG9 == ERR_SUCCESS) {
        SLEEP(sleep_time);
        GET_PROCESS_ID("second", &Z502_REG6, &Z502_REG9);
    }
    Z502_REG9 = ERR_SUCCESS;
    while (Z502_REG9 == ERR_SUCCESS) {
        SLEEP(sleep_time);
        GET_PROCESS_ID("third", &Z502_REG6, &Z502_REG9);
    }
    Z502_REG9 = ERR_SUCCESS;
    while (Z502_REG9 == ERR_SUCCESS) {
        SLEEP(sleep_time);
        GET_PROCESS_ID("fourth", &Z502_REG6, &Z502_REG9);
    }
    Z502_REG9 = ERR_SUCCESS;
    while (Z502_REG9 == ERR_SUCCESS) {
        SLEEP(sleep_time);
        GET_PROCESS_ID("fifth", &Z502_REG6, &Z502_REG9);
    }
    GET_TIME_OF_DAY(&Z502_REG7);
    printf("Test2d, PID %ld, Ends at Time %ld\n\n", Z502_REG4, Z502_REG7);
    TERMINATE_PROCESS(-2, &Z502_REG5);

}                                   // End of test2d

#define         STEP_SIZE               VIRTUAL_MEM_PAGES/(4 * PHYS_MEM_PGS )
#define         DISPLAY_GRANULARITY2e     16 * STEP_SIZE
#define         TOTAL_ITERATIONS        256
void test_mytest2e(void) {
    int Iteration, MixItUp;
    int AddressesWritten[VIRTUAL_MEM_PAGES];

    GET_PROCESS_ID("", &Z502_REG4, &Z502_REG9);
    printf("\n\nRelease %s:test_mytest2e: Pid %ld\n", CURRENT_REL, Z502_REG4);

    for (Iteration = 0; Iteration < TOTAL_ITERATIONS; Iteration++ )
            AddressesWritten[Iteration] = 0;
    for (Iteration = 0; Iteration < TOTAL_ITERATIONS; Iteration++ ) {
        GetSkewedRandomNumber(&Z502_REG3, 1024);  // Generate Address    Bugfix 4.12
	Z502_REG3 = 4 * (Z502_REG3 / 4);          // Put address on mod 4 boundary
        AddressesWritten[Iteration] = Z502_REG3;  // Keep record of location written
        Z502_REG1 = PGSIZE * Z502_REG3;           // Generate Data    Bugfix 4.12
        MEM_WRITE(Z502_REG3, &Z502_REG1);         // Write the data

        MEM_READ(Z502_REG3, &Z502_REG2); // Read back data

        if (Iteration % DISPLAY_GRANULARITY2e == 0)
            printf("PID= %ld  address= %6ld   written= %6ld   read= %6ld\n",
                    Z502_REG4, Z502_REG3, Z502_REG1, Z502_REG2);
        if (Z502_REG2 != Z502_REG1) {  // Written = Read?
            printf("AN ERROR HAS OCCURRED ON 1ST READBACK.\n");
            printf("PID= %ld  address= %ld   written= %ld   read= %ld\n",
                    Z502_REG4, Z502_REG3, Z502_REG1, Z502_REG2);
        }

        // It makes life more fun!! to write the data again
        MEM_WRITE(Z502_REG3, &Z502_REG1); // Write the data

        OSShowMemFrameStatus();

    }    // End of for loop

    // Now read back the data we've written and paged
    // We try to jump around a bit in choosing addresses we read back
    printf("Reading back data: test 2e, PID %ld.\n", Z502_REG4);

    for (Iteration = 0; Iteration < TOTAL_ITERATIONS; Iteration++ ) {
        if (!( Iteration % 2 ))      // Bugfix 4.11
            MixItUp = Iteration;
        else
            MixItUp = TOTAL_ITERATIONS - Iteration;
        Z502_REG3 = AddressesWritten[MixItUp];     // Get location we wrote
        Z502_REG1 = PGSIZE * Z502_REG3;           // Generate Data    Bugfix 4.12
        MEM_READ(Z502_REG3, &Z502_REG2);      // Read back data

        if (Iteration % DISPLAY_GRANULARITY2e == 0)
            printf("PID= %ld  address= %6ld   written= %6ld   read= %6ld\n",
                    Z502_REG4, Z502_REG3, Z502_REG1, Z502_REG2);
        if (Z502_REG2 != Z502_REG1) {  // Written = Read?
            printf("AN ERROR HAS OCCURRED ON 2ND READBACK.\n");
            printf("PID= %ld  address= %ld   written= %ld   read= %ld\n",
                    Z502_REG4, Z502_REG3, Z502_REG1, Z502_REG2);
        }
    }    // End of for loop
    TERMINATE_PROCESS(-2, &Z502_REG5);      // Added 12/1/2013
}                                           // End of test2e

// This test is designed to test is just one paging request can be responded properly
void test_paging(void) {
	INT16 i;
	long addr;
	char data[PGSIZE];
	char data_read[PGSIZE];

	for(i = 0; i<64; i++) {
		data[i] = 1;
		data_read[i]=0;
	}

	// use up all 64 frames
	for(i = 0; i < 64; i++) {
		addr = i << 4;
		MEM_WRITE(addr, &data);
	}

//	OSShowMemFrameStatus();

	for(i = 0; i<64; i++) {
		data[i] = 2;
	}

//	MEM_WRITE(64<<4, &data);
	for(i=64; i < 128; i = i+8) {
		addr = i << 4;
		MEM_WRITE(addr, &data);
	}

	for(i=64; i < 128; i = i+8) {
		addr = i << 4;
		MEM_READ(addr, &data_read);
		printf("*** page idata_read: %d \n", data_read[0]);
	}

//	OSShowMemFrameStatus();

//	MEM_READ(64<<4, &data_read);
//	printf("*** after replacement: page 64: %d \n", data_read[0]);
//
//	MEM_READ(65<<4, &data_read);
//	printf("*** after replacement page 65: %d \n", data_read[0]);
//	MEM_READ(63<<4, &data_read);
//	printf("Data Read after replacement 63: %d \n", data_read[0]);

	TERMINATE_PROCESS(-2, &Z502_REG5);
}

#define                 NUMBER_OF_ITERATIONS            1 //3
#define                 LOOP_COUNT                    65 //400
#define                 DISPLAY_GRANULARITY2          100
#define                 LOGICAL_PAGES_TO_TOUCH       2 * PHYS_MEM_PGS

typedef struct {
    INT16 page_touched[LOOP_COUNT];   // Bugfix Rel 4.03  12/1/2013
} MEMORY_TOUCHED_RECORD;

void test_mytest2f(void) {
    MEMORY_TOUCHED_RECORD *mtr;
    short Iterations, Index, Loops;

    mtr = (MEMORY_TOUCHED_RECORD *) calloc(1, sizeof(MEMORY_TOUCHED_RECORD));

    GET_PROCESS_ID("", &Z502_REG4, &Z502_REG9);
    printf("\n\nRelease %s:test_mytest2f: Pid %ld\n", CURRENT_REL, Z502_REG4);

    for (Iterations = 0; Iterations < NUMBER_OF_ITERATIONS; Iterations++) {
    	for (Index = 0; Index < LOOP_COUNT; Index++)   // Bugfix Rel 4.03  12/1/2013
    		mtr->page_touched[Index] = 0;

    	for (Loops = 0; Loops < LOOP_COUNT; Loops++) {
    		// Get a random page number
//    		GetSkewedRandomNumber(&Z502_REG7, LOGICAL_PAGES_TO_TOUCH);
    		Z502_REG7 = Loops;
    		Z502_REG3 = PGSIZE * Z502_REG7; // Convert page to addr.
    		Z502_REG1 = Z502_REG3 + Z502_REG4;   // Generate data for page
    		MEM_WRITE(Z502_REG3, &Z502_REG1);
    		// Write it again, just as a test
    		MEM_WRITE(Z502_REG3, &Z502_REG1);
//    		MEM_READ(Z502_REG3, &Z502_REG2);
//    		printf("PID= %ld  address= %ld   written= %ld   read= %ld\n",
//    				Z502_REG4, Z502_REG3, Z502_REG1, Z502_REG2);

//    		char data[PGSIZE];
//    		Z502ReadPhysicalMemory(Loops, data);
//    		Z502WritePhysicalMemory(Loops, data);

//    		MEM_READ(Z502_REG3, &Z502_REG2);
//    		printf("After swap, data read from %ld: %ld\n",
//    				Z502_REG3, Z502_REG2);

//    		Z502_REG1 = 22;
//    		MEM_WRITE(Z502_REG3+1, &Z502_REG1);
    		// Read it back and make sure it's the same
    		MEM_READ(Z502_REG3, &Z502_REG2);
//    		if (Loops % DISPLAY_GRANULARITY2 == 0)
//    			printf("PID= %ld  address= %ld   written= %ld   read= %ld\n",
//    					Z502_REG4, Z502_REG3+1, Z502_REG1, Z502_REG2);
    		if (Z502_REG2 != Z502_REG1)
    			printf("AN ERROR HAS OCCURRED: READ NOT EQUAL WRITE.\n");
//
//    		// Record in our data-base that we've accessed this page
    		mtr->page_touched[(short) Loops] = Z502_REG7;
//    		MyTest2f_Statistics( Z502_REG4 );

//    		if(Loops == 63) {
//    			OSShowPhysicalMemory(0);
//    			OSShowPhysicalMemory(1);
//    			OSShowPhysicalMemory(2);
//    			OSShowPhysicalMemory(3);
//    			OSShowPhysicalMemory(4);
//    			OSShowPhysicalMemory(5);
//    		}

        }   // End of for Loops

//    	printf("\nPage Touched\n");
//    	for(Loops = 0; Loops < LOOP_COUNT; Loops++) {
//    		if(Loops !=0 && Loops%8 == 0)
//    			printf("\n");
//    		if(mtr->page_touched[(short) Loops] < 10)
//    			printf(" %d ",mtr->page_touched[(short) Loops]);
//    		else
//    			printf("%d ",mtr->page_touched[(short) Loops]);
//    	}
//    	printf("\n");
//
//    	char data[PGSIZE];
//    	Z502ReadPhysicalMemory(0, data);
//    	printf("\ndata in frame id 0: \n");
//    	int i=0;
//    	for(i=0;i<PGSIZE;i++){
//    		if(i!=0 && i%8 == 0)
//    			printf("\n");
//
//    		if(data[i]<10)
//    			printf(" %d ",data[i]);
//    		else
//    			printf("%d ",data[i]);
//    	}
//    	printf("\n\n");

//    	OSShowPhysicalMemory(0);

    	printf("\n**********************************");
    	printf("\nINFO: Now read data back in PID: %ld\n",Z502_REG4);
    	printf("***********************************\n");

        for (Loops = 0; Loops < LOOP_COUNT; Loops++) {
//    	for (Loops = 0; Loops < 6; Loops++) {
//            // We can only read back from pages we've previously
//            // written to, so find out which pages those are.
//    		Loops = 2;
            Z502_REG6 = mtr->page_touched[(short) Loops];
            Z502_REG3 = PGSIZE * Z502_REG6; // Convert page to addr.
            Z502_REG1 = Z502_REG3 + Z502_REG4; // Expected read
//            printf("\nReading data at page %ld\n",Z502_REG6);
            MEM_READ(Z502_REG3, &Z502_REG2);
//            MyTest2f_Statistics( Z502_REG4 );

//            OSShowPhysicalMemory(0);
//
//            if (Loops % DISPLAY_GRANULARITY2 == 0)
//                printf("PID= %ld  address= %ld   written= %ld   read= %ld\n",
//                        Z502_REG4, Z502_REG3, Z502_REG1, Z502_REG2);
            if (Z502_REG2 != Z502_REG1) {
                printf("ERROR HAS OCCURRED: READ NOT SAME AS WRITE.\n");
                printf("PID= %ld  address= %ld   written= %ld   read= %ld\n",
                		Z502_REG4, Z502_REG3, Z502_REG1, Z502_REG2);
                Z502Halt();
//                printf("Loops = %d\n",Loops);
//                break;
            }
        }   // End of for Loops

//    	OSShowPhysicalMemory(0);
//    	OSShowPhysicalMemory(1);
//    	OSShowPhysicalMemory(2);
//    	OSShowPhysicalMemory(3);
//    	OSShowPhysicalMemory(4);
//    	OSShowPhysicalMemory(5);
//    	OSShowPhysicalMemory(6);

//    	OSShowPageTable();

//        OSShowMemFrameStatus();

//        // We've completed reading back everything
//        printf("TEST 2f, PID %ld, HAS COMPLETED %d ITERATIONS\n", Z502_REG4,
//                Iterations);
    }   // End of for Iterations

    TERMINATE_PROCESS(-1, &Z502_REG9);

}                                 // End of test2f


#define         PRIORITY2G              10

void test_mytest2g(void) {
    static long    sleep_time = 10000;

    printf("This is Release %s:  test_mytest2g\n", CURRENT_REL);
    CREATE_PROCESS("test2g_a", test_mytest2f, PRIORITY2G, &Z502_REG1, &Z502_REG9);
    CREATE_PROCESS("test2g_b", test_mytest2f, PRIORITY2G, &Z502_REG2, &Z502_REG9);
    CREATE_PROCESS("test2g_c", test_mytest2f, PRIORITY2G, &Z502_REG3, &Z502_REG9);
    CREATE_PROCESS("test2g_d", test_mytest2f, PRIORITY2G, &Z502_REG4, &Z502_REG9);
    CREATE_PROCESS("test2g_e", test_mytest2f, PRIORITY2G, &Z502_REG5, &Z502_REG9);
    MySuccessExpected(Z502_REG9, "CREATE_PROCESS");

    // In these next cases, we will loop until EACH of the child processes
    // has terminated.  We know it terminated because for a while we get
    // success on the call GET_PROCESS_ID, and then we get failure when the
    // process no longer exists.
    // We do this for each process so we can get decent statistics on completion

    Z502_REG9 = ERR_SUCCESS;      // Modified 12/2013 Rev 4.10
    while (Z502_REG9 == ERR_SUCCESS) {
        SLEEP(sleep_time);
        GET_PROCESS_ID("test2g_a", &Z502_REG6, &Z502_REG9);
    }
    Z502_REG9 = ERR_SUCCESS;
    while (Z502_REG9 == ERR_SUCCESS) {
        SLEEP(sleep_time);
        GET_PROCESS_ID("test2g_b", &Z502_REG6, &Z502_REG9);
    }
    Z502_REG9 = ERR_SUCCESS;
    while (Z502_REG9 == ERR_SUCCESS) {
        SLEEP(sleep_time);
        GET_PROCESS_ID("test2g_c", &Z502_REG6, &Z502_REG9);
    }
    Z502_REG9 = ERR_SUCCESS;
    while (Z502_REG9 == ERR_SUCCESS) {
        SLEEP(sleep_time);
        GET_PROCESS_ID("test2g_d", &Z502_REG6, &Z502_REG9);
    }
    Z502_REG9 = ERR_SUCCESS;
    while (Z502_REG9 == ERR_SUCCESS) {
        SLEEP(sleep_time);
        GET_PROCESS_ID("test2g_e", &Z502_REG6, &Z502_REG9);
    }
    TERMINATE_PROCESS(-2, &Z502_REG9); // Terminate all

}            // End test2g

// This test is designed to test page replacement when processes have
//		different priorities
void test_paging_priority(void) {
    static long    sleep_time = 10000;

    printf("This is Release %s:  test_mytest2g\n", CURRENT_REL);
    CREATE_PROCESS("test2g_a", test2f, PRIORITY2G, &Z502_REG1, &Z502_REG9);
    CREATE_PROCESS("test2g_b", test2f, PRIORITY2G+1, &Z502_REG2, &Z502_REG9);
    CREATE_PROCESS("test2g_c", test2f, PRIORITY2G+2, &Z502_REG3, &Z502_REG9);
    CREATE_PROCESS("test2g_d", test2f, PRIORITY2G+3, &Z502_REG4, &Z502_REG9);
    CREATE_PROCESS("test2g_e", test2f, PRIORITY2G+4, &Z502_REG5, &Z502_REG9);
    MySuccessExpected(Z502_REG9, "CREATE_PROCESS");

    // In these next cases, we will loop until EACH of the child processes
    // has terminated.  We know it terminated because for a while we get
    // success on the call GET_PROCESS_ID, and then we get failure when the
    // process no longer exists.
    // We do this for each process so we can get decent statistics on completion

    Z502_REG9 = ERR_SUCCESS;      // Modified 12/2013 Rev 4.10
    while (Z502_REG9 == ERR_SUCCESS) {
        SLEEP(sleep_time);
        GET_PROCESS_ID("test2g_a", &Z502_REG6, &Z502_REG9);
    }
    Z502_REG9 = ERR_SUCCESS;
    while (Z502_REG9 == ERR_SUCCESS) {
        SLEEP(sleep_time);
        GET_PROCESS_ID("test2g_b", &Z502_REG6, &Z502_REG9);
    }
    Z502_REG9 = ERR_SUCCESS;
    while (Z502_REG9 == ERR_SUCCESS) {
        SLEEP(sleep_time);
        GET_PROCESS_ID("test2g_c", &Z502_REG6, &Z502_REG9);
    }
    Z502_REG9 = ERR_SUCCESS;
    while (Z502_REG9 == ERR_SUCCESS) {
        SLEEP(sleep_time);
        GET_PROCESS_ID("test2g_d", &Z502_REG6, &Z502_REG9);
    }
    Z502_REG9 = ERR_SUCCESS;
    while (Z502_REG9 == ERR_SUCCESS) {
        SLEEP(sleep_time);
        GET_PROCESS_ID("test2g_e", &Z502_REG6, &Z502_REG9);
    }
    TERMINATE_PROCESS(-2, &Z502_REG9); // Terminate all

}            // End test2g

// Delay for test_disk_usage()
void delay() {
	long i = 0;
	long j = 0;

	for(i = 0; i < 50000; i++ ) {
		for( j = 0; j < 10000; j++) {

		}
	}
}

// This test is designed to show the usage map of disk
void test_disk_usage() {
	DISK_DATA *data_written;
	DISK_DATA *data_read;
	long       disk_id;
	INT32      sanity = 1234;
	long       sector;
	int        Iterations;

	data_written = (DISK_DATA *) calloc(1, sizeof(DISK_DATA));
	data_read = (DISK_DATA *) calloc(1, sizeof(DISK_DATA));
	if (data_read == 0)
		printf("Something screwed up allocating space in test2c\n");

	GET_PROCESS_ID("", &Z502_REG4, &Z502_REG9);

	sector = Z502_REG4;
	printf("\n\nRelease %s:test_mytest2c: Pid %ld\n", CURRENT_REL, Z502_REG4);

	for (Iterations = 0; Iterations < TEST2C_LOOPS; Iterations++) {
		// Pick some location on the disk to write to
		disk_id = 1;
		GetSkewedRandomNumber(&Z502_REG7, 1600);
		sector = Z502_REG7;
		data_written->int_data[0] = disk_id;
		data_written->int_data[1] = sanity;
		data_written->int_data[2] = sector;
		data_written->int_data[3] = (int) Z502_REG4;

		DISK_WRITE(disk_id, sector, (char* )(data_written->char_data));
		system("clear");
		DrvShowDiskUsageMap(1);

		delay();
	}   // End of for loop


	TERMINATE_PROCESS(-2, &Z502_REG9); // Terminate all
}

#define  MAX2F_PID    20
#define  REPORT_GRANULARITY_2F     100
void MyTest2f_Statistics(int Pid) {
    static short NotInitialized = TRUE;
    static int   PagesTouched[MAX2F_PID];
    int i;
    int LowestReportingPid = -1;
    if ( NotInitialized )  {
        for ( i = 0; i < MAX2F_PID; i++ )
            PagesTouched[i] = 0;
        NotInitialized = FALSE;
    }
    if ( Pid >= MAX2F_PID )  {
        printf( "In Test2f_Statistics - the pid entered, ");
        printf( "%d, is larger than the maximum allowed", Pid );
        exit(0);
    }
    PagesTouched[Pid]++;
    // It's time to print out a report
    if ( PagesTouched[Pid] % REPORT_GRANULARITY_2F == 0 )  {
        i = 0;
        while( PagesTouched[i] == 0 ) i++;
        LowestReportingPid = i;
        if ( Pid == LowestReportingPid ) {
            printf("----- Report by test2f - Pid %d -----\n", Pid );
            for ( i = 0; i < MAX2F_PID; i++ )  {
                if ( PagesTouched[i] > 0 )
                    printf( "Pid = %d, Pages Touched = %d\n", i, PagesTouched[i] );
            }
        }
    }
}                 // End of Test2f_Statistics
