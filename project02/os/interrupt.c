/*
 * interrupt.c
 *
 *  Created on: Oct 3, 2014
 *      Author: rdu
 */

#include	<stdio.h>
#include	"interrupt.h"
#include	"myos.h"
#include	"syscalls.h"
#include	"hardware_driver.h"

extern OS_PCB	*OSCurrentRunningPCB;

/*------------------ Functions for timer interrupt handling ------------------*/

// When an interrupt of timer occures successfully, this function is called
void OSTimerSuccessISR() {

	OSLockQueueOperation(TIMER_QUEUE, SUSPEND_UNTIL_LOCKED);
	OSLockQueueOperation(READY_QUEUE, SUSPEND_UNTIL_LOCKED);

#ifndef MINIMAL_USER_PRINT
	printf("system time (checked from interrupt handler): %d \n",DrvGetCurrentSysTime());
#endif

//	OSPopHeadFromTimerQueue();
	OSCleanOverdueProcess();
//	printf("load time from interrupt\n");
	OSLoadTimeFromTimerQueue();

	OSUnlockQueueOperation(READY_QUEUE, SUSPEND_UNTIL_LOCKED);
	OSUnlockQueueOperation(TIMER_QUEUE, SUSPEND_UNTIL_LOCKED);

#ifdef ENABLE_SCHEDULER_PRINTER
	if(OSCurrentRunningPCB != NULL)
		OSSchedulerPrinter(OSCurrentRunningPCB->OSPCBPid, SP_ACTION_TIMERISR, TRUE);
#endif

#ifndef MINIMAL_USER_PRINT
	printf("Timer ISR ended!\n");
#endif
}

// When an interrupt of timer occures with a error, this function is called
void OSTimerFailureISR() {
	// do nothing
}

/*------------------- Functions for disk interrupt handling ------------------*/

// When an interrupt of disk occurs successfully, this function is called
void OSDiskSuccessISR(long disk_id) {
#ifndef MINIMAL_USER_PRINT
	printf("OS_INFO: Loading the next disk task.\n");
#endif
//	printf("Before...\n");
//	OSKeepSimulatorAlive();
//	OSShowDiskTaskListInfo();
//	printf("interrupt of disk %ld starts\n",disk_id);
	OSLockDiskTaskList(disk_id);
	OSResumeDiskFinishedProcess(disk_id);
	OSCleanFinishedDiskTask(disk_id);
	OSLoadNextDiskTask(disk_id);
	OSUnlockDiskTaskList(disk_id);
//	printf("interrupt of disk %ld ends\n",disk_id);

//	printf("After...\n");
//	OSShowDiskTaskListInfo();

//	printf("Disk ISR ended!\n");
}

// When an interrupt of disk occurs successfully, this function is called
void OSDiskFailureISR(long disk_id) {
	// do nothing
}
