/*
 * time.c
 *
 *  Created on: Sep 11, 2014
 *      Author: rdu
 */

#include 	<stdio.h>
#include	"myos.h"
#include	"hardware_driver.h"
#include	"pcbqueue.h"
#include	"syscalls.h"


extern OS_PCB			*OSCurrentRunningPCB;
//extern OS_PCB_QUEUE 	*OSProcEmptyList;
extern OS_PCB_QUEUE		*OSProcTimerQueue;
//extern OS_PCB_QUEUE		*OSProcReadyQueue;


// Private function prototypes
// Check if a process is already in timer queue to avoid attempting to add
//	it into timer queue multiple times
BOOL OSCheckExistInTimerQueue(OS_PCB *pcb);


// Add a process to timer queue
/**
 * @input time: sleep time of the current process
 */
void OSAddToTimerQueue(long time) {

//	int i = 0;
//	BOOL load_result = FALSE;

	if(time >= 0) {

		long current_time = DrvGetCurrentSysTime();

		if(OSCurrentRunningPCB != NULL)
			OSCurrentRunningPCB->OSPCBSleepTime = time + current_time;

#ifndef MINIMAL_USER_PRINT
		printf("current system time: %ld, sleep time: %ld, set time: %ld \n",current_time,time,OSCurrentRunningPCB->OSPCBSleepTime);
#endif

		OSLockQueueOperation(TIMER_QUEUE, SUSPEND_UNTIL_LOCKED);
		OSLockQueueOperation(READY_QUEUE, SUSPEND_UNTIL_LOCKED);

		if(OSCheckExistInTimerQueue(OSCurrentRunningPCB) != TRUE)
			InsertToPCBQueue(OSProcTimerQueue,OSCurrentRunningPCB,QSORT_SLEEP_TIME);

//		printf("load time from svc\n");
		OSLoadTimeFromTimerQueue();
		OSCurrentRunningPCB = NULL;

		OSUnlockQueueOperation(READY_QUEUE, SUSPEND_UNTIL_LOCKED);
		OSUnlockQueueOperation(TIMER_QUEUE, SUSPEND_UNTIL_LOCKED);
	}
	else {
		printf("OS ERROR: A negative number cannot be passed into SLEEP() system call!\n");
	}
}

// Remove a process from timer queue
/**
 * @input pcb: the OS_PCB to be removed from the timer queue
 * @output: the result of the operation
 */
BOOL OSRemoveFromTimerQueue(OS_PCB *pcb) {

	BOOL result;

	result = RemoveFromPCBQueue(OSProcTimerQueue, pcb);

	if(result) {
//		printf("Process is removed from timer queue successfully!\n");
		return TRUE;
	}
	else {
		printf("OS ERROR: Process to be removed from ready queue is not found!\n");
		return FALSE;
	}
}

// Check if timer queue is empty, if not load the sleep time
//	from the first PCB in the queue
// when a dispatcher is implemented and used, DrvResetTimer() should be called
//	instead of DrvSetSleepTime()
void OSLoadTimeFromTimerQueue() {
	long set_time;
	long current_time;
	INT32 timer_reset_result;

	if(OSProcTimerQueue->PCBQueueHead != NULL) {
		// Try to load a sleep time to timer until the timer is reset successfully
		//	or the timer queue becomes empty
		do{
			if(OSProcTimerQueue->PCBQueueHead != NULL) {
				// load time from the first pcb in timer queue
				current_time = DrvGetCurrentSysTime();
				set_time = OSProcTimerQueue->PCBQueueHead->OSPCBSleepTime - current_time;
				timer_reset_result = DrvResetTimer(set_time);

				if(timer_reset_result == TIMER_RESET_ILLEGAL_PARAM) {
#ifndef MINIMAL_USER_PRINT
					printf("OS ERROR: A interrupt was supposed to happen at system time: %ld when system time is: %ld \n",set_time+current_time,current_time);
#endif
					OSCleanOverdueProcess();
				}
			}
			else {
				timer_reset_result = TIMER_RESET_SUCCESS;
			}
		}while(timer_reset_result != TIMER_RESET_SUCCESS);
	}
	else {
#ifndef MINIMAL_USER_PRINT
		printf("OS INFO: Failed to load time, timer queue is empty!\n");
#endif
	}
}

// Pop the head process from timer queue
OS_PCB* OSPopHeadFromTimerQueue() {
	OS_PCB* pcb;

	pcb = PopFromPCBQueueHead(OSProcTimerQueue);

	// Popped out from timer queue means it's ready to continue, so it's
	//	pushed to ready queue
//	OSLockQueueOperation(READY_QUEUE, SUSPEND_UNTIL_LOCKED);

//	if(pcb != NULL)
//		OSAddToReadyQueue(pcb);

	return pcb;

//	OSUnlockQueueOperation(READY_QUEUE, SUSPEND_UNTIL_LOCKED);

}

// Check if a process is already in timer queue to avoid attempting to add
//	it into timer queue multiple times. If it already exits, this function
//	will return TRUE, otherwise FALSE.
BOOL OSCheckExistInTimerQueue(OS_PCB *pcb) {
//	BOOL result;
//
//	if(SearchPIDFromQueue(OSProcTimerQueue, pcb->OSPCBPid) != NULL)
//		result = TRUE;
//	else
//		result = FALSE;
//
//	return result;

	if(SearchPIDFromQueue(OSProcTimerQueue, pcb->OSPCBPid) != NULL)
		return TRUE;
	else
		return FALSE;
}

// Check if there is a process in timer queue whose sleep time has already
//	passed but an interrupt was not triggered as expected.
//	** A possible situation for this scenario to happen is when the sleep time is
//	so short that after this process is put to the timer queue the system time
//	has already passed the time for this process to be popped out in the interrupt
//	handler. In this situation, the sleep time for the timer will be not reset and
//	only the first deadline will trigger an interrupt.
void OSCleanOverdueProcess() {
	OS_PCB *pcb_index;
	OS_PCB *pcb;
	int count = 0;

	pcb_index = OSProcTimerQueue->PCBQueueHead;

#ifndef MINIMAL_USER_PRINT
	printf("Cleaning processes that have already passed the deadline! \n");
#endif
	while(pcb_index != NULL) {
		if(pcb_index->OSPCBSleepTime <= DrvGetCurrentSysTime()) {
			pcb = OSPopHeadFromTimerQueue();
//			OSLockQueueOperation(READY_QUEUE, SUSPEND_UNTIL_LOCKED);
			OSAddToReadyQueue(pcb);
//			OSUnlockQueueOperation(READY_QUEUE, SUSPEND_UNTIL_LOCKED);
			pcb_index = OSProcTimerQueue->PCBQueueHead;
			count++;
		}
		else
			break;
	}

#ifndef MINIMAL_USER_PRINT
	printf("%d processes have been cleaned in total. \n", count);
#endif
}

// Ensure the heart of the simulator is still beating, do nothing
//		but force the system time to move forward
// Absolutely unnecessary for a real hardware
void OSKeepSimulatorAlive() {
	CALL();
}
