/*
 * memory.c
 *
 *  Created on: Nov 13, 2014
 *      Author: rdu
 */

#include	<string.h>
#include	<stdio.h>
#include	<stdlib.h>

#include	"global.h"
#include    "protos.h"
#include	"myos.h"


extern UINT16        *Z502_PAGE_TBL_ADDR;
extern INT16         Z502_PAGE_TBL_LENGTH;

extern OS_PCB				*OSCurrentRunningPCB;
extern OS_MEM_FRAME			OSMemFrameTable[PHYS_MEM_PGS];
//extern OS_MEM_FRAME_TABLE 	*OSMemFrameTable;

extern int MemPrintFlag;
extern int MemPrintLimitNum;

// Private function prototypes
// Initialize the page table for a process if it hasn't been not set yet
void OSInitializePageTable();
// Get the number of free frames
UINT16 OSGetFreeFrameNum();
// Request a frame from frame table
OS_MEM_FRAME* OSGetEmptyMemFrame();
// Select a victim page for replacing
OS_MEM_FRAME* OSSelectVictimFrame();
// Swap memory data into disk
void OSMemSwapIntoDisk(OS_MEM_FRAME *victim_frame);
// Swap disk data into memory
void OSMemSwapOutFromDisk(OS_MEM_FRAME *victim_frame, INT32 page_index);
// Lock operation to make frame table operation safe
BOOL OSLockMemoryFrame();
// Unlock operation to make frame table operation safe
BOOL OSUnlockMemoryFrame();
// Lock the id of victim frame
BOOL OSLockVictimFrame();
// Unlock the id of victim frame
BOOL OSUnlockVictimFrame();


// Handler function of memory fault
/**
 * @input status: status of the device which threw a fault
 */
void OSMemoryFaultHandler(INT32 status) {
	if(Z502_PAGE_TBL_ADDR == NULL)
		OSInitializePageTable();

	if(status < VIRTUAL_MEM_PAGES) {

		OS_MEM_FRAME *frame;

		if(OSGetFreeFrameNum() > 0) {
//			printf("Free frame available in frame table\n");
			frame = OSGetEmptyMemFrame();
		}
		else {
//			printf("No free frame available, try to find a victim frame\n");
			frame = OSSelectVictimFrame();
		}

		if(frame != NULL) {
			if(OSCurrentRunningPCB->OSPCBShadowPageTblAddr[status].ShadowPageValid == TRUE) {
//				printf("INFO: Page demanding is required!\n");
				OSMemSwapOutFromDisk(frame, status);
			}

			// set flags
			Z502_PAGE_TBL_ADDR[status] |= PAGE_TBL_VALID_MASK;
			// assign frame number
			Z502_PAGE_TBL_ADDR[status] &= PAGE_TBL_FRM_CLR_MASK;
			Z502_PAGE_TBL_ADDR[status] |= (INT16)frame->OSMemFrameID;

			OSLockMemoryFrame();

			// keep record of process information associated with this frame
			//	for reverse reference during page replacing
			frame->OSMemPageID = status;
			frame->OSMemFrameInUse = TRUE;
			if(OSCurrentRunningPCB != NULL) {
				frame->OSMemFramePCB = OSCurrentRunningPCB;
			}
			else {
				printf("\nOS_ERROR: Memory frame is associated with a NULL PCB! \n");
				printf("  System will be halted due to this fatal error!\n");

				Z502Halt();
			}

			OSUnlockMemoryFrame();
		}
		else {
			printf("\nOS_ERROR: No available frame and OS failed to steal a frame!\n");
			printf("  System will be halted due to this fatal error!\n");

			Z502Halt();
		}
	}
	else {
		printf("\nOS_ERROR: Tried to access an illegal virtual memory address! \n");
		printf("  System will be halted due to this fatal error!\n");

		Z502Halt();
	}

#ifdef ENABLE_MEMORY_PRINTER
	OSMemoryPrinter();
#endif
}

// Initialize the page table for a process if it hasn't been not set yet
void OSInitializePageTable() {
	Z502_PAGE_TBL_LENGTH = DEFAULT_PGE_TBL_LEN;
	Z502_PAGE_TBL_ADDR = (UINT16 *) calloc(sizeof(UINT16),
			Z502_PAGE_TBL_LENGTH);

	if(OSCurrentRunningPCB != NULL) {
		OSCurrentRunningPCB->OSPCBPageTblAddr = Z502_PAGE_TBL_ADDR;
	}
	else {
		printf("\nOS_ERROR: Page table is associated with a NULL PCB! \n");
		printf("  System will be halted due to this fatal error!\n");

		Z502Halt();
	}
}

// Get the number of free frames
/**
 * @output: number of free frames in frame table
 */
UINT16 OSGetFreeFrameNum() {
	int i = 0;
	UINT16 count = 0;

	OSLockMemoryFrame();

	for(i = 0; i < PHYS_MEM_PGS; i++) {
		if(OSMemFrameTable[i].OSMemFrameInUse == FALSE)
			count++;
	}

	OSUnlockMemoryFrame();

	return count;
}

// Request a frame from frame table
/**
 * output: if there is an empty frame available, return the pointer of an available
 * 			frame, otherwise return NULL
 */
OS_MEM_FRAME* OSGetEmptyMemFrame() {
	int i = 0;
	OS_MEM_FRAME* frame_index;

	OSLockMemoryFrame();

//	if(OSMemFrameTable->MemFrameEmptyNumber > 0) {
//		frame_index = OSMemFrameTable->MemFrameTableHead;
//
//		while(frame_index != NULL) {
//			if(frame_index->OSMemFrameInUse == FALSE) {
//				frame_index->OSMemFrameInUse = TRUE;
//				OSMemFrameTable->MemFrameEmptyNumber--;
//				break;
//			}
//			else
//				frame_index = frame_index->OSMemFrmNext;
//		}
//	}
//	else {
//		frame_index = NULL;
//	}

	for(i = 0; i < PHYS_MEM_PGS; i++ ) {
		if(OSMemFrameTable[i].OSMemFrameInUse == FALSE) {
			frame_index = &OSMemFrameTable[i];
			break;
		}
	}

	OSUnlockMemoryFrame();

	return frame_index;
}

// When a process is terminated, memory frames associated with this process should
//	be recycled
/**
 * input process_id: the id of the terminated process
 */
void OSRecycleMemFrame(long process_id) {
	int i = 0;

	OSLockMemoryFrame();

	// reclaim memory frames
	if(process_id == -1 && OSCurrentRunningPCB != NULL)
		process_id = OSCurrentRunningPCB->OSPCBPid;

	if(process_id >= 0) {
		for(i = 0; i < PHYS_MEM_PGS; i++) {
			if(OSMemFrameTable[i].OSMemFramePCB != NULL) {
				if(OSMemFrameTable[i].OSMemFramePCB->OSPCBPid == process_id) {
					OSMemFrameTable[i].OSMemPageID = -1;
					OSMemFrameTable[i].OSMemFrameInUse = FALSE;
					OSMemFrameTable[i].OSMemFramePCB = NULL;
				}
			}
			else {
				OSMemFrameTable[i].OSMemPageID = -1;
				OSMemFrameTable[i].OSMemFrameInUse = FALSE;
			}
		}
	}
	else if(process_id == -2) {
		for(i = 0; i < PHYS_MEM_PGS; i++) {
			OSMemFrameTable[i].OSMemPageID = -1;
			OSMemFrameTable[i].OSMemFrameInUse = FALSE;
			OSMemFrameTable[i].OSMemFramePCB = NULL;
		}
	}
	else {
		printf("OS_ERROR: Failed to recycles memory with an invalid process id! \n");
	}

	OSUnlockMemoryFrame();
}

// Select a victim page for replacing, refer to the report for more details of
//		the selection policy
/**
 * @output: this function will search for the best victim from the frame table
 * 				and return the reference of the victim frame
 */
OS_MEM_FRAME* OSSelectVictimFrame() {
	OS_MEM_FRAME *victim_frame;
	static int VictimID = -1;

	OSLockMemoryFrame();
	OSLockVictimFrame();
	VictimID++;
	if(VictimID == 64)
		VictimID = 0;
	if(VictimID >=0 && VictimID < PHYS_MEM_PGS)
		victim_frame = &OSMemFrameTable[VictimID];
	else {
		printf("\n\n Fatal Error, victim id illegal! \n\n");
		Z502Halt();
	}
	OSUnlockVictimFrame();
	OSUnlockMemoryFrame();

	if(OSMemFrameTable[VictimID].OSMemFrameInUse == TRUE)
		OSMemSwapIntoDisk(victim_frame);
	else
		printf("OS_INFO: No need to swap data in memory to disk\n");

#ifdef PAGING_DEBUG_INFO
	printf("Frame ID %ld is selected as the victim\n",victim_frame->OSMemFrameID);
#endif

	OSLockMemoryFrame();
	// Label frame as available
	if(victim_frame->OSMemPageID < 0 || victim_frame->OSMemPageID >= DEFAULT_PGE_TBL_LEN) {
//		printf("\n\n Fatal Error: illegal page number %ld in frame entry\n\n", victim_frame->OSMemPageID);
//
//		printf("Frame ID %ld is selected as the victim\n",victim_frame->OSMemFrameID);
//		printf("  Frame information: usage - %d, page number - %ld \n",
//				OSMemFrameEntries[VictimID].OSMemFrameInUse,
//				OSMemFrameEntries[VictimID].OSMemPageID);
//
//		Z502Halt();
	}
	else
		victim_frame->OSMemFramePCB->OSPCBPageTblAddr[victim_frame->OSMemPageID] &= PAGE_TBL_INVALID_MASK;

//	VictimID = (VictimID + 1) % PHYS_MEM_PGS;
//	VictimID++;
//	if(VictimID == 64)
//		VictimID = 0;

	if(victim_frame == NULL) {
		printf("\n\n Fatal Error, victim frame pointer cannot be NULL \n\n)");
		Z502Halt();
	}

	OSUnlockMemoryFrame();

//	printf("Frame ID %ld is selected as the victim\n",victim_frame->OSMemFrameID);

	return victim_frame;
}

// Swap memory data into disk, this function first read data out from the
//		memory location, then write data to disk
void OSMemSwapIntoDisk(OS_MEM_FRAME *victim_frame) {
	long frame_id;
	long disk_id, sector;
	int i = 0;
	int attempt_time = 3;
	BOOL attempt_result = FALSE;
	char data[PGSIZE];
	char data_out[PGSIZE];

	// Initialize data and data_out to make them different
	for(i = 0 ; i < PGSIZE; i++) {
		data[i] = 0;
		data_out[i] = 1;
	}

	// Read data out from memory
	OSLockMemoryFrame();

	frame_id = victim_frame->OSMemFramePCB->OSPCBPageTblAddr[victim_frame->OSMemPageID] & PAGE_TBL_FRAME_MASK;
	if(frame_id >= 0 && frame_id < PHYS_MEM_PGS)
		Z502ReadPhysicalMemory(frame_id, data);

	// Find a place on the disk to store the memory page
	if(victim_frame->OSMemFramePCB != NULL) {
		disk_id = victim_frame->OSMemFramePCB->OSPCBPid + 1;
		sector = victim_frame->OSMemPageID;
	}
	else {
		printf("\n\n Fatal Error \n\n");
		Z502Halt();
	}

	OSUnlockMemoryFrame();

#ifdef PAGING_DEBUG_INFO
	printf("Data to be swapped in disk %ld, sector %ld: from frame %ld \n",disk_id,sector, frame_id);
	for(i=0;i<PGSIZE;i++){
		if(data[i]<10)
			printf(" %d ",data[i]);
		else
			printf("%d ",data[i]);
	}
	printf("\n");
#endif

	// Send a disk request
//	OSDiskWriteRequestNoSuspend(disk_id,sector,data);
	do {
		OSDiskWriteRequest(disk_id,sector,data);
		OSProcessDispatch();

		OSDiskReadRequest(disk_id,sector,data_out);
		OSProcessDispatch();

		// check if memory data is correctly stored in disk
		for(i=0;i<PGSIZE;i++){
			if(data[i] != data_out[i]) {
				attempt_time --;
				break;
			}
			else
				attempt_result = TRUE;
		}

		if(attempt_result == TRUE)
			break;
	} while(attempt_time > 0);

	if(attempt_time == 0 && attempt_result == FALSE) {
		printf("\n\n Fatal Error: memory page not successfully swapped in disk \n\n");
		Z502Halt();
	}

	OSLockMemoryFrame();
	// Record where the page content is stored in the disk in shadow page table
	if(victim_frame->OSMemFramePCB != NULL) {
		victim_frame->OSMemFramePCB->OSPCBShadowPageTblAddr[victim_frame->OSMemPageID].ShadowPageValid = TRUE;
		victim_frame->OSMemFramePCB->OSPCBShadowPageTblAddr[victim_frame->OSMemPageID].ShadowPageDiskID = disk_id;
		victim_frame->OSMemFramePCB->OSPCBShadowPageTblAddr[victim_frame->OSMemPageID].ShadowPageSectorID = sector;
	}
	OSUnlockMemoryFrame();
}

// Swap memory data back from disk, when this function is called, all aspects
//		of the victim frame should have been taken care of
void OSMemSwapOutFromDisk(OS_MEM_FRAME *victim_frame, INT32 page_index) {
	long frame_id;
	long disk_id;
	long sector;
	int i = 0;
	char data[PGSIZE];
	char data_check[PGSIZE];

	// Initialize data and data_out to make them different
	for(i = 0 ; i < PGSIZE; i++) {
		data[i] = 0;
		data_check[i] = 1;
	}

	frame_id = victim_frame->OSMemFrameID;
	if(OSCurrentRunningPCB != NULL)
		OSCurrentRunningPCB->OSPCBShadowPageTblAddr[page_index].ShadowPageValid = FALSE;
	else {
		printf("\n\nINFO: Fatal Error, victim frame is associated with a NULL PCB!\n\n");
		Z502Halt();
	}

//#ifdef PAGING_DEBUG_INFO
//	printf("SWAP INFO: Read data from disk %ld, section %ld \n",
//			OSCurrentRunningPCB->OSPCBShadowPageTblAddr[page_index].ShadowPageDiskID,
//			OSCurrentRunningPCB->OSPCBShadowPageTblAddr[page_index].ShadowPageSectorID);
//#endif

	disk_id = OSCurrentRunningPCB->OSPCBShadowPageTblAddr[page_index].ShadowPageDiskID;
	sector = OSCurrentRunningPCB->OSPCBShadowPageTblAddr[page_index].ShadowPageSectorID;

	if(disk_id < 0 || disk_id > MAX_NUMBER_OF_DISKS || sector < 0 || sector >= NUM_LOGICAL_SECTORS) {
		printf("\n\nINFO: Fatal Error, illegal record read from shadow table!\n\n");
		Z502Halt();
	}

	// Swap data out from disk
	OSDiskReadRequest(disk_id,sector, data);
	OSProcessDispatch();

	// Write data back to memory
	Z502WritePhysicalMemory(frame_id, data);

	Z502ReadPhysicalMemory(frame_id, data_check);

	// check if disk data is correctly restroed to memory
	for(i=0;i<PGSIZE;i++){
		if(data[i] != data_check[i]) {
			printf("\n\nINFO: Fatal Error, wrong disk data restored to memory!\n\n");
			Z502Halt();
		}
	}

#ifdef PAGING_DEBUG_INFO
//	int i = 0;
	char read_data[PGSIZE];
	Z502ReadPhysicalMemory(frame_id, read_data);

	printf("Data now resides in memory page %d: \n",page_index);
	for(i=0;i<PGSIZE;i++){
		if(read_data[i]<10)
			printf(" %d ",read_data[i]);
		else
			printf("%d ",read_data[i]);
	}
	printf("\n");
#endif
}

// Print out status of physical memory of Z502
void OSShowMemFrameStatus() {
	int i,row,collum;

	for(i = 0; i < 16*3-1; i++) {
			printf("*");
	}

	printf("\n");

	for(i = 0; i < 16; i++) {
		if(i<10)
			printf("0%d ",i);
		else
			printf("%d ",i);
	}

	printf("\n");

	for(i = 0; i < 16*3-1; i++) {
		printf("-");
	}

	printf("\n");

	for(row = 0; row < 4; row++) {
		for(collum = 0; collum < 16; collum++) {
			if(OSMemFrameTable[row*16+collum].OSMemFrameInUse == TRUE)
				printf(" 1 ");
			else
				printf(" 0 ");
		}
		printf("\n");
	}

	for(i = 0; i < 16*3-1; i++) {
		printf("-");
	}

	printf("\n");

	printf("Summary: %d of %d frames are free.\n",OSGetFreeFrameNum(), PHYS_MEM_PGS);

	for(i = 0; i < 16*3-1; i++) {
		printf("*");
	}
	printf("\n");
}

// Print out memory contents
void OSShowPhysicalMemory(int page) {
	int i=0;
	char data[PGSIZE];

	Z502ReadPhysicalMemory(page, data);

//	printf("\n");
	for(i = 0; i < 16*3+14; i++) {
		printf("-");
	}
	printf("\n");
	printf("memory page %d: ",page);
	for(i=0;i<PGSIZE;i++){
		if(data[i]<10)
			printf(" %d ",data[i]);
		else
			printf("%d ",data[i]);
	}

	printf("\n");
	for(i = 0; i < 16*3+14; i++) {
		printf("-");
	}
	printf("\n");
}

// Print out page table of current process
void OSShowPageTable() {
	int i = 0;
	INT16 valid_bit;
	printf("\n");
	for(i = 0; i < 16+14; i++) {
		printf("-");
	}
	printf("\n");
	for(i = 0; i < 1024; i++) {
		valid_bit = Z502_PAGE_TBL_ADDR[i] & PAGE_TBL_VALID_MASK;
		if(valid_bit!=0) {
			if(i<10)
				printf("page entry 0%d: frame id - %d\n",i,Z502_PAGE_TBL_ADDR[i]&PAGE_TBL_FRAME_MASK);
			else
				printf("page entry %d: frame id - %d\n",i,Z502_PAGE_TBL_ADDR[i]&PAGE_TBL_FRAME_MASK);
		}
	}
	for(i = 0; i < 16+14; i++) {
		printf("-");
	}
	printf("\n");

//	for(i=0;i<8;i++) {
//		printf("frame %d: id - %d \n",i,OSMemFrameEntries[i].OSMemFrameID);
//	}
}

//  Use memory printer to print information of memory
void OSMemoryPrinter() {
	int i = 0;
	UINT16 mem_state_bits = 0;
	static UINT32 print_count = 0;
	BOOL enable_print = FALSE;

	if(MemPrintFlag == MEM_PRINT_FULL || (MemPrintFlag == MEM_PRINT_LIMIT && (print_count % MemPrintLimitNum) == 0))
		enable_print = TRUE;
	else
		enable_print = FALSE;

	OSLockMemoryFrame();

	if(enable_print == TRUE) {
		for (i = 0; i < PHYS_MEM_PGS; i++) {
			if (OSMemFrameTable[i].OSMemFrameInUse == TRUE) {
				mem_state_bits |= (Z502_PAGE_TBL_ADDR[OSMemFrameTable[i].OSMemPageID] & PAGE_TBL_FRM_CLR_MASK);

				// only keep the highest 3 bits
				mem_state_bits = mem_state_bits >> 13;

				MP_setup((INT32)OSMemFrameTable[i].OSMemFrameID,(INT32)OSMemFrameTable[i].OSMemFramePCB->OSPCBPid,
						(INT32)OSMemFrameTable[i].OSMemPageID,(INT32)mem_state_bits);

//				printf("frame %d is added, entry value: %d, state value: %d \n",i,
//						Z502_PAGE_TBL_ADDR[OSMemFrameEntries[i].OSMemPageID],mem_state_bits);
			}
		}

		MP_print_line();
		printf("\n");
	}
	print_count++;

	OSUnlockMemoryFrame();
}

// Lock operation to make frame table operation safe
/**
 * @output: lock result
 */
BOOL OSLockMemoryFrame() {
	BOOL LockResult;

	LockResult = FALSE;

	READ_MODIFY(MEM_FRAME_LOCK, 1, SUSPEND_UNTIL_LOCKED, &LockResult);

	return LockResult;
}

// Unlock operation to make frame table operation safe
/**
 * @output: unlock result
 */
BOOL OSUnlockMemoryFrame() {
	BOOL LockResult;

	LockResult = FALSE;

	READ_MODIFY(MEM_FRAME_LOCK, 0, SUSPEND_UNTIL_LOCKED, &LockResult);

	return LockResult;
}

// Lock the id of victim frame
/**
 * @output: unlock result
 */
BOOL OSLockVictimFrame() {
	BOOL LockResult;

	LockResult = FALSE;

	READ_MODIFY(MEM_VICTIM_LOCK, 1, SUSPEND_UNTIL_LOCKED, &LockResult);

	return LockResult;
}

// Unlock the id of victim frame
/**
 * @output: unlock result
 */
BOOL OSUnlockVictimFrame() {
	BOOL LockResult;

	LockResult = FALSE;

	READ_MODIFY(MEM_VICTIM_LOCK, 0, SUSPEND_UNTIL_LOCKED, &LockResult);

	return LockResult;
}
