/*
 * scheduling.c
 *
 *  Created on: Sep 28, 2014
 *      Author: rdu
 */

#include	<stdio.h>
#include	<string.h>

#include	"global.h"
#include	"protos.h"
#include	"hardware_driver.h"
#include	"myos.h"

//extern OS_PCB 			OSProcessEmptyList[MAX_PROCESS_NUMBER];
extern OS_PCB			*OSCurrentRunningPCB;
//extern OS_PCB_QUEUE 	*OSProcEmptyList;
extern OS_PCB_QUEUE		*OSProcReadyQueue;
extern OS_PCB_QUEUE		*OSProcTimerQueue;
extern OS_PCB_QUEUE		*OSProcSuspendQueue;
extern INT32 			OSProcessNumber;
extern OS_DISK_TASK_LIST *OSDiskTaskList[MAX_NUMBER_OF_DISKS];

extern int SchedulePrintFlag;

// Private function prototypes
// This function is only used to learn how to use scheduler printer
void OSPrintSchedulerTest();

// Prepare everything for the next ready process and give control
//	to this process to start running
void OSProcessDispatch() {
	void *next_context;

#ifndef MINIMAL_USER_PRINT
	printf("system time (checked from process dispatcher): %d \n",DrvGetCurrentSysTime());
#endif

	while(OSCurrentRunningPCB == NULL &&
			OSProcReadyQueue->PCBQueueHead == NULL) {
		CALL( OSProcessWasteTime() );
//		printf("OS INFO: System is wasting time waiting for the next ready process!\n");
		if(OSProcessNumber == OSProcSuspendQueue->QueueLength) {
			int i;
			BOOL check_disk_busy;

			check_disk_busy = FALSE;
			for(i=0;i<MAX_NUMBER_OF_DISKS;i++) {
//				printf("Disk Task List %d Length: %d\n",i, OSDiskTaskList[i]->TaskListLength);
//				if(DrvCheckIfDiskIdle(i) != TRUE) {
				if(OSDiskTaskList[i]->TaskListLength != 0) {
					check_disk_busy = TRUE;
//					printf("disk %d is busy \n",i+1);
					break;
				}
			}

			if(check_disk_busy == FALSE) {
				INT32 error;
				printf("\n");
				printf("OS WARNNING: Only suspended processes exist in OS, system will be halted!!! \n");
				OSTerminateProcess(-2, &error);
			}
		}
	}

#ifndef MINIMAL_USER_PRINT
	printf("A process will be dispatched...\n");
#endif

	if(OSCurrentRunningPCB != NULL) {

//		OSLockQueueOperation(READY_QUEUE, SUSPEND_UNTIL_LOCKED);
		if( OSProcReadyQueue->PCBQueueHead != NULL) {
			if(OSCurrentRunningPCB->OSPCBPriority > OSProcReadyQueue->PCBQueueHead->OSPCBPriority) {

				OSLockQueueOperation(READY_QUEUE, SUSPEND_UNTIL_LOCKED);

				OSAddToReadyQueue(OSCurrentRunningPCB);
				OSCurrentRunningPCB = OSPopHeadFromReadyQueue();

				next_context = OSCurrentRunningPCB->OSPCBCtx;

				OSUnlockQueueOperation(READY_QUEUE, SUSPEND_UNTIL_LOCKED);

#ifdef ENABLE_RDU_SCHEDULER_PRINTER
				OSShowAllProcessInfo();
#endif
#ifdef ENABLE_SCHEDULER_PRINTER
				if(OSCurrentRunningPCB != NULL)
					OSSchedulerPrinter(OSCurrentRunningPCB->OSPCBPid, SP_ACTION_DISPATCH, TRUE);
#endif

				Z502SwitchContext(SWITCH_CONTEXT_SAVE_MODE, &next_context);

//				printf("****************system come back**********************\n");
			}
		}
	}
	else if( OSProcReadyQueue->PCBQueueHead != NULL){

		OSLockQueueOperation(READY_QUEUE, SUSPEND_UNTIL_LOCKED);

		OSCurrentRunningPCB = OSPopHeadFromReadyQueue();

		next_context = OSCurrentRunningPCB->OSPCBCtx;

		OSUnlockQueueOperation(READY_QUEUE, SUSPEND_UNTIL_LOCKED);

#ifdef ENABLE_RDU_SCHEDULER_PRINTER
		OSShowAllProcessInfo();
#endif
#ifdef ENABLE_SCHEDULER_PRINTER
		if(OSCurrentRunningPCB != NULL)
			OSSchedulerPrinter(OSCurrentRunningPCB->OSPCBPid, SP_ACTION_DISPATCH, TRUE);
#endif

		Z502SwitchContext(SWITCH_CONTEXT_SAVE_MODE, &next_context);

//		printf("****************system come back**********************\n");
	}
}

// This function is called when there is no process on ready queue to run
void OSProcessWasteTime() {
	long i = 0, j = 0;

//	printf("OS WARNING: Wasting time to wait for the next ready process!\n");
	for(i=0; i< 500; i++){
		for(j=0; j< 500; j++){
			// do nothing
		}
	}
//	OSShowAllProcessInfo();
//	CALL( OSProcessDispatch() );
}


// Add lock to timer/ready queue, this function and the following unlock function
//	are just the wrapper of READ_MODIFY() to make it more convenient for use
// Possible function calls:
// 			OSLockQueueOperation(READY_QUEUE, SUSPEND_UNTIL_LOCKED);
//			OSUnlockQueueOperation(READY_QUEUE, SUSPEND_UNTIL_LOCKED);
//
//			OSLockQueueOperation(TIMER_QUEUE, SUSPEND_UNTIL_LOCKED);
//			OSUnlockQueueOperation(TIMER_QUEUE, SUSPEND_UNTIL_LOCKED);
/**
 * @input queue_type: READY_QUEUE or TIMER_QUEUE
 * @input lock_type: SUSPEND_UNTIL_LOCKED or DO_NOT_SUSPEND
 */
BOOL OSLockQueueOperation(INT32 queue_type, BOOL lock_type) {
	INT32 LockResult;

	if(queue_type == TIMER_QUEUE) {
		READ_MODIFY(TIMER_QUEUE_LOCK, 1, lock_type, &LockResult);

#ifndef MINIMAL_USER_PRINT
		if(LockResult)
			printf("Timer queue locked!\n");
		else
			printf("Timer queue failed to lock!\n");
#endif

		return LockResult;
	}
	else if(queue_type == READY_QUEUE) {
		READ_MODIFY(READY_QUEUE_LOCK, 1, lock_type, &LockResult);

#ifndef MINIMAL_USER_PRINT
		if(LockResult)
			printf("Ready queue locked!\n");
		else
			printf("Ready queue failed to lock!\n");
#endif

		return LockResult;
	}
	else if(queue_type == SUSPEND_QUEUE) {
		READ_MODIFY(SUSPEND_QUEUE_LOCK, 1, lock_type, &LockResult);

		return LockResult;
	}
	else {
		printf("OS ERROR: wrong parameters are passed in lock function!\n");
		return FALSE;
	}
}

// Remove lock to timer/ready queue
// Possible function calls:
// 			OSLockQueueOperation(READY_QUEUE, SUSPEND_UNTIL_LOCKED);
//			OSUnlockQueueOperation(READY_QUEUE, SUSPEND_UNTIL_LOCKED);
//
//			OSLockQueueOperation(TIMER_QUEUE, SUSPEND_UNTIL_LOCKED);
//			OSUnlockQueueOperation(TIMER_QUEUE, SUSPEND_UNTIL_LOCKED);
BOOL OSUnlockQueueOperation(INT32 queue_type, BOOL lock_type) {
	INT32 LockResult;

	// TODO check if locked before doing unlock? not sure

	if(queue_type == TIMER_QUEUE) {
		READ_MODIFY(TIMER_QUEUE_LOCK, 0, lock_type, &LockResult);

#ifndef MINIMAL_USER_PRINT
		if(LockResult)
			printf("Timer queue unlocked!\n");
		else
			printf("Timer queue failed to unlock!\n");
#endif

		return LockResult;
	}
	else if(queue_type == READY_QUEUE) {
		READ_MODIFY(READY_QUEUE_LOCK, 0, lock_type, &LockResult);

#ifndef MINIMAL_USER_PRINT
		if(LockResult)
			printf("Ready queue unlocked!\n");
		else
			printf("Ready queue failed to unlock!\n");
#endif

		return LockResult;
	}
	else if(queue_type == SUSPEND_QUEUE) {
		READ_MODIFY(SUSPEND_QUEUE_LOCK, 0, lock_type, &LockResult);

		return LockResult;
	}
	else {
		printf("OS ERROR: wrong parameters are passed in lock function!\n");
		return FALSE;
	}
}

// This function is only used to learn how to use scheduler printer
void OSPrintSchedulerTest() {
	int j = 0;
	BOOL enable_print = FALSE;
	static INT32 print_count = 0;

	if(SchedulePrintFlag == SCHED_PRINT_FULL || (SchedulePrintFlag == SCHED_PRINT_LIMIT && print_count % 20 == 0))
		enable_print = TRUE;
	else
		enable_print = FALSE;

	print_count++;

	if(enable_print == TRUE) {
		OSLockQueueOperation(TIMER_QUEUE, SUSPEND_UNTIL_LOCKED);
		OSLockQueueOperation(READY_QUEUE, SUSPEND_UNTIL_LOCKED);

		// copied from sample.c
		CALL(SP_setup( SP_TIME_MODE, 99999 ));
		CALL(SP_setup_action( SP_ACTION_MODE, "CREATE" ));
		CALL(SP_setup( SP_TARGET_MODE, 99L ));
		CALL(SP_setup( SP_RUNNING_MODE, 99L ));
		for (j = 0; j < SP_MAX_NUMBER_OF_PIDS ; j++)
			CALL(SP_setup( SP_READY_MODE, j ));
		for (j = 0; j < SP_MAX_NUMBER_OF_PIDS ; j++)
			CALL(SP_setup( SP_TIMER_SUSPENDED_MODE, j+20 ));
		for (j = 0; j < SP_MAX_NUMBER_OF_PIDS ; j++)
			CALL(SP_setup( SP_PROCESS_SUSPENDED_MODE, j+40 ));
		for (j = 0; j < SP_MAX_NUMBER_OF_PIDS ; j++)
			CALL(SP_setup( SP_DISK_SUSPENDED_MODE, j+60 ));
		for (j = 0; j < SP_MAX_NUMBER_OF_PIDS ; j++)
			CALL(SP_setup( SP_TERMINATED_MODE, j+80 ));
		for (j = 0; j < SP_MAX_NUMBER_OF_PIDS ; j++)
			CALL(SP_setup( SP_NEW_MODE, j+50 ));
		CALL(SP_print_line());

		OSUnlockQueueOperation(READY_QUEUE, SUSPEND_UNTIL_LOCKED);
		OSUnlockQueueOperation(TIMER_QUEUE, SUSPEND_UNTIL_LOCKED);
	}
}

// Use scheduler printer to print information of processes
/**
 * @input pcb: the OS_PCB to be logged
 * @input action: the action to be logged,
 * @input enable_printf: if TRUE, print log, otherwise, just do logging
 * @input print_info: decide if the info logged will be print out or not
 */
void OSSchedulerPrinter(long pid, char *action, BOOL print_info) {
	BOOL enable_print = FALSE;
	static long print_count = 0;

	if(SchedulePrintFlag == SCHED_PRINT_FULL || (SchedulePrintFlag == SCHED_PRINT_LIMIT && print_count < SCHED_PRINT_LIM_NUM))
		enable_print = TRUE;
	else
		enable_print = FALSE;

	print_count++;
	// avoid variable overflow
	if(print_count == 65535)
		print_count = 65535;

	if(enable_print == TRUE) {

		OS_PCB *pcb_index;

		if(strcmp(action, SP_ACTION_DISPATCH) == 0) {
			if(OSCurrentRunningPCB != NULL) {
				if(OSCurrentRunningPCB->SPPrintDispatchCount < SP_MAX_DISPATCH_PRINT)
					OSCurrentRunningPCB->SPPrintDispatchCount++;
				else
					return;
			}
		}
		else if(strcmp(action, SP_ACTION_SUSPEND) == 0 || strcmp(action,SP_ACTION_CHANGEPRIO)) {
			OS_PCB *pcb;
			INT32 source;

			pcb = OSSearchForProcess(pid, &source);

			if(pcb != NULL) {
				pcb->SPPrintDispatchCount = 0;
			}
		}

		OSLockQueueOperation(TIMER_QUEUE, SUSPEND_UNTIL_LOCKED);
		OSLockQueueOperation(READY_QUEUE, SUSPEND_UNTIL_LOCKED);
		//	OSLockQueueOperation(SUSPEND_QUEUE, SUSPEND_UNTIL_LOCKED);

		// log current time
		CALL(SP_setup(SP_TIME_MODE, DrvGetCurrentSysTime()));

		// log current event


		CALL(SP_setup_action(SP_ACTION_MODE, action));
		CALL(SP_setup(SP_TARGET_MODE, pid));

		// log current running process
		if(OSCurrentRunningPCB != NULL) {
			CALL(SP_setup( SP_RUNNING_MODE, OSCurrentRunningPCB->OSPCBPid));
		}
		else {
			// if no process is running, pass in the maximum PID number allowed
			CALL(SP_setup( SP_RUNNING_MODE, 99));
		}

		// log ready queue
		pcb_index = OSProcReadyQueue->PCBQueueHead;
		while(pcb_index != NULL) {
			CALL(SP_setup( SP_READY_MODE, pcb_index->OSPCBPid ));
			pcb_index = pcb_index->OSPCBNext;
		}

		// log timer queue
		pcb_index = OSProcTimerQueue->PCBQueueHead;
		while(pcb_index != NULL) {
			CALL(SP_setup( SP_TIMER_SUSPENDED_MODE, pcb_index->OSPCBPid ));
			pcb_index = pcb_index->OSPCBNext;
		}

		// log suspend queue
		pcb_index = OSProcSuspendQueue->PCBQueueHead;
		while(pcb_index != NULL) {
			CALL(SP_setup( SP_PROCESS_SUSPENDED_MODE, pcb_index->OSPCBPid ));
			pcb_index = pcb_index->OSPCBNext;
		}

		if(print_info == TRUE) {
			printf("/*************** print start ******************/ \n");
			//		printf("---------------- print start ------------------- \n");
			CALL(SP_print_line());
			printf("----------------------------------------------- \n");
		}

		OSUnlockQueueOperation(READY_QUEUE, SUSPEND_UNTIL_LOCKED);
		OSUnlockQueueOperation(TIMER_QUEUE, SUSPEND_UNTIL_LOCKED);
		//	OSUnlockQueueOperation(SUSPEND_QUEUE, SUSPEND_UNTIL_LOCKED);
	}
}

