CS502 Operating System
========

By Ruixiang Du ([http://rdu.im](http://rdu.im))

This project is configured to be capable of cross-platform development (in Windows/Linux/MacOSX) using GNU toolchain. The project should compile without any change in the code in all types of host operating systems (see development note b for more details). The main development environment used is Ubuntu 12.04 64bit. MAC OS X is the secondary development platform. Windows is rarely used.

## 1. Repository structure ##

* **origin**: unmodified code downloaded from the course website
* **test0**: test0 configured and tested in Eclipse as well as in the shell
* **myOS**: test1 and test2 are implemented in this folder
* **project01**: deliverables from phase 1 of the project
* **project02**: deliverables from phase 2 of the project

## 2. Organization of myOS

* **simulator**: implementation of simulator z502
* **driver**: drivers for the timer and disk
* **os**: operating system
* **lib**: possible libraries used in the os
* **usr**: user code to do testing

## 3. Changes made to the original code

**Unchanged files**: 

* z502.c 
* z502.h 
* syscalls.h
* state_printer.c
* sample.c
* protos.h

**Files that are not intended to be changed**

* global.h: added preprocessor to make the code compile in different host operating systems without the need to change code

**Files that are intended to be changed**

* base.c
* test.c

## 4. Development notes ##

**a. How to configure the project in Eclipse in Linux**

According to the given command:

```
$ gcc -g *.c -lm -lpthread -o os
```
we can see we need to link the math library(-lm) and the pthread library(-lpthread), thus we also need to tell Eclipse to do so.

* ${Project Name} -> Properties -> C/C++ Build -> Settings -> GCC C Linker -> Libraries
* under the "Libraries" section, add "pthread" and "m"

then the code should be able to be compile and linked.

**b. Detection of host operating system to make code cross-platform**

In file global.h, conditional compilation is added to define different variables according to the detected OS type. Currently it has been tested in Windows 8.1 64bit, MAC OS X Mavericks and Ubuntu 12.04 64bit. Only GNU toolchain is tested. In Windows, MinGW is used to provide GNU tool set. This code snippet might not work properly with other toolchains such as the one provided by VisualStudio.

**Note**: The code doesn't need to be changed for successful compilation no matter which host OS is using. However, some changes in the project configuration of Eclipse need to be applied if the development is done in windows. The following steps should make it work: **1**. Change the toolchain to be MinGW GCC from Linux GCC at "${Project Name} -> Properties -> C/C++ Build -> Tool Chain Editor". **2**. Delete "pthread" in the place mentioned above in note a.

**c. Advantages of declaring a array and linking them as a doubly linked list**

Memory allocation and free can be avoided when a new node is created or a exsiting node is deleted for a list.


## 5. TODO list ##

**Implement a simple shell**[tutorial](http://www.gnu.org/software/libc/manual/html_node/Implementing-a-Shell.html) -- finished

**Check if all processes have finished** -- finished

**restore code change to test1a, test1b** -- finished

**Finish termination and suspend of processes in timer queue**

**Make sure nothing is locked when a process is suspended** -- finished

**Because of lock, the interrupt handler is blocked and when it's unblcked, a wrong element in timer queue is popped out** -- finished

**Clean up printer calls for different tests** -- finished

**Free memory allocated for messages**

**Write a makefile for the project to compile independently** -- finished

**Make sure the SendMessage() function doesn't resume a process in timer queue** -- finished

**Update Timer driver so that each function is atomic**

## 6. Notes for report ##

Phase 1

**Cross-platform compilation**

**PCB creation method**

**Layered design**

**Deadlock problem**

1. Lock variables with the same order at different place
2. Always use lock and unlock in pair, don't put any one of the pair into a if condition

Phase 2

Disk operations registers may be shared by more than one thread since driver functions may be called from either the interrupt handler or the system call. So each function from disk driver must be atomic.

1. Create a paging replacement algorithm that steal frame from a low priority process