/*
 * driver.h
 *
 *  Created on: Sep 11, 2014
 *      Author: rdu
 */

#ifndef DRIVER_H_
#define DRIVER_H_

#include "global.h"

#define TIMER_RESET_SUCCESS			 1
#define	TIMER_RESET_FAILURE			-1
#define TIMER_RESET_ILLEGAL_PARAM	-2


/*------------------ Driver Functions of Timer ------------------*/

// Communicate with timer and get current system time
void DrvGetTimeOfDay(INT32 *time);

// Return current system time, internally works in the same way
//	with DrvGetTimeOfDay(), but provides different access interface
INT32 DrvGetCurrentSysTime();

// Set sleep time to timer, used when only single process runs like
//	the situation in test0
void DrvSetSleepTime(long sleep_time);

// Rest sleep time, this functions is to be called after one interrupt
//	is triggered and the timer queue is not empty. Z502_Idle() is not
//	called inside this function
// Function return values: TIMER_RESET_SUCCESS, TIMER_RESET_FAILURE
//						   TIMER_RESET_ILLEGAL_PARAM
INT32 DrvResetTimer(long sleep_time);

// Similar to DrvResetTimer(), but the parameter passed in is the absolute system
//	time when the interrupt needs to be triggered instead of the relative sleep time
INT32 DrvResetTimerToSysTime(long sys_time);

// Check if timer is available, this function will be called when timer queue
//	is empty and the first process wants to sleep and set a sleep time for timer.
//	Afterwards, the loading work will be taken care of in interrupt handler
BOOL OSCheckIfTimerIdle();

/*------------------ Driver Functions of Disk -------------------*/




#endif /* DRIVER_H_ */
