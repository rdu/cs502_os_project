/*
 * task.c
 *
 *  Created on: Sep 11, 2014
 *      Author: rdu
 */

#include	"string.h"
#include 	 <stdlib.h>
#include	 <assert.h>

#include 	"myos.h"
#include	"pcbqueue.h"
#include	"protos.h"

extern OS_PCB			*OSCurrentRunningPCB;
extern OS_PCB_QUEUE 	*OSProcEmptyList;
extern OS_PCB_QUEUE		*OSProcReadyQueue;
extern OS_PCB_QUEUE		*OSProcTimerQueue;
extern OS_PCB_QUEUE		*OSProcSuspendQueue;
extern INT32 			OSProcessNumber;

// Private function prototypes
// Delete a process, free memory allocated for member variables
void OSDeleteProcess(OS_PCB *pcb);
// Delete current process, free memory allocated for member variables
void OSDeleteCurrentProcess();
// Add a process to suspend queue
void OSAddToSuspendQueue(OS_PCB *pcb);
// Remove a process from suspend queue
BOOL OSRemoveFromSuspendQueue(OS_PCB *pcb);

// Create a process
/**
 * @input proc_name: a string used to specify the name of the new process
 * @input entry_address: entry address of the new process
 * @input pid: the pid of the new process will be stored in this variable as a return
 * @input result: result of the creation operation
 */
OS_PCB* OSCreateProcess(char *proc_name, void *entry_address, long priority, INT32 *pid, INT32 *result) {

	OS_PCB *pcb;

	if(OSProcessNumber < MAX_PROCESS_NUMBER) {

		if(priority > 0) {

			if(OSGetProcessID(proc_name, result) < 0) {

				OSLockQueueOperation(READY_QUEUE, SUSPEND_UNTIL_LOCKED);

				pcb = PopFromPCBQueueHead(OSProcEmptyList);					// get pcb from empty list
				pcb->OSPCBPid = OSProcessNumber;							// set pid
				pcb->OSPCBProcName = (char *)malloc(strlen(proc_name) + 1);
				assert(pcb->OSPCBProcName != NULL);
				strcpy(pcb->OSPCBProcName,proc_name);						// process name
				Z502MakeContext(&pcb->OSPCBCtx,entry_address, USER_MODE);	// process context
				pcb->OSPCBSleepTime = 0;										// process wait time
				pcb->OSPCBPriority = priority; 								// process priority

				OSAddToReadyQueue(pcb);

				OSProcessNumber++;

				*pid = pcb->OSPCBPid;

				OSUnlockQueueOperation(READY_QUEUE, SUSPEND_UNTIL_LOCKED);

#ifdef ENABLE_SCHEDULER_PRINTER
				OSSchedulerPrinter(pcb->OSPCBPid, SP_ACTION_CREATE, TRUE);
#endif

				*result = ERR_SUCCESS;
			}
			else {
				pcb = NULL;
				*pid = -1;
				*result = ERR_BAD_PARAM;
				printf("OS ERROR: A process with the same name has already existed! \n");
			}
		}
		else {
			pcb = NULL;
			*pid = -1;
			*result = ERR_BAD_PARAM;
			printf("OS ERROR: The priority cannot be a negative number!\n");
		}

		return pcb;

	} else {
		printf("OS ERROR: System has reached the maximum number of processes!\n");

		*pid = -1;
		*result = ERR_BAD_PARAM;

		return NULL;
	}
}

// Suspend a process
/**
 * @input process_id: the pid of the process to be suspended
 * @input error: the operation result will be stored in the variable which this
 * 					error pointer points to
 */
void OSSuspendProcess(long process_id, INT32 *error) {
	if(process_id > 0) {
		OS_PCB *pcb;
		INT32 queue;
		BOOL result;

		pcb = OSSearchForProcess(process_id, &queue);

		// suspend a process if this process is in ready queue
		if(queue == READY_QUEUE) {
			OSLockQueueOperation(READY_QUEUE, SUSPEND_UNTIL_LOCKED);
			result = OSRemoveFromReadyQueue(pcb);
			OSUnlockQueueOperation(READY_QUEUE, SUSPEND_UNTIL_LOCKED);

			if(result) {
				pcb->OSPCBSleepTime = 0;
//				OSLockQueueOperation(SUSPEND_QUEUE, SUSPEND_UNTIL_LOCKED);
				OSAddToSuspendQueue(pcb);
//				OSUnlockQueueOperation(SUSPEND_QUEUE, SUSPEND_UNTIL_LOCKED);

				*error = ERR_SUCCESS;
			}
			else {
				*error = ERR_BAD_PARAM;
			}
		}
		else if(queue == TIMER_QUEUE) {
			// TODO find a way to deal with this situation properly
			printf("process found in timer queue \n");
			*error = ERR_BAD_PARAM;
//			OSLockQueueOperation(TIMER_QUEUE, SUSPEND_UNTIL_LOCKED);
//			pcb->OSPendingOperation = OP_SUSPEND_PROC;
//			OSUnlockQueueOperation(TIMER_QUEUE, SUSPEND_UNTIL_LOCKED);
//
//			*error = ERR_SUCCESS;
		}
		else if(queue == SUSPEND_QUEUE) {
//			printf("process found in suspend queue \n");
			*error = ERR_BAD_PARAM;
		}
		else if(queue == RUNNING_PCB) {
			printf("OS_ERROR: Current process is trying to suspend in a illegal way! \n");
			*error = ERR_BAD_PARAM;
		}
		else {
			*error = ERR_BAD_PARAM;
		}

		if(*error == ERR_SUCCESS) {
#ifdef ENABLE_SCHEDULER_PRINTER
			OSSchedulerPrinter(process_id, SP_ACTION_SUSPEND, TRUE);
#endif
		}
	}
	else if(process_id == -1) {

		if(OSCurrentRunningPCB != NULL) {
			long suspended_pid;

			suspended_pid = OSCurrentRunningPCB->OSPCBPid;
//			OSLockQueueOperation(SUSPEND_QUEUE, SUSPEND_UNTIL_LOCKED);
			OSCurrentRunningPCB->OSPCBSleepTime = 0;
			OSAddToSuspendQueue(OSCurrentRunningPCB);
//			OSUnlockQueueOperation(SUSPEND_QUEUE, SUSPEND_UNTIL_LOCKED);

			OSCurrentRunningPCB = NULL;

			*error = ERR_SUCCESS;

#ifdef ENABLE_SCHEDULER_PRINTER
			OSSchedulerPrinter(suspended_pid, SP_ACTION_SUSPEND, TRUE);
#endif
		}
		else {
			*error = ERR_BAD_PARAM;
		}
	}
	else {
		*error = ERR_BAD_PARAM;
	}
}

// Resume a process
/**
 * @input process_id: the pid of the process to be resumed
 * @input error: the operation result will be stored in the variable which this
 * 					error pointer points to
 */
void OSResumeProcess(long process_id, INT32 *error) {
	if(process_id >= 0) {
		OS_PCB *pcb;
		INT32 queue;
		BOOL result;

		pcb = OSSearchForProcess(process_id, &queue);

		// resume a process if this process is in suspend queue
		if(queue == SUSPEND_QUEUE) {

			OSLockQueueOperation(READY_QUEUE, SUSPEND_UNTIL_LOCKED);
//			OSLockQueueOperation(SUSPEND_QUEUE, SUSPEND_UNTIL_LOCKED);
			result = OSRemoveFromSuspendQueue(pcb);
			if(result) {
				OSAddToReadyQueue(pcb);
				*error = ERR_SUCCESS;

//				OSUnlockQueueOperation(READY_QUEUE, SUSPEND_UNTIL_LOCKED);
//				OSUnlockQueueOperation(SUSPEND_QUEUE, SUSPEND_UNTIL_LOCKED);
			}
			else {
				*error = ERR_BAD_PARAM;
			}
			OSUnlockQueueOperation(READY_QUEUE, SUSPEND_UNTIL_LOCKED);
		}
		else {
			*error = ERR_BAD_PARAM;
		}

		if(*error == ERR_SUCCESS) {
#ifdef ENABLE_SCHEDULER_PRINTER
			OSSchedulerPrinter(process_id, SP_ACTION_RESUME, TRUE);
#endif
		}
	}
	else {
		*error = ERR_BAD_PARAM;
	}
}

// Change the priority of a process
/**
 * @input process_id: the pid of the target process
 * @input new_priority: the new priority to be reassigned
 * @input *error: the result of the operation
 */
void OSChangeProcessPiority(long process_id, long new_priority, INT32 *error) {
	if(process_id > 0 && new_priority < MAX_PRIORITY_NUM) {
		OS_PCB *pcb;
		INT32 queue;
		BOOL result;

		pcb = OSSearchForProcess(process_id, &queue);

		if(queue == READY_QUEUE) {
			OSLockQueueOperation(READY_QUEUE, SUSPEND_UNTIL_LOCKED);
			result = OSRemoveFromReadyQueue(pcb);
			OSUnlockQueueOperation(READY_QUEUE, SUSPEND_UNTIL_LOCKED);

			if(result) {
				OSLockQueueOperation(READY_QUEUE, SUSPEND_UNTIL_LOCKED);
				pcb->OSPCBPriority = new_priority;
				OSAddToReadyQueue(pcb);
				OSUnlockQueueOperation(READY_QUEUE, SUSPEND_UNTIL_LOCKED);

				printf("priority changed successfully!\n");

				*error = ERR_SUCCESS;
			}
			else {
				*error = ERR_BAD_PARAM;
			}
		}
		else if(queue == TIMER_QUEUE) {
			OSLockQueueOperation(TIMER_QUEUE, SUSPEND_UNTIL_LOCKED);
			pcb->OSPCBPriority = new_priority;
			OSUnlockQueueOperation(TIMER_QUEUE, SUSPEND_UNTIL_LOCKED);

			*error = ERR_SUCCESS;
		}
		else if(queue == SUSPEND_QUEUE) {
//			OSLockQueueOperation(SUSPEND_QUEUE, SUSPEND_UNTIL_LOCKED);
			pcb->OSPCBPriority = new_priority;
//			OSUnlockQueueOperation(SUSPEND_QUEUE, SUSPEND_UNTIL_LOCKED);

			*error = ERR_SUCCESS;
		}
		else if(queue == RUNNING_PCB) {
			printf("OS_ERROR: Current process is trying to change priority in a illegal way! \n");
			*error = ERR_BAD_PARAM;
		}
		else {
			*error = ERR_BAD_PARAM;
		}

		if(*error == ERR_SUCCESS) {
#ifdef ENABLE_SCHEDULER_PRINTER
			OSSchedulerPrinter(process_id, SP_ACTION_CHANGEPRIO, TRUE);
#endif
		}
	}
	else if(process_id == -1) {

		if(OSCurrentRunningPCB != NULL) {
			OSCurrentRunningPCB->OSPCBPriority = new_priority;

			*error = ERR_SUCCESS;

#ifdef ENABLE_SCHEDULER_PRINTER
			OSSchedulerPrinter(0, SP_ACTION_CHANGEPRIO, TRUE);
#endif
		}
		else {
			*error = ERR_BAD_PARAM;
		}
	}
	else {
		*error = ERR_BAD_PARAM;
	}
}

// Delete a process
/**
 * input *pcb: the pointer to the pcb to be deleted
 */
void OSDeleteProcess(OS_PCB *pcb) {
	OSRemoveFromReadyQueue(pcb);

	// reset pcb values
	pcb->OSPCBSleepTime = 0;
	pcb->OSPCBPriority = MAX_PRIORITY_NUM-1;
	pcb->SPPrintDispatchCount = 0;
	pcb->OSPendingOperation = 0;
	free(pcb->OSPCBProcName);

	// return this pcb to the empty list
	PushToPCBQueueTail(OSProcEmptyList, pcb);

	OSProcessNumber--;
}

// Delete a process, free memory allocated for member variables
void OSDeleteCurrentProcess() {
	// reset pcb values
	OSCurrentRunningPCB->OSPCBSleepTime = 0;
	OSCurrentRunningPCB->OSPCBPriority = MAX_PRIORITY_NUM-1;
	OSCurrentRunningPCB->SPPrintDispatchCount = 0;
	OSCurrentRunningPCB->OSPendingOperation = 0;
	free(OSCurrentRunningPCB->OSPCBProcName);

	// return this pcb to the empty list
	PushToPCBQueueTail(OSProcEmptyList, OSCurrentRunningPCB);

	OSCurrentRunningPCB = NULL;

	OSProcessNumber--;
}

// Terminate a process
/**
 * @input process_id: the process_id of the process to be terminated
 * @input error: the result of the termination operation
 */
void OSTerminateProcess(long process_id, INT32 *error) {
	if(process_id > 0) {

		OS_PCB *pcb;
		INT32 queue;

		pcb = OSSearchForProcess(process_id, &queue);

		// delete a process if this process is in ready queue
		if(queue == READY_QUEUE) {
//			printf("process found in ready queue! \n");
			OSLockQueueOperation(READY_QUEUE, SUSPEND_UNTIL_LOCKED);
			OSDeleteProcess(pcb);
			OSUnlockQueueOperation(READY_QUEUE, SUSPEND_UNTIL_LOCKED);
			*error = ERR_SUCCESS;
		}
		else if(queue == TIMER_QUEUE) {
			// TODO find a way to deal with this situation properly
			printf("process found in timer queue \n");
			*error = ERR_BAD_PARAM;
//			OSLockQueueOperation(TIMER_QUEUE, SUSPEND_UNTIL_LOCKED);
//			pcb->OSPendingOperation = OP_TERMINATE_PROC;
//			OSUnlockQueueOperation(TIMER_QUEUE, SUSPEND_UNTIL_LOCKED);
//			*error = ERR_SUCCESS;
		}
		else if(queue == SUSPEND_QUEUE) {
//			printf("process found in suspend queue \n");
//			*error = ERR_BAD_PARAM;

			OSDeleteProcess(pcb);
			*error = ERR_SUCCESS;
		}
		else if(queue == RUNNING_PCB) {
			printf("OS_ERROR: Current process is trying to terminate in a illegal way! \n");
			*error = ERR_BAD_PARAM;
		}
		else {
			*error = ERR_BAD_PARAM;
		}

#ifdef ENABLE_SCHEDULER_PRINTER
		if(*error == ERR_SUCCESS && queue != TIMER_QUEUE)
		OSSchedulerPrinter(process_id, SP_ACTION_TERMINATE, TRUE);
#endif
	}
	else if(process_id == -1) {

		long terminated_pid;

		terminated_pid = OSCurrentRunningPCB->OSPCBPid;

		OSLockQueueOperation(READY_QUEUE, SUSPEND_UNTIL_LOCKED);

		OSDeleteCurrentProcess();

		OSUnlockQueueOperation(READY_QUEUE, SUSPEND_UNTIL_LOCKED);

#ifdef ENABLE_SCHEDULER_PRINTER
		OSSchedulerPrinter(terminated_pid, SP_ACTION_TERMINATE, TRUE);
#endif

		*error = ERR_SUCCESS;

//		printf("Current process has been terminated successfully! \n");

//		printf("Total number of processes is %d \n", OSProcessNumber);

		if(OSProcessNumber == 0) {
			printf("\n");
			printf("There is no process to run, system will be halt! \n");
			Z502Halt();
		}
	}
	else if(process_id == -2) {
		*error = ERR_SUCCESS;

		Z502Halt();
	}

}

// Add a process to ready queue
/**
 * @input pcb: the OS_PCB to be added to the ready queue
 * @output: the result of the operation
 */
void OSAddToReadyQueue(OS_PCB *pcb) {

	if(pcb != NULL) {
		InsertToPCBQueue(OSProcReadyQueue, pcb, QSORT_PRIORITY);
	}

//	if(pcb != NULL) {
//		// this pcb doesn't belong to any queue and it's
//		//	also not the current running process, so it can
//		//	be deleted/suspended directly
//		if(pcb->OSPendingOperation == OP_TERMINATE_PROC) {
//			OSDeleteProcess(pcb);
//		}
//		else if(pcb->OSPendingOperation == OP_SUSPEND_PROC) {
//			pcb->OSPCBSleepTime = 0;
//			OSAddToSuspendQueue(pcb);
//		}
//		else
//			InsertToPCBQueue(OSProcReadyQueue, pcb, QSORT_PRIORITY);
//	}
}

// Remove a process from ready queue
/**
 * @input pcb: the OS_PCB to be removed from the ready queue
 * @output: the result of the operation
 */
BOOL OSRemoveFromReadyQueue(OS_PCB *pcb) {

	BOOL result;

	result = RemoveFromPCBQueue(OSProcReadyQueue, pcb);

	if(result) {
//		printf("Process is removed from ready queue successfully!\n");
		return TRUE;
	}
	else {
		printf("OS ERROR: Process to be removed from ready queue is not found!\n");
		return FALSE;
	}
}

// Pop out the first element from the ready queue and remove this element
//	from ready queue
OS_PCB* OSPopHeadFromReadyQueue() {
	OS_PCB* pcb;

	if(OSProcReadyQueue->PCBQueueHead != NULL) {
		pcb = PopFromPCBQueueHead(OSProcReadyQueue);
	}
	else {
		pcb = NULL;
	}

	return pcb;
}

// Add a process to process suspend queue
/**
 * @input pcb: the OS_PCB to be added to the suspend queue
 * @output: the result of the operation
 */
void OSAddToSuspendQueue(OS_PCB *pcb) {
	InsertToPCBQueue(OSProcSuspendQueue, pcb, QSORT_PRIORITY);
}

// Remove a process from suspend queue
/**
 * @input pcb: the OS_PCB to be removed from the suspend queue
 * @output: the result of the operation
 */
BOOL OSRemoveFromSuspendQueue(OS_PCB *pcb) {

	BOOL result;

	result = RemoveFromPCBQueue(OSProcSuspendQueue, pcb);

	if(result) {
//		printf("Process is removed from suspend queue successfully!\n");
		return TRUE;
	}
	else {
		printf("OS ERROR: Process to be removed from suspend queue is not found!\n");
		return FALSE;
	}
}

// Get process id from name
/**
 * @input process_name: process name of the queried process
 * @input result: pass in a pointer and the query result will be stored
 * 					in the variable this pointer points to
 * @output: the process id
 */
INT32 OSGetProcessID(char *process_name, INT32 *result) {
	OS_PCB* pcb_ready;
	OS_PCB* pcb_timer;
	OS_PCB* pcb_suspend;
	INT32	process_id = -1;

	OSLockQueueOperation(TIMER_QUEUE, SUSPEND_UNTIL_LOCKED);
	OSLockQueueOperation(READY_QUEUE, SUSPEND_UNTIL_LOCKED);
//	OSLockQueueOperation(SUSPEND_QUEUE, SUSPEND_UNTIL_LOCKED);

	// check ready queue
	pcb_ready = SearchNameFromQueue(OSProcReadyQueue, process_name);

	// check timer queue
	pcb_timer = SearchNameFromQueue(OSProcTimerQueue, process_name);

	// check suspend queue
	pcb_suspend = SearchNameFromQueue(OSProcSuspendQueue, process_name);

	if(pcb_ready != NULL) {
		process_id = pcb_ready->OSPCBPid;
		*result = ERR_SUCCESS;
	}
	else if(pcb_timer != NULL) {
		process_id = pcb_timer->OSPCBPid;
		*result = ERR_SUCCESS;
	}
	else if(pcb_suspend != NULL) {
		process_id = pcb_suspend->OSPCBPid;
		*result = ERR_SUCCESS;
	}
	// check if the name passed in is the name of the current running process
	else if(OSCurrentRunningPCB != NULL){
		if(strcmp(OSCurrentRunningPCB->OSPCBProcName,process_name) == 0)
		{
			process_id = OSCurrentRunningPCB->OSPCBPid;
			*result = ERR_SUCCESS;
		}
		else {
			process_id = -1;
			*result = ERR_BAD_PARAM;
		}
	}
	else {
		process_id = -1;
		*result = ERR_BAD_PARAM;
	}

	OSUnlockQueueOperation(READY_QUEUE, SUSPEND_UNTIL_LOCKED);
	OSUnlockQueueOperation(TIMER_QUEUE, SUSPEND_UNTIL_LOCKED);
//	OSUnlockQueueOperation(SUSPEND_QUEUE, SUSPEND_UNTIL_LOCKED);

	return process_id;
}

// Search a process from all existing queues, this function assumes
//	a valid process_id is passed in, otherwise it may cause problems
/**
 * @input process_id: the pid of the process to be searched
 * @input queue_source: the name of the queue from which the process
 * 					is found will be stored in the variable this pointer
 * 					points to
 * @output : a pointer to the pcb of the goal process
 */
OS_PCB* OSSearchForProcess(long process_id, INT32 *queue_source) {
	OS_PCB *pcb;
	OS_PCB *pcb_ready;
	OS_PCB *pcb_timer;
	OS_PCB *pcb_suspend;

	OSLockQueueOperation(TIMER_QUEUE, SUSPEND_UNTIL_LOCKED);
	OSLockQueueOperation(READY_QUEUE, SUSPEND_UNTIL_LOCKED);
//	OSLockQueueOperation(SUSPEND_QUEUE, SUSPEND_UNTIL_LOCKED);

	// check ready queue
	pcb_ready = SearchPIDFromQueue(OSProcReadyQueue, process_id);

	// check timer queue
	pcb_timer = SearchPIDFromQueue(OSProcTimerQueue, process_id);

	// check timer queue
	pcb_suspend = SearchPIDFromQueue(OSProcSuspendQueue, process_id);

	// delete a process if this process is in ready queue
	if(pcb_ready != NULL) {
		pcb = pcb_ready;
		*queue_source = READY_QUEUE;
	}
	else if(pcb_timer != NULL) {
		pcb = pcb_timer;
		*queue_source = TIMER_QUEUE;
	}
	else if(pcb_suspend != NULL){
		pcb = pcb_suspend;
		*queue_source = SUSPEND_QUEUE;
	}
	// check if the name passed in is the name of the current running process
	else if(OSCurrentRunningPCB != NULL){
		if(OSCurrentRunningPCB->OSPCBPid == process_id)
		{
			pcb = pcb_suspend;
			*queue_source = RUNNING_PCB;
		}
		else {
			pcb = NULL;
			*queue_source = 0;
		}
	}
	else {
		pcb = NULL;
		*queue_source = 0;
	}

	OSUnlockQueueOperation(READY_QUEUE, SUSPEND_UNTIL_LOCKED);
	OSUnlockQueueOperation(TIMER_QUEUE, SUSPEND_UNTIL_LOCKED);
//	OSUnlockQueueOperation(SUSPEND_QUEUE, SUSPEND_UNTIL_LOCKED);

	return pcb;
}

// Print out info of all processes
void OSShowAllProcessInfo() {
	OS_PCB *pcb_index;
	INT32	i = 0, j = 0;

	OSLockQueueOperation(TIMER_QUEUE, SUSPEND_UNTIL_LOCKED);
	OSLockQueueOperation(READY_QUEUE, SUSPEND_UNTIL_LOCKED);
//	OSLockQueueOperation(SUSPEND_QUEUE, SUSPEND_UNTIL_LOCKED);

	if(OSCurrentRunningPCB != NULL) {
		printf("//-----------------------------------------// \n");

		printf("-------current running process: pid: %ld, name: %s, priority: %ld -----\n",OSCurrentRunningPCB->OSPCBPid,
				OSCurrentRunningPCB->OSPCBProcName,OSCurrentRunningPCB->OSPCBPriority);

	}
	else {
		printf("******* No running user process!  ********** \n");
	}

	if(OSProcReadyQueue->PCBQueueHead != NULL) {
		pcb_index = OSProcReadyQueue->PCBQueueHead;

		printf("******* ready queue length: %d  ********** \n",OSProcReadyQueue->QueueLength);

		while(pcb_index != NULL) {
			printf("-------queue element %d pid: %ld, name: %s, priority: %ld -----\n",i,pcb_index->OSPCBPid,pcb_index->OSPCBProcName,pcb_index->OSPCBPriority);
			pcb_index = pcb_index->OSPCBNext;
			i++;
		}
	}
	else {
		printf("******* ready queue length: 0  ********** \n");
	}

//	OSUnlockQueueOperation(READY_QUEUE, SUSPEND_UNTIL_LOCKED);
//	OSLockQueueOperation(TIMER_QUEUE, SUSPEND_UNTIL_LOCKED);

	if(OSProcTimerQueue->PCBQueueHead != NULL) {
		pcb_index = OSProcTimerQueue->PCBQueueHead;

		printf("******* timer queue length: %d  ********** \n",OSProcTimerQueue->QueueLength);

		while(pcb_index != NULL) {
			printf("-------queue element %d pid: %ld, name: %s, sleep time: %ld, priority: %ld -----\n",j,pcb_index->OSPCBPid,pcb_index->OSPCBProcName,pcb_index->OSPCBSleepTime,pcb_index->OSPCBPriority);
			pcb_index = pcb_index->OSPCBNext;
			j++;
		}
	}
	else {
		printf("******* timer queue length: 0  ********** \n");
	}

	if(OSProcSuspendQueue->PCBQueueHead != NULL) {
		pcb_index = OSProcSuspendQueue->PCBQueueHead;

		printf("******* suspend queue length: %d  ********** \n",OSProcSuspendQueue->QueueLength);

		while(pcb_index != NULL) {
			printf("-------queue element %d pid: %ld, name: %s, sleep time: %ld, priority: %ld -----\n",j,pcb_index->OSPCBPid,pcb_index->OSPCBProcName,pcb_index->OSPCBSleepTime,pcb_index->OSPCBPriority);
			pcb_index = pcb_index->OSPCBNext;
			j++;
		}
	}
	else {
		printf("******* suspend queue length: 0  ********** \n");
	}

	OSUnlockQueueOperation(READY_QUEUE, SUSPEND_UNTIL_LOCKED);
	OSUnlockQueueOperation(TIMER_QUEUE, SUSPEND_UNTIL_LOCKED);
//	OSUnlockQueueOperation(SUSPEND_QUEUE, SUSPEND_UNTIL_LOCKED);
}

// Print out info of remaining elements in empty list
void OSShowEmptyListInfo() {
	OS_PCB *pcb_index;
	INT32	i = 0;

	if(OSProcEmptyList->PCBQueueHead != NULL) {
		pcb_index = OSProcEmptyList->PCBQueueHead;

		printf("******* empty list length: %d  ********** \n",OSProcEmptyList->QueueLength);

		while(pcb_index != NULL) {
			printf("-------queue element %d id: %ld -----\n",i,pcb_index->OSPCBPid);
			pcb_index = pcb_index->OSPCBNext;
			i++;
		}
	}
	else {
		printf("******* empty list length: 0  ********** \n");
	}
}


