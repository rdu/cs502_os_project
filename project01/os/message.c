/*
 * message.c
 *
 *  Created on: Oct 7, 2014
 *      Author: rdu
 */

#include	<string.h>
#include	<stdio.h>
#include	<stdlib.h>

#include	"global.h"
#include	"myos.h"

extern OS_PCB			*OSCurrentRunningPCB;
extern OS_MSG_QUEUE 	*OSIPCMsgQueue;

// Private function prototypes
// Create a IPC message
OS_MSG* OSCreateIPCMessage(long target_pid, char *message_buffer, INT32 message_send_length);
// Add a message to message queue
BOOL OSAddMsgToQueue(OS_MSG *msg);
// Remove a message from message queue
void OSRemoveMsgFromQueue(OS_MSG *msg);
// Search for a message from message queue
OS_MSG* OSSearchMsgFromQueue(long source_pid, long target_pid);

// Send a message to a process with the pid of "target_pid"
/**
 * @input target_pid: pid of the receiver process
 * @input *message_buffer: place to store message
 * @input message_send_lenght: length of the message sent
 * @input *error: operation result
 */
void OSSendMessage(long target_pid, char *message_buffer,
		long message_send_length, INT32 *error) {
	if(target_pid < MAX_PROCESS_NUMBER && target_pid > -2) {
		OS_MSG *msg;
		BOOL result;
		INT32 resume_result;

		// create a message
		msg = OSCreateIPCMessage(target_pid, message_buffer,message_send_length);

		// add the message to message queue
		if(msg != NULL) {
			result = OSAddMsgToQueue(msg);

			if(result == TRUE) {
				*error = ERR_SUCCESS;

#ifndef MINIMAL_USER_PRINT
				printf("OS INFO: Message sent successfully!\n");
#endif
#ifdef ENABLE_RDU_MESSAGE_PRINTER
				OSShowMsgQueueInfo();
#endif
				// tell OS a message is ready for the target and if this target is suspended,
				//	resume it
				OSResumeProcess(target_pid, &resume_result);
#ifndef MINIMAL_USER_PRINT
				if(resume_result == ERR_SUCCESS)
					printf("OS INFO: Target process has been resumed to receive the message!\n");
#endif
			}
			else {
				*error = ERR_BAD_PARAM;
#ifndef MINIMAL_USER_PRINT
				printf("OS ERROR: Message queue is full and no new message can be added!\n");
#endif
			}
		}
		else {
			*error = ERR_BAD_PARAM;
#ifndef MINIMAL_USER_PRINT
			printf("OS ERROR: Message is longer than the maximum allowed length!\n");
#endif
		}
	}
	else {
		*error = ERR_BAD_PARAM;
#ifndef MINIMAL_USER_PRINT
		printf("OS ERROR: Trying to send message to a process with illegal process id!\n");
#endif
	}
}

// Receive a message from a process with the pid of "source_pid"
/**
 * @input source_pid: pid of the sender process
 * @input *message_buffer: place to store message
 * @input message_receive_length: length of the message received
 * @input *message_sender_pid: pid of the sender process
 * @Input *error: operation result
 */
void OSReceiveMessage(long source_pid, char *message_buffer,
		long message_receive_length, INT32 *message_send_length,
		INT32 *message_sender_pid,INT32 *error) {

	if(source_pid < MAX_PROCESS_NUMBER && source_pid > -2) {

		if(message_receive_length < MAX_MSG_LENGTH) {
			// check if there a message available for the receiver process
			OS_MSG *msg = NULL;

			if(OSCurrentRunningPCB != NULL)
				msg =OSSearchMsgFromQueue(source_pid, OSCurrentRunningPCB->OSPCBPid);

			if(msg != NULL) {
				if(msg->OSMSGLength < message_receive_length) {
					strcpy(message_buffer, msg->OSMSGContent);
					*message_send_length = msg->OSMSGLength;
					*message_sender_pid = msg->OSMSGSourcePid;

					OSRemoveMsgFromQueue(msg);

					*error = ERR_SUCCESS;
				}
				else {
					*error = MSG_RX_ERR_LES_BUFFER;

#ifndef MINIMAL_USER_PRINT
					printf("OS ERROR: Do not have enough buffer to store incoming message!\n");
#endif
				}
			}
			else {
				INT32 suspend_result;
				*error = MSG_RX_ERR_NOT_FOUND;
#ifndef MINIMAL_USER_PRINT
				printf("OS INFO: Cannot find expected message from message queue!\n");
#endif
				// if this process cannot find the desired message, suspend it self
				OSSuspendProcess(-1, &suspend_result);
			}
		}
		else {
			*error = MSG_RX_ERR_EXC_LENGTH;
#ifndef MINIMAL_USER_PRINT
			printf("OS ERROR: Trying to receive a message whose length is bigger than the maximum allowed length!\n");
#endif
		}
	}
	else {
		*error = MSG_RX_ERR_ILL_SOURCE;
#ifndef MINIMAL_USER_PRINT
		printf("OS ERROR: Trying to receive a message from a process with illegal process id!\n");
#endif
	}
}

// Create a IPC message
OS_MSG* OSCreateIPCMessage(long target_pid, char *message_buffer, INT32 message_send_length) {

	if(message_send_length < MAX_MSG_LENGTH) {
		OS_MSG *msg;

		//TODO Find a good place to free memory
		msg = (OS_MSG *)malloc(sizeof(OS_MSG));

		if(OSCurrentRunningPCB != NULL)
			msg->OSMSGSourcePid = OSCurrentRunningPCB->OSPCBPid;
		msg->OSMSGTargetPid = target_pid;
		msg->OSMSGLength = message_send_length;
		strcpy(msg->OSMSGContent, message_buffer);

		return msg;
	}
	else {
		return NULL;
	}
}

// Add a message to message queue
BOOL OSAddMsgToQueue(OS_MSG *msg) {
	if(OSIPCMsgQueue->QueueLength < MSG_QUEUE_LENGTH) {
		if(msg != NULL) {
			if(OSIPCMsgQueue->MSGQueueHead == NULL) {
				OSIPCMsgQueue->MSGQueueHead = msg;
				OSIPCMsgQueue->MSGQueueTail = msg;

				msg->OSMSGNext = NULL;
				msg->OSMSGPrev = NULL;
			}
			else {
				msg->OSMSGPrev = OSIPCMsgQueue->MSGQueueTail;
				msg->OSMSGNext = NULL;
				OSIPCMsgQueue->MSGQueueTail->OSMSGNext = msg;

				OSIPCMsgQueue->MSGQueueTail = msg;
			}

			OSIPCMsgQueue->QueueLength++;

			return TRUE;
		}
		else
			return FALSE;
	}
	else {
		return FALSE;
	}
}
// Remove a message from message queue
void OSRemoveMsgFromQueue(OS_MSG *msg) {
	if(msg != NULL) {
		if(OSIPCMsgQueue->MSGQueueHead == msg) {
			OSIPCMsgQueue->MSGQueueHead = msg->OSMSGNext;
			if(OSIPCMsgQueue->MSGQueueHead != NULL)
				OSIPCMsgQueue->MSGQueueHead->OSMSGPrev = NULL;
			else
				OSIPCMsgQueue->MSGQueueTail = NULL;
		}
		else if(OSIPCMsgQueue->MSGQueueTail == msg) {
			OSIPCMsgQueue->MSGQueueTail = msg->OSMSGPrev;
			OSIPCMsgQueue->MSGQueueTail->OSMSGNext = NULL;
		}
		else {
			msg->OSMSGPrev->OSMSGNext = msg->OSMSGNext;
			msg->OSMSGNext->OSMSGPrev = msg->OSMSGPrev;
		}

		msg->OSMSGNext = NULL;
		msg->OSMSGPrev = NULL;

		OSIPCMsgQueue->QueueLength--;
	}
}

// Search for a message from message queue with a specific source pid and target pid
OS_MSG* OSSearchMsgFromQueue(long source_pid, long target_pid) {
	OS_MSG *msg_index;

	msg_index = OSIPCMsgQueue->MSGQueueTail;

	while(msg_index != NULL) {
		// see if sender pid of the message is the desired one
		if(msg_index->OSMSGTargetPid == target_pid || msg_index->OSMSGTargetPid == -1) {
			// check if this message is broadcasted by the current process itself
			if(source_pid == -1 && msg_index->OSMSGSourcePid != target_pid)
				break;
			else {
				if(msg_index->OSMSGSourcePid == source_pid)
					break;
				else
					msg_index = msg_index->OSMSGPrev;
			}
		}
		else
			msg_index = msg_index->OSMSGPrev;
	}

	return msg_index;
}

// Print information of message queue
void OSShowMsgQueueInfo() {
	OS_MSG *msg_index;
	int i = 0;

	if(OSIPCMsgQueue->MSGQueueHead != NULL) {
		msg_index = OSIPCMsgQueue->MSGQueueHead;

		printf("******* message queue length: %d  ********** \n",OSIPCMsgQueue->QueueLength);

		while(msg_index != NULL) {
			printf("-------queue element %d source id: %ld, target id: %ld, message length: %ld -----\n",
					i,msg_index->OSMSGSourcePid,msg_index->OSMSGTargetPid, msg_index->OSMSGLength);
			msg_index = msg_index->OSMSGNext;
			i++;
		}
	}
	else {
		printf("******* message queue length: 0  ********** \n");
	}
}
