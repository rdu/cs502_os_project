/*
 * mytest.h
 *
 *  Created on: Sep 23, 2014
 *      Author: rdu
 */

#ifndef MYTEST_H_
#define MYTEST_H_

//------------------------------------------------------------------------//
// The following tests are modified from the code in test.c. They are used
//	to test a specific aspect (mostly an intermediate step) of a task before
//	the full test for that task in test.c can be passed.
//------------------------------------------------------------------------//

// This test is designed to test CREATE_PROCESS() and GET_PROCESS_ID()
//	system calls
void test_process(void);
// This test is designed to test if TerminateProcess() can work properly
void test_terminate(void);
// This test is designed to test if timer queue works properly, it's a
//	simplified version of test1c without recursively sleep in test1x
void test_timerqueue(void);
// This test is designed to test the dispatcher
void test_dispatcher(void);
// This test is designed to test if suspend/resume works
void test_suspend(void);
// This test is designed to test if priority can be changed properly
void test_changepriority(void);
// This test is designed to test if the OS can behave properly when it tries
//	to suspend/terminate a process in timer queue
void test_timerqueuespecial(void);


//------------------------------------------------------------------------//
// The following test are just copied here so that extra information can be
//	printed out or just part of the code can be tested
//------------------------------------------------------------------------//
void test_mytest1i(void);
void test_mytest1j(void);


//------------------------------------------------------------------------//
// The original test1l() doesn't work. This is a modified copy. It keeps most
//	of the original code and tries to make it possible to pass.
//------------------------------------------------------------------------//
void test_mytest1l(void);

//------------------------------------------------------------------------//
// Functions showing additional features are declared here
//------------------------------------------------------------------------//
// Add code for the original test1m in test.c here
void test_mytest1m(void);

#endif /* MYTEST_H_ */
