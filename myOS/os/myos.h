/*
 * myos.h
 *
 *  Created on: Sep 11, 2014
 *      Author: rdu
 */

#ifndef MYOS_H_
#define MYOS_H_

#include 	"global.h"

//#define RDU_DEBUG

/*------------------------  OS Configurations ------------------------*/

#define		MAX_PROCESS_NUMBER		20
#define		MAX_PRIORITY_NUM		35
#define		MSG_QUEUE_LENGTH		32
#define		MAX_MSG_LENGTH			128
#define		DEFAULT_PGE_TBL_LEN		1024

/*--------------------------  Macro Switches -------------------------*/

#define		MEM_PAG_REPL_FIFO

// if defined, only minimal info will be printed out from user code
#define 	MINIMAL_USER_PRINT

// if defined, OS ERROR/INFO will be printed out
#define		SHOW_OS_ERROR_INFO

// if defined, only minimal info from hardware drivers will be printed out
#define		MINIMAL_DRIVER_PRINT

// if define, only minimal info will be printed out from interrupt/fault handler
#define		MINIMAL_INT_FAU_PRINT

// if defined, scheduler printer will be enabled
#define 	ENABLE_SCHEDULER_PRINTER

// if defined, memory printer will be enabled
#define 	ENABLE_MEMORY_PRINTER

// if defined, user-defined scheduler printer will be enabled
//#define		ENABLE_RDU_SCHEDULER_PRINTER

// if defined, user-define message queue printer will be enabled
//#define		ENABLE_RDU_MESSAGE_PRINTER

// if defined, debug info for page replacement will be shown
//#define		PAGING_DEBUG_INFO


/*------------------------ Process Management ------------------------*/

// Definitions
//#define	 PROCESS_NAME_LENGTH	20				// max number of chars for process name
#define		SCHED_PRINT_FULL		1
#define		SCHED_PRINT_LIMIT		0
#define		SCHED_PRINT_DISABLE		-1
#define		SCHED_PRINT_LIM_NUM		20

#define		OS_STATE_RUN			(short)0		// process is running
#define  	OS_STATE_READY			(short)1  		// process is ready to run
#define  	OS_STATE_SUSPEND		(short)2  		// process is suspended
#define	 	OS_STATE_WAIT			(short)3		// process is waiting for a event to happen (timer/IO)

#define 	OP_SUSPEND_PROC			(short)10  		// suspend a process
#define 	OP_TERMINATE_PROC		(short)11  		// terminate a process

// Data structure for shadow table entry
typedef struct os_shadow_page_table_entry {
	BOOL ShadowPageValid;
	long ShadowPageDiskID;
	long ShadowPageSectorID;
} OS_SHADOW_PAGE_TABLE_ENTRY;

// Data structure of process control block
typedef struct os_pcb {
	long 	OSPCBPid;							// process ID
	char	*OSPCBProcName;						// process name

	struct	os_pcb *OSPCBPrev;					// pointer to the previous PCB in the list
	struct	os_pcb *OSPCBNext;					// pointer to the next PCB in the list

	void 	*OSPCBCtx;							// context of the process

	long	OSPCBSleepTime;						// sleep time
	long	OSPCBPriority;						// priority of the process
	UINT16	OSPCBState;							// process state: run, suspend

	UINT32	OSPendingOperation;					// stores pending operation such as suspend, terminate

	UINT16  *OSPCBPageTblAddr;					// stores a reference of page table address

	OS_SHADOW_PAGE_TABLE_ENTRY OSPCBShadowPageTblAddr[DEFAULT_PGE_TBL_LEN];
												// stores where a page is swapped to on the disk

	UINT32	SPPrintDispatchCount;				// time limit to print dispatch in SP
} OS_PCB;

// Data structure for PCB queues
typedef struct os_pcb_queue {
	OS_PCB *PCBQueueHead;
	OS_PCB *PCBQueueTail;

	INT32	QueueLength;
} OS_PCB_QUEUE;

// Functions implemented in "os/process.c"
// Create a process
OS_PCB* OSCreateProcess(char *proc_name, void *entry_address, long priority, INT32 *pid, INT32 *result);
// Terminate a process
void OSTerminateProcess(long process_id, INT32 *error);
// Add a process to ready queue
void OSAddToReadyQueue(OS_PCB *pcb);
// Remove a process from ready queue
BOOL OSRemoveFromReadyQueue(OS_PCB *pcb);
// Pop out the first element from the ready queue
OS_PCB* OSPopHeadFromReadyQueue();
// Suspend a process
void OSSuspendProcess(long process_id, INT32 *error);
// Resume a process
void OSResumeProcess(long process_id, INT32 *error);
// Change the priority of a process
void OSChangeProcessPiority(long process_id, long new_priority, INT32 *error);
// Search a process from all existing queues (search domain includes current running PCB)
OS_PCB* OSSearchForProcess(long process_id, INT32 *queue_source);
// Get process id from name
INT32 OSGetProcessID(char *process_name, INT32 *result);
// Print out info of all processes
void OSShowAllProcessInfo();
// Print out info of remaining elements in empty list
void OSShowEmptyListInfo();


/*------------------------- Timer Management -------------------------*/

#define 	TIMER_DRV_REG_SET_LOCK 	MEMORY_INTERLOCK_BASE+1

// Functions implemented in "os/ostimer.c"
// Add a process to timer queue
void OSAddToTimerQueue(long time);
// Remove a process from timer queue
BOOL OSRemoveFromTimerQueue(OS_PCB *pcb);
// Pop the head process from timer queue
OS_PCB* OSPopHeadFromTimerQueue();
// Check if timer queue is empty, if not load the sleep time
//	from the first PCB in the queue
void OSLoadTimeFromTimerQueue();
// Ensure the heart of the simulator is still beating, do nothing
//		but force the system time to move forward, a wrapper of CALL()
// Absolutely unnecessary for a real hardware
void OSKeepSimulatorAlive();
// Check if there is a process in timer queue whose sleep time has already
//	passed but an interrupt was not triggered as expected.
void OSCleanOverdueProcess();


/*---------------------------- Scheduling ----------------------------*/

// System locks
/*  These are the allowable locations for hardware synchronization support */
/*  #define      MEMORY_INTERLOCK_BASE     0x7FE00000					   */
/*  #define      MEMORY_INTERLOCK_SIZE     0x00000100					   */

#define 	TIMER_QUEUE_LOCK		TIMER_DRV_REG_SET_LOCK+1
#define 	READY_QUEUE_LOCK 		TIMER_QUEUE_LOCK+1
#define		SUSPEND_QUEUE_LOCK		READY_QUEUE_LOCK+1

#define		TIMER_QUEUE				1
#define		READY_QUEUE				2
#define		SUSPEND_QUEUE			3
#define		RUNNING_PCB				6

#define     SUSPEND_UNTIL_LOCKED    TRUE
#define     DO_NOT_SUSPEND          FALSE

#define		SP_ACTION_CREATE		"Create"
#define		SP_ACTION_DISPATCH		"Dispatch"
#define		SP_ACTION_SUSPEND		"Suspend"
#define		SP_ACTION_RESUME		"Resume"
#define		SP_ACTION_CHANGEPRIO	"ChngPrio"
#define		SP_ACTION_TERMINATE		"Done"
#define		SP_ACTION_TIMERISR		"TimerISR"
#define		SP_ACTION_PRIVLINST		"InsFault"

#define		SP_MAX_DISPATCH_PRINT	6

// Functions implemented in "os/scheduling.c"
// Prepare everything for the next ready process and give control
//	to this process to start running
void OSProcessDispatch();
// This function is called when there is no process on ready queue to run
void OSProcessWasteTime();
// Add lock to timer/ready queue
BOOL OSLockQueueOperation(INT32 queue_type, BOOL lock_type);
// Remove lock to timer/ready queue
BOOL OSUnlockQueueOperation(INT32 queue_type, BOOL lock_type);
// Use scheduler printer to print information of processes
void OSSchedulerPrinter(long pid, char *action, BOOL print_info);


/*----------------------- Message Send/Receive -----------------------*/

#define		MSG_RX_ERR_ILL_SOURCE	21L
#define		MSG_RX_ERR_EXC_LENGTH	22L
#define		MSG_RX_ERR_NOT_FOUND	23L
#define		MSG_RX_ERR_LES_BUFFER	24L

// Data structure of message
typedef struct os_msg {
	long 	OSMSGSourcePid;					// process ID of message sender
	long 	OSMSGTargetPid;				// process ID of message receiver

	struct os_msg	*OSMSGPrev;
	struct os_msg	*OSMSGNext;

	long	OSMSGLength;					// actual message length
	char	OSMSGContent[MAX_MSG_LENGTH];	// message buffer with the size of MAX_MSG_LENGTH
} OS_MSG;

// Data structure for message queues
typedef struct os_msg_queue {
	OS_MSG *MSGQueueHead;
	OS_MSG *MSGQueueTail;

	INT32	QueueLength;
} OS_MSG_QUEUE;

// Functions implemented in "os/message.c"
// Send a message to a process with the pid of "target_pid"
void OSSendMessage(long target_pid, char *message_buffer, long message_send_length, INT32 *error);
// Receive a message from a process with the pid of "source_pid"
void OSReceiveMessage(long source_pid,char *message_buffer,long message_receive_length,
		INT32 *message_send_length,INT32 *message_sender_pid,INT32 *error);
// Print information of message queue
void OSShowMsgQueueInfo();


/*------------------------- Memory Management ------------------------*/

#define		MEM_VICTIM_LOCK	SUSPEND_QUEUE_LOCK+1
#define		MEM_FRAME_LOCK	MEM_VICTIM_LOCK+1

#define		PAGE_TBL_VALID_MASK 	0x8000
#define		PAGE_TBL_INVALID_MASK	0x7fff
#define		PAGE_TBL_MODIF_MASK 	0x4000
#define		PAGE_TBL_UNMODIF_MASK	0xbfff
#define		PAGE_TBL_REFER_MASK 	0x2000
#define		PAGE_TBL_RESRV_MASK 	0x1000
#define		PAGE_TBL_FRAME_MASK		0x0fff
#define		PAGE_TBL_FRM_CLR_MASK	0xf000

#define 	MEM_PRINT_FULL			1
#define		MEM_PRINT_LIMIT			0
#define		MEM_PRINT_DISABLE		-1

#define		MEM_PRINT_LIMIT_NUM		20

// Data structure of frame table entries
typedef struct os_mem_frame {
	long OSMemFrameID;

	BOOL OSMemFrameInUse;
	long OSMemPageID;
//	long OSMemFramePid;
	OS_PCB *OSMemFramePCB;

//	struct os_mem_frame *OSMemFrmPrev;
//	struct os_mem_frame *OSMemFrmNext;
} OS_MEM_FRAME;

//// Data structure for frame table
//typedef struct os_mem_frame_table {
//	OS_MEM_FRAME *MemFrameTableHead;
//	OS_MEM_FRAME *MemFrameTableTail;
//
//	long	MemFrameEmptyNumber;
//} OS_MEM_FRAME_TABLE;

//// Data structure for shadow table entry
//typedef struct os_shadow_page_table_entry {
//	BOOL ShadowPageInUse;
//	long ShadowPageDiskID;
//	long ShadowPageSectorID;
//} OS_SHADOW_PAGE_TABLE_ENTRY;

// Functions implemented in "os/memory.c"
// Handler function of memory fault
void OSMemoryFaultHandler(INT32 status);
// When a process is terminated, memory frames associated with this process should
void OSRecycleMemFrame(long process_id);
// Print out status of physical memory of Z502
void OSShowMemFrameStatus();
// Print out memory contents
void OSShowPhysicalMemory(int page);
// Print out page table of current process
void OSShowPageTable();
//  Use memory printer to print information of memory
void OSMemoryPrinter();

/*-------------------------- Disk Management -------------------------*/

#define		DISK_TASK_LIST1_LOCK	MEM_FRAME_LOCK+1
#define		DISK_TASK_LIST2_LOCK	DISK_TASK_LIST1_LOCK+1
#define		DISK_TASK_LIST3_LOCK	DISK_TASK_LIST2_LOCK+1
#define		DISK_TASK_LIST4_LOCK	DISK_TASK_LIST3_LOCK+1
#define		DISK_TASK_LIST5_LOCK	DISK_TASK_LIST4_LOCK+1
#define		DISK_TASK_LIST6_LOCK	DISK_TASK_LIST5_LOCK+1
#define		DISK_TASK_LIST7_LOCK	DISK_TASK_LIST6_LOCK+1
#define		DISK_TASK_LIST8_LOCK	DISK_TASK_LIST7_LOCK+1

#define 	DISK_DRV_REG_SET_LOCK 	DISK_TASK_LIST8_LOCK+1
// *** the last lock is define for timer: TIMER_DRV_REG_SET_LOCK

// Data structure of message
typedef struct os_disk_task {
	// Process ID of the requester
	long OSDiskTaskRequestPID;
	BOOL OSDiskTaskResponded;

	// Task details
	long 	OSTaskDiskId;
	long 	OSTaskDiskSector;
	char	*OSDiskData;
	BOOL	OSDiskTaskType;

	struct os_disk_task	*OSDiskTaskPrev;
	struct os_disk_task	*OSDiskTaskNext;
} OS_DISK_TASK;

// Data structure for disk task lists
typedef struct os_disk_task_list {
	OS_DISK_TASK *DiskTaskListHead;
	OS_DISK_TASK *DiskTaskListTail;

	INT32	TaskListLength;
} OS_DISK_TASK_LIST;

// Functions implemented in "os/disk.c"
// Load next disk task from list
void OSLoadNextDiskTask(long disk_id);
// Resume all processes whose disk operation request has been finished
void OSResumeDiskFinishedProcess(long disk_id);
// Clean all finished task from task list
void OSCleanFinishedDiskTask(long disk_id);
// Write data to a disk
void OSDiskWriteRequest(long disk_id, long sector, char *data);
// Create a disk write request without suspend current process
void OSDiskWriteRequestNoSuspend(long disk_id, long sector, char *data);
// Read data from a disk
void OSDiskReadRequest(long disk_id, long sector, char *data);
// Find a disk/sector that is available for writing data
BOOL OSDiskGetFreeSector(long *disk_id, long *sector);
// Print information of message queue
void OSShowDiskTaskListInfo();
// Lock the task list
BOOL OSLockDiskTaskList(long list_index);
// Unlock the task list
BOOL OSUnlockDiskTaskList(long list_index);


#endif /* MYOS_H_ */
