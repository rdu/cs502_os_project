/************************************************************************

        This code forms the base of the operating system you will
        build.  It has only the barest rudiments of what you will
        eventually construct; yet it contains the interfaces that
        allow test.c and z502.c to be successfully built together.

        Revision History:
        1.0 August 1990
        1.1 December 1990: Portability attempted.
        1.3 July     1992: More Portability enhancements.
                           Add call to sample_code.
        1.4 December 1992: Limit (temporarily) printout in
                           interrupt handler.  More portability.
        2.0 January  2000: A number of small changes.
        2.1 May      2001: Bug fixes and clear STAT_VECTOR
        2.2 July     2002: Make code appropriate for undergrads.
                           Default program start is in test0.
        3.0 August   2004: Modified to support memory mapped IO
        3.1 August   2004: hardware interrupt runs on separate thread
        3.11 August  2004: Support for OS level locking
	4.0  July    2013: Major portions rewritten to support multiple threads
************************************************************************/
#include             "string.h"
#include			 <stdint.h>
#include 			 <stdlib.h>
#include			 <assert.h>

#include             "global.h"
#include             "syscalls.h"
#include             "protos.h"
#include 			 "hardware_driver.h"
#include 			 "myos.h"
#include			 "interrupt.h"
#include			 "mytest.h"


// These loacations are global and define information about the page table
extern UINT16        *Z502_PAGE_TBL_ADDR;
extern INT16         Z502_PAGE_TBL_LENGTH;

extern long Z502_REG1;

extern void          *TO_VECTOR [];

char                 *call_names[] = { "mem_read ", "mem_write",
                            "read_mod ", "get_time ", "sleep    ",
                            "get_pid  ", "create   ", "term_proc",
                            "suspend  ", "resume   ", "ch_prior ",
                            "send     ", "receive  ", "disk_read",
                            "disk_wrt ", "def_sh_ar" };

OS_PCB 			OSProcessEmptyList[MAX_PROCESS_NUMBER];
OS_PCB			*OSCurrentRunningPCB;
OS_PCB_QUEUE 	*OSProcEmptyList;
OS_PCB_QUEUE	*OSProcReadyQueue;
OS_PCB_QUEUE	*OSProcTimerQueue;
OS_PCB_QUEUE	*OSProcSuspendQueue;
INT32 			OSProcessNumber;

OS_MSG_QUEUE 	*OSIPCMsgQueue;

OS_MEM_FRAME	OSMemFrameTable[PHYS_MEM_PGS];
//OS_MEM_FRAME_TABLE *OSMemFrameTable;

OS_DISK_TASK_LIST *OSDiskTaskList[MAX_NUMBER_OF_DISKS];

int MemPrintFlag = MEM_PRINT_DISABLE;
int	MemPrintLimitNum = 20;

int SchedulePrintFlag = SCHED_PRINT_DISABLE;

// Private function prototypes
// Initialize pcb empty list
void OSInitPCB(void);
// Initialize message queue
void OSInitMessageQueue(void);
// Initialize memory frame table
void OSInitMemFrmTable(void);
// Initialize disk task lists
void OSInitDiskTaskList(void);


/************************************************************************
    INTERRUPT_HANDLER
        When the Z502 gets a hardware interrupt, it transfers control to
        this routine in the OS.
************************************************************************/
void    interrupt_handler( void ) {
    INT32              device_id;
    INT32              status;
    INT32              Index = 0;

    // Get cause of interrupt
    MEM_READ(Z502InterruptDevice, &device_id );

    while(device_id != -1) {
    	// Set this device as target of our query
    	MEM_WRITE(Z502InterruptDevice, &device_id );
    	// Now read the status of this device
    	MEM_READ(Z502InterruptStatus, &status );

    	if( device_id == TIMER_INTERRUPT ){
    		switch(status){
    		case ERR_SUCCESS:
    			printf( "Interrupt of device %d has occurred with no error.\n",
    					device_id);
//    			printf("system time (checked from interrupt handler): %d \n",DrvGetCurrentSysTime());
//    			OSPopHeadFromTimerQueue();
//    			OSLoadTimeFromTimerQueue();
    			OSTimerSuccessISR();
//    			OSShowDiskTaskListInfo();
    			break;
    		case ERR_BAD_PARAM:
    			printf( "Interrupt of device %d has occurred with an ERR_BAD_PARAM = %d.\n",
    					device_id, status );
    			OSTimerFailureISR();
    			break;
    		default:
    			printf( "ERROR! interrupt status of timer not recognized!\n");
    			printf( "Z502InterruptStatus is: %i\n", status);
    		}
    	}
    	else if( device_id >= DISK_INTERRUPT  && device_id <= LARGEST_STAT_VECTOR_INDEX){
    		INT32 disk_id;

    		disk_id = device_id - 4;

    		switch(status){
    		case ERR_SUCCESS:
#ifndef MINIMAL_INT_FAU_PRINT
    			printf( "Interrupt of device %d has occurred with no error.\n",
    					device_id);
#endif
    			OSDiskSuccessISR(disk_id);
    			break;
    		case ERR_BAD_PARAM:
    			printf( "Interrupt of device %d has occurred with an ERR_BAD_PARAM = %d.\n",
    					disk_id, status );
    			OSDiskFailureISR(disk_id);
    			break;
    		case ERR_NO_PREVIOUS_WRITE:
    			printf(" Interrupt: Disk %d is trying to access a unwritten sector!\n", disk_id);
    			OSDiskFailureISR(disk_id);
    			break;
    		case ERR_DISK_IN_USE:
    			printf( "Interrupt:Disk %d is busy! \n", disk_id );
    			OSDiskFailureISR(disk_id);
    			break;
    		default:
    			printf( "ERROR! interrupt status of disk not recognized!\n");
    			printf( "Z502InterruptStatus is: %i\n", status);
    		}
    	}

    	// Clear out this device - we're done with it
    	MEM_WRITE(Z502InterruptClear, &Index );

    	MEM_READ(Z502InterruptDevice, &device_id );
    }
}                                       /* End of interrupt_handler */
/************************************************************************
    FAULT_HANDLER
        The beginning of the OS502.  Used to receive hardware faults.
************************************************************************/

void    fault_handler( void )
    {
    INT32       device_id;
    INT32       status;
    INT32       Index = 0;
    static UINT32 fault_print_count = 0;

    // Get cause of interrupt
    MEM_READ(Z502InterruptDevice, &device_id );
    // Set this device as target of our query
    MEM_WRITE(Z502InterruptDevice, &device_id );
    // Now read the status of this device
    MEM_READ(Z502InterruptStatus, &status );

//    if(MemPrintFlag == MEM_PRINT_LIMIT && (fault_print_count % MemPrintLimitNum)==0)
//    	printf( "Fault_handler: Found vector type %d with value %d\n",
//    			device_id, status );
//    else if(MemPrintFlag != MEM_PRINT_LIMIT)
//    	printf( "Fault_handler: Found vector type %d with value %d\n",
//    			device_id, status );

    fault_print_count++;

    switch(device_id) {
    	case CPU_ERROR:
    		printf("OS_ERROR: CPU error!\n");
    		break;
    	case INVALID_MEMORY:
//    		printf("OS_ERROR: Invalid memory!\n");
    		OSMemoryFaultHandler(status);
    		break;
    	case INVALID_PHYSICAL_MEMORY:
    		printf("OS_ERROR: Invalid physical memory!\n");
    		break;
    	case PRIVILEGED_INSTRUCTION:
    		printf("\n");
    		printf("OS_ERROR: Privileged instructions are used in USER_MODE!\n");
    		printf("  System is about to halt due to this fatal error!\n");
#ifdef ENABLE_SCHEDULER_PRINTER
	if(OSCurrentRunningPCB != NULL)
		OSSchedulerPrinter(OSCurrentRunningPCB->OSPCBPid, SP_ACTION_PRIVLINST, TRUE);
#endif
    		Z502Halt();
    		break;
    	default:
    		printf( "ERROR! fault vector type not recognized!\n");
    		printf( "Z502InterruptStatus is - %i\n", status);

    }

    // Clear out this device - we're done with it
    MEM_WRITE(Z502InterruptClear, &Index );
}                                       /* End of fault_handler */

/************************************************************************
    SVC
        The beginning of the OS502.  Used to receive software interrupts.
        All system calls come to this point in the code and are to be
        handled by the student written code here.
        The variable do_print is designed to print out the data for the
        incoming calls, but does so only for the first ten calls.  This
        allows the user to see what's happening, but doesn't overwhelm
        with the amount of data.
************************************************************************/

void    svc( SYSTEM_CALL_DATA *SystemCallData ) {
    short               call_type;
    static short        do_print = 10;
    short               i;
//    INT32 				Time;

    call_type = (short)SystemCallData->SystemCallNumber;

    if ( do_print > 0 ) {
        printf( "SVC handler: %s\n", call_names[call_type]);
        for (i = 0; i < SystemCallData->NumberOfArguments - 1; i++ ){
        	 //Value = (long)*SystemCallData->Argument[i];
             printf( "Arg %d: Contents = (Decimal) %8ld,  (Hex) %8lX\n", i,
             (unsigned long )SystemCallData->Argument[i],
             (unsigned long )SystemCallData->Argument[i]);
        }
        do_print--;
    }

    switch(call_type){
//    	case SYSNUM_MEM_READ:
//    		break;
//    	case SYSNUM_MEM_WRITE:
//    		break;
//    	case SYSNUM_READ_MODIFY:
//    		break;
    	// Get time service call
    	case SYSNUM_GET_TIME_OF_DAY:
    		CALL ( DrvGetTimeOfDay((INT32 *)SystemCallData->Argument[0]) );
//    		DrvGetTimeOfDay((INT32 *)SystemCallData->Argument[0]);
			break;
		// Get set sleep time call
    	case SYSNUM_SLEEP:
//    		CALL( OSAddToTimerQueue((long)SystemCallData->Argument[0]));
    		OSAddToTimerQueue((long)SystemCallData->Argument[0]);
    		CALL( OSProcessDispatch() );
    		break;
    	case SYSNUM_GET_PROCESS_ID:
    		*SystemCallData->Argument[1] = OSGetProcessID((char *)SystemCallData->Argument[0],(INT32 *)SystemCallData->Argument[2]);
    		break;
    	case SYSNUM_CREATE_PROCESS:
    		CALL( OSCreateProcess((char *)SystemCallData->Argument[0],(void *)SystemCallData->Argument[1],(long)SystemCallData->Argument[2],
    				(INT32 *)SystemCallData->Argument[3],(INT32 *)SystemCallData->Argument[4]) );
    		CALL( OSProcessDispatch() );
    		break;
    	case SYSNUM_TERMINATE_PROCESS:
//    		Z502Halt();
    		CALL( OSTerminateProcess((long)SystemCallData->Argument[0], (INT32 *)SystemCallData->Argument[1]) );
    		CALL( OSProcessDispatch() );
    		break;
    	case SYSNUM_SUSPEND_PROCESS:
    		CALL( OSSuspendProcess((long)SystemCallData->Argument[0], (INT32 *)SystemCallData->Argument[1]));
    		CALL( OSProcessDispatch() );
    		break;
    	case SYSNUM_RESUME_PROCESS:
    		CALL( OSResumeProcess((long)SystemCallData->Argument[0], (INT32 *)SystemCallData->Argument[1]));
    		CALL( OSProcessDispatch() );
    		break;
    	case SYSNUM_CHANGE_PRIORITY:
    		CALL( OSChangeProcessPiority((long)SystemCallData->Argument[0], (long)SystemCallData->Argument[1], (INT32 *)SystemCallData->Argument[2]) );
    		CALL( OSProcessDispatch() );
    		break;
    	case SYSNUM_SEND_MESSAGE:
    		CALL( OSSendMessage((long)SystemCallData->Argument[0], (char *)SystemCallData->Argument[1],
    				(long)SystemCallData->Argument[2], (INT32 *)SystemCallData->Argument[3]) );
    		CALL( OSProcessDispatch() );
    		break;
    	case SYSNUM_RECEIVE_MESSAGE:
    		CALL( OSReceiveMessage((long)SystemCallData->Argument[0], (char *)SystemCallData->Argument[1],
    				(long)SystemCallData->Argument[2], (INT32 *)SystemCallData->Argument[3],
    				(INT32 *)SystemCallData->Argument[4],(INT32 *)SystemCallData->Argument[5]) );
    		if(*SystemCallData->Argument[5] == MSG_RX_ERR_NOT_FOUND) {
    			CALL( OSProcessDispatch() );
    			CALL( OSReceiveMessage((long)SystemCallData->Argument[0], (char *)SystemCallData->Argument[1],
    					(long)SystemCallData->Argument[2], (INT32 *)SystemCallData->Argument[3],
    					(INT32 *)SystemCallData->Argument[4],(INT32 *)SystemCallData->Argument[5]) );
    		}
    		break;
    	case SYSNUM_DISK_READ:
//    		CALL( DrvDiskRead((long)SystemCallData->Argument[0], (long)SystemCallData->Argument[1], (char *)SystemCallData->Argument[2]) );
    		CALL( OSDiskReadRequest((long)SystemCallData->Argument[0], (long)SystemCallData->Argument[1], (char *)SystemCallData->Argument[2]) );
    		CALL( OSProcessDispatch() );
    		break;
    	case SYSNUM_DISK_WRITE:
//    		CALL( DrvDiskWrite((long)SystemCallData->Argument[0], (long)SystemCallData->Argument[1], (char *)SystemCallData->Argument[2]) );
    		CALL( OSDiskWriteRequest((long)SystemCallData->Argument[0], (long)SystemCallData->Argument[1], (char *)SystemCallData->Argument[2]) );
    		CALL( OSProcessDispatch() );
    		break;
    	case SYSNUM_DEFINE_SHARED_AREA:
    		break;
    	default:
    		printf( "ERROR! call_type not recognized!\n");
    		printf( "Call_type is - %i\n", call_type);
    } // End of switch
}  // End of svc


/************************************************************************
    osInit
        This is the first routine called after the simulation begins.  This
        is equivalent to boot code.  All the initial OS components can be
        defined and initialized here.
************************************************************************/

void    osInit( int argc, char *argv[]  ) {
    void                *next_context;
    INT32               i;
    INT32 				*root_pid,*root_creation_result;

    /* Demonstrates how calling arguments are passed thru to here       */

    printf( "Program called with %d arguments:", argc );
    for ( i = 0; i < argc; i++ )
        printf( " %s", argv[i] );
    printf( "\n" );
    printf( "Calling with argument 'sample' executes the sample program.\n" );

    /*          Setup so handlers will come to code in base.c           */

    TO_VECTOR[TO_VECTOR_INT_HANDLER_ADDR]   = (void *)interrupt_handler;
    TO_VECTOR[TO_VECTOR_FAULT_HANDLER_ADDR] = (void *)fault_handler;
    TO_VECTOR[TO_VECTOR_TRAP_HANDLER_ADDR]  = (void *)svc;

    /*			Initialize PCB and all others							*/
    OSInitPCB();
    OSInitMessageQueue();
    OSInitMemFrmTable();
    OSInitDiskTaskList();


    /*		    Prepare to create the first process						*/
    root_pid = (INT32 *)malloc(sizeof(INT32));
    root_creation_result = (INT32 *)malloc(sizeof(INT32));
    assert(root_pid != NULL);
    assert(root_creation_result != NULL);

    /*  Determine if the switch was set, and if so go to demo routine.  */

    if (( argc > 1 ) && ( strcmp( argv[1], "sample" ) == 0 ) ) {
        Z502MakeContext( &next_context, (void *)sample_code, KERNEL_MODE );
        Z502SwitchContext( SWITCH_CONTEXT_KILL_MODE, &next_context );
    }
    else if (( argc > 1 ) && ( strcmp( argv[1], "test0" ) == 0 ) ) {
    	OSCreateProcess("test0", (void *)test0, 1, root_pid, root_creation_result);
    }
    else if(( argc > 1 ) && ( strcmp( argv[1], "test1a" ) == 0 ) ) {
    	OSCreateProcess("test1a", (void *)test1a, 1, root_pid, root_creation_result);
    }
    else if(( argc > 1 ) && ( strcmp( argv[1], "test1b" ) == 0 ) ) {
    	OSCreateProcess("test1b", (void *)test1b, 1, root_pid, root_creation_result);
    }
    else if(( argc > 1 ) && ( strcmp( argv[1], "test1c" ) == 0 ) ) {
    	OSCreateProcess("test1c", (void *)test1c, 1, root_pid, root_creation_result);
    }
    else if(( argc > 1 ) && ( strcmp( argv[1], "test1d" ) == 0 ) ) {
    	OSCreateProcess("test1d", (void *)test1d, 1, root_pid, root_creation_result);
    }
    else if(( argc > 1 ) && ( strcmp( argv[1], "test1e" ) == 0 ) ) {
    	OSCreateProcess("test1e", (void *)test1e, 1, root_pid, root_creation_result);
    }
    else if(( argc > 1 ) && ( strcmp( argv[1], "test1f" ) == 0 ) ) {
    	OSCreateProcess("test1f", (void *)test1f, 1, root_pid, root_creation_result);
    }
    else if(( argc > 1 ) && ( strcmp( argv[1], "test1g" ) == 0 ) ) {
    	OSCreateProcess("test1g", (void *)test1g, 1, root_pid, root_creation_result);
    }
    else if(( argc > 1 ) && ( strcmp( argv[1], "test1h" ) == 0 ) ) {
    	OSCreateProcess("test1h", (void *)test1h, 1, root_pid, root_creation_result);
    }
    else if(( argc > 1 ) && ( strcmp( argv[1], "test1i" ) == 0 ) ) {
    	OSCreateProcess("test1i", (void *)test1i, 1, root_pid, root_creation_result);
    }
    else if(( argc > 1 ) && ( strcmp( argv[1], "test1j" ) == 0 ) ) {
    	OSCreateProcess("test1j", (void *)test1j, 1, root_pid, root_creation_result);
    }
    else if(( argc > 1 ) && ( strcmp( argv[1], "test1k" ) == 0 ) ) {
    	OSCreateProcess("test1k", (void *)test1k, 1, root_pid, root_creation_result);
    }
    else if(( argc > 1 ) && ( strcmp( argv[1], "test1l" ) == 0 ) ) {
    	OSCreateProcess("test1l", (void *)test1l, 1, root_pid, root_creation_result);
    }
    else if(( argc > 1 ) && ( strcmp( argv[1], "test2a" ) == 0 ) ) {
    	OSCreateProcess("test2a", (void *)test2a, 1, root_pid, root_creation_result);
    	MemPrintFlag = MEM_PRINT_FULL;
    }
    else if(( argc > 1 ) && ( strcmp( argv[1], "test2b" ) == 0 ) ) {
    	OSCreateProcess("test2b", (void *)test2b, 1, root_pid, root_creation_result);
    	MemPrintFlag = MEM_PRINT_FULL;
    }
    else if(( argc > 1 ) && ( strcmp( argv[1], "test2c" ) == 0 ) ) {
    	OSCreateProcess("test2c", (void *)test2c, 1, root_pid, root_creation_result);
    	SchedulePrintFlag = SCHED_PRINT_FULL;
    }
    else if(( argc > 1 ) && ( strcmp( argv[1], "test2d" ) == 0 ) ) {
    	OSCreateProcess("test2d", (void *)test2d, 1, root_pid, root_creation_result);
    	SchedulePrintFlag = SCHED_PRINT_LIMIT;
    }
    else if(( argc > 1 ) && ( strcmp( argv[1], "test2e" ) == 0 ) ) {
    	OSCreateProcess("test2e", (void *)test2e, 1, root_pid, root_creation_result);
    	MemPrintFlag = MEM_PRINT_LIMIT;
    	MemPrintLimitNum = 20;
    	SchedulePrintFlag = SCHED_PRINT_LIMIT;
    }
    else if(( argc > 1 ) && ( strcmp( argv[1], "test2f" ) == 0 ) ) {
    	OSCreateProcess("test2f", (void *)test2f, 1, root_pid, root_creation_result);
    	MemPrintFlag = MEM_PRINT_LIMIT;
    	MemPrintLimitNum = 100;
    }
    else if(( argc > 1 ) && ( strcmp( argv[1], "test2g" ) == 0 ) ) {
    	OSCreateProcess("test2g", (void *)test2g, 1, root_pid, root_creation_result);
    }
    else if(( argc > 1 ) && ( strcmp( argv[1], "test2g" ) == 0 ) ) {
    	OSCreateProcess("test_disk", (void *)test_disk_usage, 1, root_pid, root_creation_result);
    }
    else {
//    	Z502MakeContext( &next_context, (void *)test_mytest1m, USER_MODE );
//    	Z502SwitchContext( SWITCH_CONTEXT_KILL_MODE, &next_context );
//    	OSCreateProcess("", (void *)test_mytest1m, MAX_PRIORITY_NUM-1, root_pid, root_creation_result);

    	OSCreateProcess("debug", (void *)test_disk_usage, 1, root_pid, root_creation_result);
    }

    /*  This should be done by a "os_make_process" routine, so that
        test0 runs on a process recognized by the operating system.    */
//    Z502MakeContext( &next_context, (void *)test0, USER_MODE );
//    Z502SwitchContext( SWITCH_CONTEXT_KILL_MODE, &next_context );

//    OSCreateProcess("", (void *)test_mytest1m, 1, root_pid, root_creation_result);

    OSProcessDispatch();
}                                               // End of osInit

// Initialize the PCB linked list
void OSInitPCB(void) {
	INT32	i;

	OSProcEmptyList = (OS_PCB_QUEUE *)malloc(sizeof(OS_PCB_QUEUE));
	assert(OSProcEmptyList != NULL);
	OSProcReadyQueue = (OS_PCB_QUEUE *)malloc(sizeof(OS_PCB_QUEUE));
	assert(OSProcReadyQueue != NULL);
	OSProcTimerQueue = (OS_PCB_QUEUE *)malloc(sizeof(OS_PCB_QUEUE));
	assert(OSProcTimerQueue != NULL);
	OSProcSuspendQueue = (OS_PCB_QUEUE *)malloc(sizeof(OS_PCB_QUEUE));
	assert(OSProcSuspendQueue != NULL);
//	OSCurrentRunningPCB = (OS_PCB *)malloc(sizeof(OS_PCB));
//	assert(OSCurrentRunningPCB != NULL);

	OSProcEmptyList->PCBQueueHead = &OSProcessEmptyList[0];
	OSProcEmptyList->PCBQueueTail = &OSProcessEmptyList[MAX_PROCESS_NUMBER-1];
	OSProcEmptyList->QueueLength = MAX_PROCESS_NUMBER;

	OSProcReadyQueue->PCBQueueHead = NULL;
	OSProcReadyQueue->PCBQueueTail = NULL;
	OSProcReadyQueue->QueueLength = 0;

	OSProcTimerQueue->PCBQueueHead = NULL;
	OSProcTimerQueue->PCBQueueTail = NULL;
	OSProcTimerQueue->QueueLength = 0;

	OSProcSuspendQueue->PCBQueueHead = NULL;
	OSProcSuspendQueue->PCBQueueTail = NULL;
	OSProcSuspendQueue->QueueLength = 0;

	OSCurrentRunningPCB = NULL;

	OSProcessNumber = 0;

	for(i=0;i<MAX_PROCESS_NUMBER;i++) {

		OSProcessEmptyList[i].OSPCBPid = i;
		OSProcessEmptyList[i].OSPCBPriority = MAX_PRIORITY_NUM-1;
		OSProcessEmptyList[i].OSPCBSleepTime = 0;
		OSProcessEmptyList[i].SPPrintDispatchCount = 0;
		OSProcessEmptyList[i].OSPendingOperation = 0;

		if(i == 0)
			OSProcessEmptyList[i].OSPCBPrev = NULL;
		else
			OSProcessEmptyList[i].OSPCBPrev = &OSProcessEmptyList[i-1];

		if(i == MAX_PROCESS_NUMBER-1)
			OSProcessEmptyList[i].OSPCBNext = NULL;
		else
			OSProcessEmptyList[i].OSPCBNext = &OSProcessEmptyList[i+1];
	}

#ifdef RDU_DEBUG
	for(i=0;i<MAX_PROCESS_NUMBER;i++) {
		printf("pcb id:%d \n",OSProcessEmptyList[i].OSPCBPid);
	}

	printf("PCB list initialized\n");
#endif
}

// Initialize other variables
void OSInitMessageQueue(void) {
	// Initialize the pointer to the message queue
	OSIPCMsgQueue = (OS_MSG_QUEUE *)malloc(sizeof(OS_MSG_QUEUE));
	assert(OSIPCMsgQueue != NULL);
}

// Initialize memory frame table
void OSInitMemFrmTable(void) {
	INT32 i = 0;

//	OSMemFrameTable = (OS_MEM_FRAME_TABLE *)malloc(sizeof(OS_PCB_QUEUE));
//	assert(OSMemFrameTable != NULL);

	for(i=0; i< PHYS_MEM_PGS; i++) {
		OSMemFrameTable[i].OSMemFrameID = i;
		OSMemFrameTable[i].OSMemPageID = -1;
		OSMemFrameTable[i].OSMemFrameInUse = FALSE;
		// set default pid to be -3 as -2, -1, 0, ... MAX_PROCESS_NUMBER-1 are reserved
//		OSMemFrameEntries[i].OSMemFramePid = -3;
		OSMemFrameTable[i].OSMemFramePCB = NULL;

//		if(i == 0)
//			OSMemFrameEntries[i].OSMemFrmPrev = NULL;
//		else
//			OSMemFrameEntries[i].OSMemFrmPrev = &OSMemFrameEntries[i-1];
//
//		if(i == PHYS_MEM_PGS-1)
//			OSMemFrameEntries[i].OSMemFrmNext = NULL;
//		else
//			OSMemFrameEntries[i].OSMemFrmNext = &OSMemFrameEntries[i+1];
	}

//	OSMemFrameTable->MemFrameEmptyNumber = PHYS_MEM_PGS;
//	OSMemFrameTable->MemFrameTableHead = &OSMemFrameEntries[0];
//	OSMemFrameTable->MemFrameTableTail = &OSMemFrameEntries[PHYS_MEM_PGS-1];
}

// Initialize disk task lists
void OSInitDiskTaskList(void) {
	int i = 0;

	for(i=0; i<MAX_NUMBER_OF_DISKS; i++) {
		OSDiskTaskList[i] = (OS_DISK_TASK_LIST *)malloc(sizeof(OS_DISK_TASK_LIST));
		assert(OSDiskTaskList != NULL);

		OSDiskTaskList[i]->DiskTaskListHead = NULL;
		OSDiskTaskList[i]->DiskTaskListTail = NULL;
		OSDiskTaskList[i]->TaskListLength = 0;
	}

	DrvDiskFormat();
}
