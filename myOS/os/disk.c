/*
 * disk.c
 *
 *  Created on: Nov 19, 2014
 *      Author: rdu
 */

#include	<string.h>
#include	<stdio.h>
#include	<stdlib.h>

#include	"global.h"
#include	"protos.h"
#include	"syscalls.h"
#include	"myos.h"
#include	"hardware_driver.h"

extern OS_PCB			 *OSCurrentRunningPCB;
extern OS_DISK_TASK_LIST *OSDiskTaskList[MAX_NUMBER_OF_DISKS];

// Private function prototypes
// Create a disk task
OS_DISK_TASK* OSCreateDiskTask(long disk_id, long sector, char *data, BOOL type);
// Add a task to task list
void OSAddDiskTaskToList(long disk_id, OS_DISK_TASK *task);
// Remove a task from task list
void OSRemoveDiskTaskFromList(long disk_id, OS_DISK_TASK *task);
// Pop the head task from task list
OS_DISK_TASK* OSPopHeadFromDiskTaskList(long disk_id);
// Get the next unresponded disk task
OS_DISK_TASK* OSGetNextUnrespondedDiskTask(long disk_id);

// Load next disk task from list
void OSLoadNextDiskTask(long disk_id) {
	OS_DISK_TASK *task;

	task = OSGetNextUnrespondedDiskTask(disk_id);

	if(task != NULL) {
		task->OSDiskTaskResponded = TRUE;

		if(task->OSDiskTaskType == DISK_ACT_WRITE) {
			DrvDiskWrite(task->OSTaskDiskId, task->OSTaskDiskSector, task->OSDiskData);
		}
		else if(task->OSDiskTaskType == DISK_ACT_READ) {
			DrvDiskRead(task->OSTaskDiskId, task->OSTaskDiskSector, task->OSDiskData);
		}

#ifndef 	MINIMAL_USER_PRINT
		printf("ROS_INFO: Next task in the task list have been loaded!\n");
#endif
	}
#ifndef 	MINIMAL_USER_PRINT
	else {
		printf("ROS_INFO: All tasks in the task list have been finished!\n");
	}
#endif
}

// Resume all processes whose disk operation request has been finished
void OSResumeDiskFinishedProcess(long disk_id) {
	INT32 disk_index;
	OS_DISK_TASK *task_index;
	INT32 count = 0;

	disk_index = disk_id - 1;
	task_index = OSDiskTaskList[disk_index]->DiskTaskListHead;

//	printf("Trying to resume processes from disk task list %d\n",disk_index);

	while(task_index != NULL) {
		if(task_index->OSDiskTaskResponded == TRUE) {
//			printf("Found one process to be resumed!\n");
			INT32 resume_result;
			OSResumeProcess(task_index->OSDiskTaskRequestPID, &resume_result);
			count++;
		}
		else
			break;

		task_index = task_index->OSDiskTaskNext;
	}

//	printf("OS_INFO: %d process(es) resumed successfully!\n",count);
}

// Create a disk write request
/**
 * @input disk_id: id of the disk that you want to write data to
 * @input sector: sector number of the disk
 * @input data: data read you want to write to disk
 */
void OSDiskWriteRequest(long disk_id, long sector, char *data) {
	OS_DISK_TASK *task;

	task = OSCreateDiskTask(disk_id, sector, data, DISK_ACT_WRITE);

	if(task != NULL) {
		OSLockDiskTaskList(disk_id);
		OSAddDiskTaskToList(disk_id, task);
		OSUnlockDiskTaskList(disk_id);

		if(DrvCheckIfDiskIdle(disk_id)) {
			OSLockDiskTaskList(disk_id);
			OSLoadNextDiskTask(disk_id);
			OSUnlockDiskTaskList(disk_id);
		}

		// suspend current process to wait for disk operation finishing
		INT32 suspend_result;
		OSSuspendProcess(-1, &suspend_result);

#ifndef MINIMAL_USER_PRINT
		printf("OS INFO: Disk write task added successfully!\n");
#endif
	}
}

// Create a disk write request without suspend current process
/**
 * @input disk_id: id of the disk that you want to write data to
 * @input sector: sector number of the disk
 * @input data: data read you want to write to disk
 */
void OSDiskWriteRequestNoSuspend(long disk_id, long sector, char *data) {
	OS_DISK_TASK *task;

	task = OSCreateDiskTask(disk_id, sector, data, DISK_ACT_WRITE);

	if(task != NULL) {
		OSLockDiskTaskList(disk_id);
		OSAddDiskTaskToList(disk_id, task);
		OSUnlockDiskTaskList(disk_id);

		if(DrvCheckIfDiskIdle(disk_id)) {
			OSLockDiskTaskList(disk_id);
			OSLoadNextDiskTask(disk_id);
			OSUnlockDiskTaskList(disk_id);
		}

//		// suspend current process to wait for disk operation finishing
//		INT32 suspend_result;
//		OSSuspendProcess(-1, &suspend_result);

#ifndef MINIMAL_USER_PRINT
		printf("OS INFO: Disk write task added successfully!\n");
#endif
	}
}

// Create a disk read request
/**
 * @input disk_id: id of the disk that you want to read data from
 * @input sector: sector number of the disk
 * @input data: buffer used to contain data read from disk
 */
void OSDiskReadRequest(long disk_id, long sector, char *data) {
	OS_DISK_TASK *task;

	task = OSCreateDiskTask(disk_id, sector, data, DISK_ACT_READ);

	if(task != NULL) {
		OSLockDiskTaskList(disk_id);
		OSAddDiskTaskToList(disk_id, task);
		OSUnlockDiskTaskList(disk_id);

		if(DrvCheckIfDiskIdle(disk_id)) {
			OSLockDiskTaskList(disk_id);
			OSLoadNextDiskTask(disk_id);
			OSUnlockDiskTaskList(disk_id);
		}

		// suspend current process to wait for disk operation finishing
		INT32 suspend_result;
		OSSuspendProcess(-1, &suspend_result);

#ifndef MINIMAL_USER_PRINT
		printf("OS INFO: Disk read task added successfully!\n");
#endif
	}
}

// Create a disk task
/**
 * @input disk_id: id of the target disk
 * @input sector: sector id to be read from or written to
 * @input data: data to be sent or received
 * @input type: operation type, read or write
 * @output: a pointer to the newly created disk task
 */
OS_DISK_TASK* OSCreateDiskTask(long disk_id, long sector, char *data, BOOL type) {
	OS_DISK_TASK *task;

	if(disk_id >0 && disk_id <= MAX_NUMBER_OF_DISKS
			&& sector >= 0 && sector < NUM_LOGICAL_SECTORS) {
		//TODO Find a good place to free memory
		task = (OS_DISK_TASK *)malloc(sizeof(OS_DISK_TASK));

		task->OSDiskTaskRequestPID = OSCurrentRunningPCB->OSPCBPid;
		task->OSDiskTaskResponded = FALSE;

		task->OSTaskDiskId = disk_id;
		task->OSTaskDiskSector = sector;
//		strcpy(task->OSDiskData, data);
		task->OSDiskData = data;
		task->OSDiskTaskType = type;
	}
	else
		task = NULL;

	return task;
}

// Add a task to task list
/**
 * @input disk_id: id of the disk whose task list will accept the task
 * @input task: a pointer to the task to be added
 */
void OSAddDiskTaskToList(long disk_id, OS_DISK_TASK *task) {
	long disk_index;

	disk_index = disk_id - 1;

	if(task != NULL && disk_index < MAX_NUMBER_OF_DISKS) {
		if(OSDiskTaskList[disk_index]->DiskTaskListHead == NULL) {
			OSDiskTaskList[disk_index]->DiskTaskListHead = task;
			OSDiskTaskList[disk_index]->DiskTaskListTail = task;

			task->OSDiskTaskPrev = NULL;
			task->OSDiskTaskNext = NULL;
		}
		else {
			task->OSDiskTaskPrev = OSDiskTaskList[disk_index]->DiskTaskListTail;
			task->OSDiskTaskNext = NULL;
			OSDiskTaskList[disk_index]->DiskTaskListTail->OSDiskTaskNext = task;

			OSDiskTaskList[disk_index]->DiskTaskListTail = task;
		}
		OSDiskTaskList[disk_index]->TaskListLength++;

#ifndef MINIMAL_USER_PRINT
		printf("OS_INFO: Disk task added successfully!\n");
#endif
	}
}

// Remove a task from task list
/**
 * @input disk_id: id of the disk whose task list will remove the task
 * @input task: a pointer to the task to be removed
 */
void OSRemoveDiskTaskFromList(long disk_id, OS_DISK_TASK *task) {
	long disk_index;

	disk_index = disk_id - 1;

	if(task != NULL  && disk_index < MAX_NUMBER_OF_DISKS) {
		if(OSDiskTaskList[disk_index]->DiskTaskListHead == task) {
			OSDiskTaskList[disk_index]->DiskTaskListHead = task->OSDiskTaskNext;
			if(OSDiskTaskList[disk_index]->DiskTaskListHead != NULL)
				OSDiskTaskList[disk_index]->DiskTaskListHead->OSDiskTaskPrev = NULL;
			else
				OSDiskTaskList[disk_index]->DiskTaskListTail = NULL;
		}
		else if(OSDiskTaskList[disk_index]->DiskTaskListTail == task) {
			OSDiskTaskList[disk_index]->DiskTaskListTail = task->OSDiskTaskPrev;
			OSDiskTaskList[disk_index]->DiskTaskListTail->OSDiskTaskNext = NULL;
		}
		else {
			task->OSDiskTaskPrev->OSDiskTaskNext = task->OSDiskTaskNext;
			task->OSDiskTaskNext->OSDiskTaskPrev = task->OSDiskTaskPrev;
		}

		task->OSDiskTaskNext = NULL;
		task->OSDiskTaskPrev = NULL;

		OSDiskTaskList[disk_index]->TaskListLength--;
	}
}

// Get the next unresponded disk task
/**
 * @input disk_id: disk id where the next unresponded task will be searched from
 * @output: a pointer to the next available unresponded task
 */
OS_DISK_TASK* OSGetNextUnrespondedDiskTask(long disk_id) {

	OS_DISK_TASK *task_index;
	INT32 disk_index;

	disk_index = disk_id - 1;

	task_index = OSDiskTaskList[disk_index]->DiskTaskListHead;

	while(task_index != NULL) {
		if(task_index->OSDiskTaskResponded == TRUE)
			task_index = task_index->OSDiskTaskNext;
		else
			return task_index;
	}

	return NULL;
}

// Clean all finished task from task list
/**
 * @input disk_id: id of the disk whose task list needs to be cleaned
 */
void OSCleanFinishedDiskTask(long disk_id) {
	OS_DISK_TASK *task_index;
	INT32 disk_index;

	disk_index = disk_id - 1;

	task_index = OSDiskTaskList[disk_index]->DiskTaskListHead;

	while(task_index != NULL) {
		if(task_index->OSDiskTaskResponded == TRUE)
			OSPopHeadFromDiskTaskList(disk_id);
		else
			break;

		task_index = task_index->OSDiskTaskNext;
	}
}

//// Find a disk/sector that is available for writing data
//BOOL OSDiskGetFreeSector(long *disk_id, long *sector) {
//	static long free_sector = 0;
//
//	*disk_id = 8;
//	*sector = free_sector;
//
//	free_sector++;
//
////	if(free_sector == 1600) {
////		if((*disk_id) > 5) {
////			(*disk_id)--;
////			free_sector=0;
////		}
////		else
////			return FALSE;
////		printf("disk is full\n");
////	}
////
////	if((*disk_id) >= 5 && (*disk_id) <= MAX_NUMBER_OF_DISKS && (*sector) >= 0 && (*sector) < NUM_LOGICAL_SECTORS)
////		return TRUE;
////	else
////		return FALSE;
//
//	if(free_sector == 1600) {
//		(*disk_id)--;
//		free_sector=0;
//		printf("disk is full\n");
//	}
//
//	if((*disk_id) >= 1 && (*disk_id) <= MAX_NUMBER_OF_DISKS && (*sector) >= 0 && (*sector) < NUM_LOGICAL_SECTORS)
//		return TRUE;
//	else
//		return FALSE;
//}


// Pop the head task from task list
/**
 * @input disk_id: disk id where the next task will be searched from
 * @output: a pointer to the next task
 */
OS_DISK_TASK* OSPopHeadFromDiskTaskList(long disk_id) {
	INT32 disk_index;
	OS_DISK_TASK *task;

	disk_index = disk_id - 1;

	if(OSDiskTaskList[disk_index]->DiskTaskListHead != NULL) {
		task = OSDiskTaskList[disk_index]->DiskTaskListHead;

		if(task->OSDiskTaskNext != NULL) {
			OSDiskTaskList[disk_index]->DiskTaskListHead = task->OSDiskTaskNext;

			if(OSDiskTaskList[disk_index]->DiskTaskListHead != NULL)
				OSDiskTaskList[disk_index]->DiskTaskListHead->OSDiskTaskPrev = NULL;
			else
				OSDiskTaskList[disk_index]->DiskTaskListTail = NULL;
		}
		else {
			OSDiskTaskList[disk_index]->DiskTaskListHead = NULL;
			OSDiskTaskList[disk_index]->DiskTaskListTail = NULL;
		}

		OSDiskTaskList[disk_index]->TaskListLength--;
		return task;
	}
	else
		return NULL;
}

// Lock the task list
/**
 * @input list_index: the index of the list to be locked
 * @input: lock result
 */
BOOL OSLockDiskTaskList(long disk_id) {
	BOOL LockResult;
	INT32 list_index;

	list_index = disk_id - 1;

	LockResult = FALSE;

	if(list_index < MAX_NUMBER_OF_DISKS) {
		switch(list_index) {
			case 0:
				READ_MODIFY(DISK_TASK_LIST1_LOCK, 1, SUSPEND_UNTIL_LOCKED, &LockResult);
				break;
			case 1:
				READ_MODIFY(DISK_TASK_LIST2_LOCK, 1, SUSPEND_UNTIL_LOCKED, &LockResult);
				break;
			case 2:
				READ_MODIFY(DISK_TASK_LIST3_LOCK, 1, SUSPEND_UNTIL_LOCKED, &LockResult);
				break;
			case 3:
				READ_MODIFY(DISK_TASK_LIST4_LOCK, 1, SUSPEND_UNTIL_LOCKED, &LockResult);
				break;
			case 4:
				READ_MODIFY(DISK_TASK_LIST5_LOCK, 1, SUSPEND_UNTIL_LOCKED, &LockResult);
				break;
			case 5:
				READ_MODIFY(DISK_TASK_LIST6_LOCK, 1, SUSPEND_UNTIL_LOCKED, &LockResult);
				break;
			case 6:
				READ_MODIFY(DISK_TASK_LIST7_LOCK, 1, SUSPEND_UNTIL_LOCKED, &LockResult);
				break;
			case 7:
				READ_MODIFY(DISK_TASK_LIST8_LOCK, 1, SUSPEND_UNTIL_LOCKED, &LockResult);
				break;
			default:
				printf("OS_ERROR: Lock is not defined for this disk yet!\n");
		}

#ifndef MINIMAL_USER_PRINT
		if(LockResult)
			printf("Disk %ld task list locked!\n",disk_id);
		else
			printf("Disk %ld task list failed to lock!\n",disk_id);
#endif
	}
	return LockResult;
}

// Lock the task list
/**
 * @input list_index: the index of the list to be unlocked
 * @input: unlock result
 */
BOOL OSUnlockDiskTaskList(long disk_id) {
	BOOL LockResult;
	INT32 list_index;

	list_index = disk_id - 1;

	LockResult = FALSE;

	if(list_index < MAX_NUMBER_OF_DISKS) {
		switch(list_index) {
			case 0:
				READ_MODIFY(DISK_TASK_LIST1_LOCK, 0, SUSPEND_UNTIL_LOCKED, &LockResult);
				break;
			case 1:
				READ_MODIFY(DISK_TASK_LIST2_LOCK, 0, SUSPEND_UNTIL_LOCKED, &LockResult);
				break;
			case 2:
				READ_MODIFY(DISK_TASK_LIST3_LOCK, 0, SUSPEND_UNTIL_LOCKED, &LockResult);
				break;
			case 3:
				READ_MODIFY(DISK_TASK_LIST4_LOCK, 0, SUSPEND_UNTIL_LOCKED, &LockResult);
				break;
			case 4:
				READ_MODIFY(DISK_TASK_LIST5_LOCK, 0, SUSPEND_UNTIL_LOCKED, &LockResult);
				break;
			case 5:
				READ_MODIFY(DISK_TASK_LIST6_LOCK, 0, SUSPEND_UNTIL_LOCKED, &LockResult);
				break;
			case 6:
				READ_MODIFY(DISK_TASK_LIST7_LOCK, 0, SUSPEND_UNTIL_LOCKED, &LockResult);
				break;
			case 7:
				READ_MODIFY(DISK_TASK_LIST8_LOCK, 0, SUSPEND_UNTIL_LOCKED, &LockResult);
				break;
			default:
				printf("OS_ERROR: Lock is not defined for this disk yet!\n");
		}

#ifndef MINIMAL_USER_PRINT
		if(LockResult)
			printf("Disk %ld task list unlocked!\n",disk_id);
		else
			printf("Disk %ld task list failed to unlock!\n",disk_id);
#endif
	}
	return LockResult;
}

// Print information of message queue
void OSShowDiskTaskListInfo() {
	INT32 i=0;
	OS_DISK_TASK *task_index;

	for(i=0;i<MAX_NUMBER_OF_DISKS;i++){
		OSLockDiskTaskList(i+1);
		if(OSDiskTaskList[i]->DiskTaskListHead != NULL) {
			INT32 j=0;
			task_index = OSDiskTaskList[i]->DiskTaskListHead;

			printf("******* Disk %d Task List Length: %d  ********** \n",i+1,OSDiskTaskList[i]->TaskListLength);

			while(task_index != NULL) {
				printf("-------disk task %d : operation: %d, responded: %d -----\n",j,task_index->OSDiskTaskType, task_index->OSDiskTaskResponded);
				task_index = task_index->OSDiskTaskNext;
				j++;
			}
		}
		else {
//			printf("******* Disk %d Task List Length: %d  ********** \n",i+1,OSDiskTaskList[i]->TaskListLength);
		}
		OSUnlockDiskTaskList(i+1);
	}
}
