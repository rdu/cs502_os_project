/*
 * interrupt.h
 *
 *  Created on: Oct 3, 2014
 *      Author: rdu
 */

#ifndef INTERRUPT_H_
#define INTERRUPT_H_

// Functions called by interrupt handlers. As interrupt service routines
//	are executed on another thread, it's easier to manage resource lock
//	by separating this part of code with others.


/*------------------ Functions for timer interrupt handling ------------------*/

// When an interrupt of timer occurs successfully, this function is called
void OSTimerSuccessISR();
// When an interrupt of timer occurs with a error, this function is called
void OSTimerFailureISR();



/*------------------- Functions for disk interrupt handling ------------------*/

// When an interrupt of disk occurs successfully, this function is called
void OSDiskSuccessISR(long disk_id);
// When an interrupt of disk occurs successfully, this function is called
void OSDiskFailureISR(long disk_id);

#endif /* INTERRUPT_H_ */
