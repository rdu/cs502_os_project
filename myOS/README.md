Code Change Notes
========


**global.h**: added preprocessor to make the code compile in different host operating systems without the need to change code

```
//#define         NT
//#define         LINUX
//#define         MAC

#ifdef _WIN64
	#define         NT
#elif _WIN32
	#define         NT
#elif __APPLE__
    #define         MAC
#elif __linux
	#define         LINUX
#endif
```

