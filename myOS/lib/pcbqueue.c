/*
 * pcbqueue.c
 *
 *  Created on: Sep 20, 2014
 *      Author: rdu
 */

#include 	"string.h"
#include	<stdio.h>
#include	<stdlib.h>
#include 	"pcbqueue.h"


// Add a element to the tail of a queue
void PushToPCBQueueTail(OS_PCB_QUEUE *queue, OS_PCB *pcb) {

	if(pcb != NULL) {
		if(queue->PCBQueueTail != NULL) {
			// add the link between the last element and the new element
			queue->PCBQueueTail->OSPCBNext = pcb;
			pcb->OSPCBPrev = queue->PCBQueueTail;
			pcb->OSPCBNext = NULL;
		}
		else {
			// tail pointer is null means the queue is empty before pushing
			queue->PCBQueueHead = pcb;
		}

		// set the tail of the queue to point to the new element
		queue->PCBQueueTail = pcb;

		queue->QueueLength++;
	}
	else {
		printf("OS ERROR: Invalid queue operation!\n");
	}
}

// Remove a existing element from the head of a queue
OS_PCB* PopFromPCBQueueHead(OS_PCB_QUEUE *queue) {
	OS_PCB *pcb_head;

	if(queue->PCBQueueHead != NULL){
		// get the first element
		pcb_head = queue->PCBQueueHead;

		if(pcb_head->OSPCBNext != NULL) {
			// set the second element as the new queue head
			queue->PCBQueueHead = pcb_head->OSPCBNext;
//			queue->PCBQueueHead->OSPCBPrev = NULL;

			if(queue->PCBQueueHead != NULL)
				queue->PCBQueueHead->OSPCBPrev = NULL;
			else
				queue->PCBQueueTail = NULL;
		}
		else {
			queue->PCBQueueHead = NULL;
			queue->PCBQueueTail = NULL;
		}

		queue->QueueLength--;

		return pcb_head;
	}
	else
		return NULL;
}

// Insert a new element to a list
void InsertToPCBQueue(OS_PCB_QUEUE *queue, OS_PCB *element, INT32 sort_type) {

	if(element != NULL) {
		if(queue->PCBQueueHead == NULL || queue->PCBQueueTail == NULL) {
			queue->PCBQueueHead = element;
			queue->PCBQueueTail = element;

			element->OSPCBPrev = NULL;
			element->OSPCBNext = NULL;
		}
		else {
			OS_PCB *pcb_index;

			// add element according to priority
			if(sort_type == QSORT_PRIORITY) {

				pcb_index = queue->PCBQueueHead;

				if(element->OSPCBPriority < queue->PCBQueueHead->OSPCBPriority) {
					element->OSPCBPrev = NULL;
					element->OSPCBNext = queue->PCBQueueHead;
					queue->PCBQueueHead->OSPCBPrev = element;

					queue->PCBQueueHead = element;
				}
				else if(element->OSPCBPriority >= queue->PCBQueueTail->OSPCBPriority) {
					queue->PCBQueueTail->OSPCBNext = element;
					element->OSPCBPrev = queue->PCBQueueTail;
					element->OSPCBNext = NULL;

					queue->PCBQueueTail = element;
				}
				else {
					while(pcb_index != NULL) {

						if(pcb_index->OSPCBPriority > element->OSPCBPriority) {
							pcb_index->OSPCBPrev->OSPCBNext = element;

							element->OSPCBPrev = pcb_index->OSPCBPrev;
							element->OSPCBNext = pcb_index;

							pcb_index->OSPCBPrev = element;

							break;
						}
						else {
							pcb_index = pcb_index->OSPCBNext;
						}
					}
				}
			}
			// add element according to sleep time
			else if(sort_type == QSORT_SLEEP_TIME) {

				pcb_index = queue->PCBQueueHead;

				if(element->OSPCBSleepTime < queue->PCBQueueHead->OSPCBSleepTime) {
					element->OSPCBPrev = NULL;
					element->OSPCBNext = queue->PCBQueueHead;
					queue->PCBQueueHead->OSPCBPrev = element;

					queue->PCBQueueHead = element;
				}
				else if(element->OSPCBSleepTime >= queue->PCBQueueTail->OSPCBSleepTime) {
					queue->PCBQueueTail->OSPCBNext = element;
					element->OSPCBPrev = queue->PCBQueueTail;
					element->OSPCBNext = NULL;

					queue->PCBQueueTail = element;
				}
				else {
					while(pcb_index != NULL)  {
						if(pcb_index->OSPCBSleepTime > element->OSPCBSleepTime) {
							pcb_index->OSPCBPrev->OSPCBNext = element;

							element->OSPCBPrev = pcb_index->OSPCBPrev;
							element->OSPCBNext = pcb_index;

							pcb_index->OSPCBPrev = element;

							break;
						}
						else {
							pcb_index = pcb_index->OSPCBNext;
						}
					}
				}
			}
		}

		queue->QueueLength++;
	}
	else {
		printf("OS ERROR: Invalid queue operation!\n");
	}
}

// Remove a existing element from a list
BOOL RemoveFromPCBQueue(OS_PCB_QUEUE *queue, OS_PCB *element) {

	if(element != NULL) {
		if(SearchPIDFromQueue(queue, element->OSPCBPid) != NULL) {
			if(queue->PCBQueueHead == element) {
				//			printf("process found before deleted \n");
				queue->PCBQueueHead = element->OSPCBNext;
				if(queue->PCBQueueHead != NULL)
					queue->PCBQueueHead->OSPCBPrev = NULL;
				else
					queue->PCBQueueTail = NULL;
			}
			else if(queue->PCBQueueTail == element) {
				queue->PCBQueueTail = element->OSPCBPrev;
				queue->PCBQueueTail->OSPCBNext = NULL;
			}
			else {
				element->OSPCBPrev->OSPCBNext = element->OSPCBNext;
				element->OSPCBNext->OSPCBPrev = element->OSPCBPrev;
			}

			element->OSPCBNext = NULL;
			element->OSPCBPrev = NULL;

			queue->QueueLength--;

			return TRUE;
		}
		else {
			return FALSE;
		}
	}
	else {
		printf("OS ERROR: Invalid queue operation!\n");
		return FALSE;
	}
}

// Search a process using process name
OS_PCB*	SearchNameFromQueue(OS_PCB_QUEUE *queue, char *process_name) {
	OS_PCB* pcb_index;

	pcb_index = queue->PCBQueueHead;

//	if(pcb_index != NULL) {
//		while(strcmp(pcb_index->OSPCBProcName,process_name) != 0) {
//			pcb_index = pcb_index->OSPCBNext;
//
//			if(pcb_index == NULL)
//				break;
//		}
//	}

	while(pcb_index != NULL) {
		if(strcmp(pcb_index->OSPCBProcName,process_name) == 0)
			break;
		else
			pcb_index = pcb_index->OSPCBNext;
	}

	return pcb_index;
}

// Search a process using process id
OS_PCB* SearchPIDFromQueue(OS_PCB_QUEUE *queue, long process_id) {
	OS_PCB* pcb_index;

	pcb_index = queue->PCBQueueHead;

	while(pcb_index != NULL) {
		if(pcb_index->OSPCBPid == process_id)
			break;
		else
			pcb_index = pcb_index->OSPCBNext;
	}

	return pcb_index;
}
