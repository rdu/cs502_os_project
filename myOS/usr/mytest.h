/*
 * mytest.h
 *
 *  Created on: Sep 23, 2014
 *      Author: rdu
 */

#ifndef MYTEST_H_
#define MYTEST_H_

//------------------------------------------------------------------------//
// The following tests are modified from the code in test.c. They are used
//	to test a specific aspect (mostly an intermediate step) of a task before
//	the full test for that task in test.c can be passed.
//------------------------------------------------------------------------//

// This test is designed to test CREATE_PROCESS() and GET_PROCESS_ID()
//	system calls
void test_process(void);
// This test is designed to test if TerminateProcess() can work properly
void test_terminate(void);
// This test is designed to test if timer queue works properly, it's a
//	simplified version of test1c without recursively sleep in test1x
void test_timerqueue(void);
// This test is designed to test the dispatcher
void test_dispatcher(void);
// This test is designed to test if suspend/resume works
void test_suspend(void);
// This test is designed to test if priority can be changed properly
void test_changepriority(void);
// This test is designed to test if the OS can behave properly when it tries
//	to suspend/terminate a process in timer queue
void test_timerqueuespecial(void);
// This test is designed to test if disk can write and read
void test_diskwr(void);
// This test is designed to test is just one paging request can be responded properly
void test_paging();
// This test is designed to test page replacing when processes have different priorities
void test_paging_priority(void);

//------------------------------------------------------------------------//
// The following test are just copied here so that extra information can be
//	printed out or just part of the code can be tested
//------------------------------------------------------------------------//
void test_mytest1i(void);
void test_mytest1j(void);
void test_mytest2a(void);
void test_mytest2b(void);
void test_mytest2c(void);
void test_mytest2d(void);
void test_mytest2e(void);
void test_mytest2f(void);
void test_mytest2g(void);

//------------------------------------------------------------------------//
// The original test1l() doesn't work. This is a modified copy. It keeps most
//	of the original code and tries to make it possible to pass.
//------------------------------------------------------------------------//
void test_mytest1l(void);

//------------------------------------------------------------------------//
// Functions showing additional features are declared here
//------------------------------------------------------------------------//
// Code for the original test1m in test.c here
//		implementation of a simple shell
void test_mytest1m(void);
// This test is designed to show the usage map of disk
void test_disk_usage();


#endif /* MYTEST_H_ */
