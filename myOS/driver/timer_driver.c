/*
 * timer.c
 *
 *  Created on: Sep 8, 2014
 *      Author: rdu
 */

#include	<stdlib.h>
#include	"hardware_driver.h"
#include	"protos.h"
#include	"myos.h"

// Private function prototypes
BOOL OSLockTimerDrvOperation();
BOOL OSUnlockTimerkDrvOperation();

// Communicate with timer and get current system time
/**
 * @input time: the time of day got from the system will be stored in the
 * 					variable which this pointer points to
 */
void DrvGetTimeOfDay(INT32 *time) {
	CALL( MEM_READ( Z502ClockStatus, time ) );
}

// Return current system time, internally works in the same way
//	with DrvGetTimeOfDay(), but provides different access interface
/**
 * @output: the current system will be return from the function
 */
INT32 DrvGetCurrentSysTime() {
	INT32 sys_time;
	INT32 *time = 0;

	time = (INT32 *)malloc(sizeof(INT32));
	MEM_READ( Z502ClockStatus, time );
	sys_time = *time;
	free(time);

	return sys_time;
}

// Set sleep time to timer, used when only single process runs like
//	the situation in test0
/**
 * @input sleep_time: the time for the process to sleep
 */
void DrvSetSleepTime(long sleep_time) {
	INT32 Status;

	if(sleep_time >= 0) {
		CALL( MEM_READ(Z502TimerStatus, &Status) );
		if (Status == DEVICE_FREE) {
			CALL( MEM_WRITE(Z502TimerStart, &sleep_time) );
			CALL( MEM_READ(Z502TimerStatus, &Status) );

			if (Status == DEVICE_IN_USE) {
				printf("The next output from the Interrupt Handler should report that \n");
				printf("   interrupt of device 4 has occurred with no error.\n");
			}
			else {
				printf("HARDWARE ERROR: Sleep time is not set correctly!\n");
			}

			Z502Idle();                //  Let the interrupt for this timer occur
		}
		else
			printf("HARDWARE ERROR: Timer is busy now!\n");
	}
	else {
		printf("ILLEGAL PARAMETER: Sleep time cannot be a negative number!\n");
	}
}

// Rest sleep time, this functions is to be called after one interrupt
//	is triggered and the timer queue is not empty. Z502_Idle() is not
//	called inside this function
// As this function is called to reset timer, there is no need to check if timer
//	is busy first. Otherwise only one time can be set and others will be blocked
//	until this time is interrupted.
/**
 * @input sleep_time: the time to sleep for the process which has a short sleep time
 * @output: reset result, TIMER_RESET_SUCCESS, TIMER_RESET_FAILURE or TIMER_RESET_ILLEGAL_PARAM
 */
INT32 DrvResetTimer(long sleep_time) {
	INT32 Status;

	if(sleep_time >= 0) {
		OSLockTimerDrvOperation();

		CALL( MEM_WRITE(Z502TimerStart, &sleep_time) );
		CALL( MEM_READ(Z502TimerStatus, &Status) );

		OSUnlockTimerkDrvOperation();

		if (Status == DEVICE_IN_USE) {
#ifndef MINIMAL_DRIVER_PRINT
			printf("Sleep time from head of timer queue has been loaded successfully! \n");
			printf("  A timer interrupt will occur when system time is: %ld \n",sleep_time+DrvGetCurrentSysTime());
#endif
			return TIMER_RESET_SUCCESS;
		}
		else {
			printf("HARDWARE ERROR: Sleep time is not set correctly!\n");
			return TIMER_RESET_FAILURE;
		}
	}
	else {
#ifndef MINIMAL_DRIVER_PRINT
		printf("ILLEGAL PARAMETER: Sleep time cannot be a negative number!\n");
#endif
		return TIMER_RESET_ILLEGAL_PARAM;
	}
}

// Check if timer is available, this function will be called when timer queue
//	is empty and the first process wants to sleep and set a sleep time for timer.
//	Afterwards, the loading work will be taken care of in interrupt handler
/**
 * @output: TRUE if timer is idle and otherwise FALSE
 */
BOOL OSCheckIfTimerIdle() {
	INT32 Status;

	CALL( MEM_READ(Z502TimerStatus, &Status) );
	if (Status == DEVICE_FREE) {
		return TRUE;
	}
	else {
		return FALSE;
	}
}

// Lock timer register operation
/**
 * @output: lock result
 */
BOOL OSLockTimerDrvOperation() {
	BOOL LockResult;

	LockResult = FALSE;

	READ_MODIFY(TIMER_DRV_REG_SET_LOCK, 1, SUSPEND_UNTIL_LOCKED, &LockResult);

#ifndef MINIMAL_DRIVER_PRINT
		if(LockResult)
			printf("Timer register operation locked!\n");
		else
			printf("Timer register operation failed to lock!\n");
#endif

	return LockResult;
}

// Unlock timer register operation
/**
 * @output: lock result
 */
BOOL OSUnlockTimerkDrvOperation() {
	BOOL LockResult;

	LockResult = FALSE;

	READ_MODIFY(TIMER_DRV_REG_SET_LOCK, 0, SUSPEND_UNTIL_LOCKED, &LockResult);

#ifndef MINIMAL_DRIVER_PRINT
		if(LockResult)
			printf("Timer register operation unlocked!\n");
		else
			printf("Timer register operation failed to unlock!\n");
#endif

	return LockResult;
}
