/*
 * disk.c
 *
 *  Created on: Sep 11, 2014
 *      Author: rdu
 */

#include 	"hardware_driver.h"
#include	"myos.h"
#include	"protos.h"

// Variables used to record usage of disk
UINT32 DiskRecord[MAX_NUMBER_OF_DISKS][NUM_LOGICAL_SECTORS/DISK_RECORD_LENGTH] = {{0,0}};
UINT32 DiskRecordSetMask[32] = {0};
UINT32 DiskRecordResetMask[32] = {0};

// Private function prototypes
// Set the status of a sector of a disk to be used or unused
void DrvSetDiskSectorStatus(long disk_id, long sector, BOOL status);
// Get the status of the specified sector and disk
BOOL DrvGetDiskSectorStatus(long disk_id, long sector);
BOOL OSLockDiskDrvOperation();
BOOL OSUnlockDiskDrvOperation();
//void DrvDiskOperate(long disk_id, long sector, char *data, INT32 disk_action);

// Read data from the disk
/**
 * @input disk_id: id of the disk that you want to read data from
 * @input sector: sector number of the disk
 * @input data: buffer used to contain data read from disk
 */
void DrvDiskRead(long disk_id, long sector, char *data) {
	INT32 disk_status;
	INT32 disk_action;

	OSLockDiskDrvOperation();

	// specify disk id
	MEM_WRITE(Z502DiskSetID, &disk_id);

	// check disk availability
	MEM_READ(Z502DiskStatus, &disk_status);
	if (disk_status == DEVICE_FREE) {       // Disk hasn't been used - should be free
#ifndef MINIMAL_DRIVER_PRINT
		printf("Disk is available to read\n");
#endif
		// specify disk sector
		MEM_WRITE(Z502DiskSetSector, &sector);
		MEM_WRITE(Z502DiskSetBuffer, (INT32 * )data);
		disk_action = DISK_ACT_READ;                         // Specify a read
		MEM_WRITE(Z502DiskSetAction, &disk_action);
		disk_action = DISK_ACT_START;                        // Must be set to 0
		MEM_WRITE(Z502DiskStart, &disk_action);

		// Disk should now be started - let's see
		MEM_WRITE(Z502DiskSetID, &disk_id);
		MEM_READ(Z502DiskStatus, &disk_status);
		if (disk_status == DEVICE_IN_USE) {
#ifndef MINIMAL_DRIVER_PRINT
			printf("Disk is reading data from specified sector\n");
#endif
		}
		else
			printf("HARDWARE ERROR: Disk %ld failed to start reading!\n", disk_id);
	}
	else
		printf("HARDWARE ERROR: Disk %ld is busy for reading!\n", disk_id);

	OSUnlockDiskDrvOperation();
}

// Write data to the disk
/**
 * @input disk_id: id of the disk that you want to write data to
 * @input sector: sector number of the disk
 * @input data: data read you want to write to disk
 */
void DrvDiskWrite(long disk_id, long sector, char *data) {
	INT32 disk_status;
	INT32 disk_action;

	OSLockDiskDrvOperation();

	// specify disk id
	MEM_WRITE(Z502DiskSetID, &disk_id);

	// check disk availability
	MEM_READ(Z502DiskStatus, &disk_status);
	if (disk_status == DEVICE_FREE) {        // Disk hasn't been used - should be free
#ifndef MINIMAL_DRIVER_PRINT
		printf("Disk is available to write\n");
#endif
		// specify disk sector
		MEM_WRITE(Z502DiskSetSector, &sector);
		MEM_WRITE(Z502DiskSetBuffer, (INT32 * )data);
		disk_action = DISK_ACT_WRITE;                        // Specify a write
		MEM_WRITE(Z502DiskSetAction, &disk_action);
		disk_action = DISK_ACT_START;                        // Must be set to 0
		MEM_WRITE(Z502DiskStart, &disk_action);

		// Disk should now be started - let's see
		MEM_WRITE(Z502DiskSetID, &disk_id);
		MEM_READ(Z502DiskStatus, &disk_status);
		if (disk_status == DEVICE_IN_USE) {
#ifndef MINIMAL_DRIVER_PRINT
			printf("Disk is writing data to specified sector\n");
#endif
			DrvSetDiskSectorStatus(disk_id,sector,TRUE);
		}
		else
			printf("HARDWARE ERROR: Disk %ld failed to start writing!\n", disk_id);
	}
	else
		printf("HARDWARE ERROR: Disk %ld is busy for writing!\n", disk_id);

	OSUnlockDiskDrvOperation();
}

// Check availability of disk
/**
 * @input disk_id: id of the queried disk
 * @output: availability of the disk
 */
BOOL DrvCheckIfDiskIdle(long disk_id) {
	INT32 disk_status;

	OSLockDiskDrvOperation();

	// specify disk id
	MEM_WRITE(Z502DiskSetID, &disk_id);

	// check disk availability
	MEM_READ(Z502DiskStatus, &disk_status);

	OSUnlockDiskDrvOperation();

	if (disk_status == DEVICE_FREE)
		return TRUE;
	else
		return FALSE;
}

// TODO to be re-implemented
// Find a disk/sector that is available for writing data
/**
 * @input *disk_id: pointer used to get return value
 * @input *sector: pointer used to get return value
 * @output : operation result, success or fail
 */
BOOL DrvGetFreeDiskSector(long *disk_id, long *sector) {
	static long free_sector = 0;

	*disk_id = 8;
	*sector = free_sector;

	free_sector++;

	if(free_sector == 1600) {
		(*disk_id)--;
		free_sector=0;
		printf("disk is full\n");
	}

	if((*disk_id) >= 1 && (*disk_id) <= MAX_NUMBER_OF_DISKS && (*sector) >= 0 && (*sector) < NUM_LOGICAL_SECTORS)
		return TRUE;
	else
		return FALSE;
}

// Initialize disk information (format)
void DrvDiskFormat() {
	int i = 0;
	int j = 0;

	for( i = 0; i < MAX_NUMBER_OF_DISKS; i++) {
		for( j = 0; j < NUM_LOGICAL_SECTORS/DISK_RECORD_LENGTH; j++) {
			DiskRecord[i][j] = 0;
		}
	}

	for( i = 0; i < DISK_RECORD_LENGTH; i++) {
		DiskRecordSetMask[i] = (UINT32)1 << i;
		DiskRecordResetMask[i] = ~DiskRecordSetMask[i];
//		printf("set: %u , reset: %u\n",DiskRecordSetMask[i],DiskRecordResetMask[i]);
	}
}

// Display the usage of the specified disk
void DrvShowDiskUsageMap(long disk_id) {
	UINT32 row = 0;
	UINT32 collum = 0;
	UINT32 sector = 0;
	int i = 0;

//	DrvSetDiskSectorStatus(disk_id,0,TRUE);
//	DrvSetDiskSectorStatus(disk_id,1,TRUE);
//	DrvSetDiskSectorStatus(disk_id,2,TRUE);
//	DrvSetDiskSectorStatus(disk_id,3,TRUE);
//	DrvSetDiskSectorStatus(disk_id,350,TRUE);
//
////	for(sector = 0; sector< 25; sector++)
//		printf("value: %d\n",DiskRecord[0][sector]);
//		printf("status of sector 0: %d \n",DrvGetDiskSectorStatus(disk_id,sector));
//
//	DrvSetDiskSectorStatus(disk_id,0,FALSE);
//	DrvSetDiskSectorStatus(disk_id,1,TRUE);
//	DrvSetDiskSectorStatus(disk_id,2,FALSE);
//	DrvSetDiskSectorStatus(disk_id,3,FALSE);
//
//	printf("value: %d\n",DiskRecord[0][sector]);
//	printf("status of sector 0: %d \n",DrvGetDiskSectorStatus(disk_id,sector));

	printf("\n");
	for(i = 0; i < 65; i++)
		printf("*");
	printf("\n");
	for(i = 0; i < 20; i++)
		printf(" ");
	printf("Usage Map of Disk %ld\n",disk_id);
	for(i = 0; i < 65; i++)
		printf("-");
	printf("\n");

	for(row = 0; row < NUM_LOGICAL_SECTORS/(DISK_RECORD_LENGTH*2); row ++) {
		for(collum = 0; collum < DISK_RECORD_LENGTH * 2; collum++) {
			sector = row*64 + collum;

			if(collum == 32)
				printf(" ");

			if(DrvGetDiskSectorStatus(disk_id,sector) == TRUE)
				printf("\x1b[31m1\x1b[0m");
			else
				printf("\x1b[32m0\x1b[0m");
		}
		printf("\n");
	}

	for(i = 0; i < 65; i++)
		printf("*");
	printf("\n");

	printf("\n");
//	printf("\x1b[31mThis is red text\x1b[0m\n");
//	printf("\x1b[32mThis is green text\x1b[0m\n");
}

// Set the status of a sector of a disk to be used or unused
void DrvSetDiskSectorStatus(long disk_id, long sector, BOOL status) {
	long disk_index = 0;
	long disk_cluster = 0;
	long cluster_offset = 0;

	if(disk_id < 1 || disk_id > MAX_NUMBER_OF_DISKS || sector < 0 || sector >= NUM_LOGICAL_SECTORS) {
		printf("DRV_INFO: Illegal disk id or sector number\n");
		return ;
	}

	disk_index = disk_id - 1;
	disk_cluster = sector/DISK_RECORD_LENGTH;
	cluster_offset = sector%DISK_RECORD_LENGTH;

//	printf("disk_index: %ld, disk_cluster: %ld, cluster_offset: %ld\n",disk_index,disk_cluster,cluster_offset);
//	printf("Set mask: %u, Reset mask: %u\n",DiskRecordSetMask[cluster_offset],DiskRecordResetMask[cluster_offset]);

	if(status == TRUE)
		DiskRecord[disk_index][disk_cluster] |= DiskRecordSetMask[cluster_offset];
	else
		DiskRecord[disk_index][disk_cluster] &= DiskRecordResetMask[cluster_offset];
}

// Get the status of the specified sector and disk
BOOL DrvGetDiskSectorStatus(long disk_id, long sector) {
	long disk_index = 0;
	long disk_cluster = 0;
	long cluster_offset = 0;
	UINT32 check = 0;

	disk_index = disk_id - 1;
	disk_cluster = sector/DISK_RECORD_LENGTH;
	cluster_offset = sector%DISK_RECORD_LENGTH;

	check = DiskRecord[disk_index][disk_cluster] & DiskRecordSetMask[cluster_offset];

	if(check != 0)
		return TRUE;
	else
		return FALSE;
}

// Lock disk register operation because related disk registers may be
//	shared by more than one thread
/**
 * @output: lock result
 */
BOOL OSLockDiskDrvOperation() {
	BOOL LockResult;

	LockResult = FALSE;

	READ_MODIFY(DISK_DRV_REG_SET_LOCK, 1, SUSPEND_UNTIL_LOCKED, &LockResult);

#ifndef MINIMAL_DRIVER_PRINT
		if(LockResult)
			printf("Disk register operation locked!\n");
		else
			printf("Disk register operation failed to lock!\n");
#endif

	return LockResult;
}

// Unlock disk register operation because related disk registers may be
//	shared by more than one thread
/**
 * @output: lock result
 */
BOOL OSUnlockDiskDrvOperation() {
	BOOL LockResult;

	LockResult = FALSE;

	READ_MODIFY(DISK_DRV_REG_SET_LOCK, 0, SUSPEND_UNTIL_LOCKED, &LockResult);

#ifndef MINIMAL_DRIVER_PRINT
		if(LockResult)
			printf("Disk register operation unlocked!\n");
		else
			printf("Disk register operation failed to unlock!\n");
#endif

	return LockResult;
}
